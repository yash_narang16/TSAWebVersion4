<%-- 
    Document   : Mapping
    Created on : Jul 6, 2016, 10:02:29 AM
    Author     : Yash
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <script type='text/javascript' src='js/loader.js'></script>
        <link rel="stylesheet" type="text/css" href="css/loader.css">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">

        <link rel="stylesheet" type="text/css" href="css/newcss.css">
        <link rel="stylesheet" type="text/css" href="css/calendar.css">
        <link rel="stylesheet" type="text/css" href="css/mapping.css">
        <link rel="stylesheet" type="text/css" href="css/Toast.css">
        

    </head>
    <body ng-app="mapping" ng-controller="mappingctrl">
        <nav class="navbar navbar-default web-color navbar-fixed-top">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand text-color" href="Home.jsp">
                        <!--<img alt="Brand" src="Images/logo.png">-->
                        Teacher Support Application
                    </a>
                </div>
            </div>
        </nav>
        <div class="container" style="padding:60px;">
            <div class="row" ng-cloak>
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <uib-tabset active="activeJustified" justified="true" class="vertical-layout">
                        <uib-tab index="0" heading=" Map Lesson Plan">
                            <!--//tab content1-->
                            <div class="row">
                                <form>
                                    <div class="form-group col-lg-6 col-md-6 col-sm-12">
                                        <label for="Class">Choose Class</label>
                                        <select class="form-control" id="Class" ng-model="class.class_id[0]" ng-options="class.class_name for class in class" ng-change="Fetch_Subject(class.class_id[0])" required>
                                            <option disabled selected value="">Choose your option</option>
                                        </select>
                                    </div>

                                    <div class="form-group col-lg-6 col-md-6 col-sm-12">
                                        <label for="Subject">Choose Subject</label>
                                        <select class="form-control" id="Subject" ng-model="subject.subject_id[0]" ng-options="subject.subject_name for subject in subject" ng-change="Fetch_chapter(subject.subject_id[0])" required>
                                            <option disabled selected value="">Choose Subject</option>
                                            <option ng:repeat="subject in subject" value="{{subject.subject_id}}">{{subject.subject_name}}</option>

                                        </select>

                                    </div>

                                </form>

                            </div>
                            <div class="row">
                                <div class="col-lg-3 col-md-3 col-sm-12" ng-repeat="lessonplan in lessonplan">
                                    <div class="panel panel-default">
                                        <div class="panel-heading lessonplan-card">
                                            {{lessonplan.lessonplan_name}}
                                            <div class="icon-tools pull-right">

                                                <span class="glyphicon glyphicon-modal-window" ng-click="open('lg')"></span>



                                                <span class="glyphicon glyphicon-calendar" ng-click="opencalendar(this)"></span>
                                                <span class="glyphicon glyphicon-tag"></span>
                                            </div>
                                        </div>
                                        <div class="panel-body">
                                            <!--<input class="form-control" uib-datepicker-popup="{{format}}" ng-model="dt" is-open="popup1.opened" datepicker-options="dateOptions" ng-required="true" close-text="Close" alt-input-formats="altInputFormats" style="display:none;">-->
                                            <div class="lessonplan-text"> 
                                                <span class="pull-left">{{lessonplan.lessonplan_class}} Class</span>
                                                <!--<span class="pull-right">{{dt | date:'yyyy-MM-dd'}}</span>-->
                                            </div>
                                            <div class="lessonplan-text"> 
                                                <span class="pull-left">{{lessonplan.lessonplan_subject}}</span>
                                                <span class="pull-right">{{lessonplan.lessonplan_duration}} min</span>
                                            </div>
                                            <div> 
                                                <p> Objectives: {{lessonplan.lessonplan_objective_name }}</p>

                                                <p>Objective Description: {{lessonplan.lessonplan_objective_descp}} </p>
                                            </div><!--
                                            -->
                                        </div>

                                    </div>
                                </div>
                   

                            </div>

                        </uib-tab>
                        <uib-tab index="1" heading="View Mapped Lesson Plan" ng-init="loadEvents()">
                            <calendar ng-model="currentDate" calendar-mode="mode" event-source="eventSource"
                                      range-changed="reloadSource(startTime, endTime)"
                                      event-selected="onEventSelected(event)"
                                      time-selected="onTimeSelected(selectedTime)"></calendar>

                        </uib-tab>

                    </uib-tabset>
                </div>
            </div>
        </div>



        <script type="text/javascript" src="js/angular.js"></script>
        <script type="text/javascript" src="js/ui-bootstrap-tpls-1.3.3.min.js"></script>
        <script type="text/javascript" src="js/calendar-tpls.min.js"></script>
        <script type="text/javascript" src="js/mapping.js"></script>
        <script type="text/javascript" src="js/angular-animate.min.js"></script>
        <script type="text/javascript" src="js/toast.js"></script>
        <script type="text/javascript" src="js/Services/CommonService.js"></script>
        
        <script type="text/javascript" src="js/Services/LessonPlanService.js"></script>
        <script type="text/javascript" src="js/Services/MappingService.js"></script>
        
        
        
        <script type="text/ng-template" id="openview.html">
            <div class="modal-header">
            <h3 class="modal-title">Lesson Plan</h3>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
            <button class="btn btn-primary" type="button" ng-click="ok()">OK</button>
            <button class="btn btn-warning" type="button" ng-click="cancel()">Cancel</button>
            </div>
        </script>
        <script type="text/ng-template" id="calendar.html">
            <div class="modal-header">
            <h3 class="modal-title">Lesson Plan</h3>
            </div>
            <div class="modal-body" ng-init="loadEvents()">
            <calendar ng-model="currentDate" calendar-mode="mode" event-source="eventSource"
            range-changed="reloadSource(startTime, endTime)"
            event-selected="onEventSelected(event)"
            time-selected="onTimeSelected(selectedTime)"></calendar>
            </div>
            <div class="modal-footer">
            <button class="btn btn-primary" type="button" ng-click="ok()">OK</button>
            <button class="btn btn-warning" type="button" ng-click="cancel()">Cancel</button>
            </div>
        </script>




    </body>
</html>
