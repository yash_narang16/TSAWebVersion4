<%-- 
    Document   : Attendance
    Created on : Jul 5, 2016, 2:54:40 PM
    Author     : Yash
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>

        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/newcss.css">
        <link rel="stylesheet" type="text/css" href="css/attendance.css">
        <link rel="stylesheet" type="text/css" href="css/Toast.css">
        <link rel="stylesheet" type="text/css" href="css/new loader.css">

    </head>
    <body ng-app="attendance" ng-controller="attendancectrl">
        <nav class="navbar navbar-default web-color navbar-fixed-top">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand text-color" href="#">
                        <!--<img alt="Brand" src="Images/logo.png">-->
                        Teacher Support Application
                    </a>
                </div>
            </div>
        </nav>
        <div class="container" style="padding: 60px;">
            <div class="loader-wrapper" ng-show="loaderhide">
                <div class="loader"></div>
            </div>

            <uib-tabset active="activeJustified" justified="true" ng-cloak>
                <uib-tab index="0" heading="Record Attendance">

                    <div class="row">
                        <div class="form-group col-lg-6 col-md-6 col-sm-12">
                            <label for="Class">Choose Class</label>
                            <select class="form-control" id="Class" ng-model="class.class_id[0]" ng-options="class.class_name for class in class" ng-change="Fetch_Subject(class.class_id[0])" required>
                                <option disabled selected value="">Choose your option</option>
                            </select>
                        </div>
                        <!--                         <div class="col-lg-6 col-md-6 col-sm-12">
                                                     <div class="curdate-box pull-right">
                                                        {{curdate | date:'yyyy-MM-dd'}}
                                                     </div>
                                                </div>-->
                    </div>


                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="heading-attendance">
                              Student  Record
                                <div class="pull-right" ng-show="showbutton" ng-cloak>
                                     <button type="button" class="btn btn-warning" ng-click="clearAll()">
                                    <span class="glyphicon glyphicon-remove" aria-hidden="true" ></span> Deselect All
                                </button>
                                     <button type="button" class="btn btn-primary" ng-click="selectAll()">
                                    <span class="glyphicon glyphicon-asterisk" aria-hidden="true" ></span> Select All
                                </button>
                                <button type="button" class="btn btn-success" ng-click="saveAttendance()">
                                    <span class="glyphicon glyphicon-ok" aria-hidden="true" ></span> Submit
                                </button>
<!--                                <span> {{curdate| date:'yyyy-MM-dd'}}</span>-->
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-12" ng-repeat="student in studentdata track by $index">

                            <div class="student-name">
                                <input  id="{{student.studentname}}" class="checkbox-custom" type="checkbox" ng-model="student.check">
                                <label for="{{student.studentname}}" class="checkbox-custom-label">  {{student.studentname}}</label>  


                            </div>


                        </div>





                    </div>



                </uib-tab>
                <uib-tab index="1" heading="View Attendance" ng-click="display_attendance()" ng-cloak>

                    <div class="row">
                        <div class="form-group col-lg-6 col-md-6 col-sm-12">

                            <label for="Class">Choose Class</label>
                            <select class="form-control" id="Class">
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                                <option>5</option>
                            </select>


                        </div>
                        <div class="form-group col-lg-6 col-md-6 col-sm-12">
                            <label for="Class">Select Date</label>

                            <p class="input-group">
                                <input type="text" class="form-control" uib-datepicker-popup="{{format}}" ng-model="dt" is-open="popup1.opened" ng-change="filterdate(dt)" datepicker-options="dateOptions" ng-required="true" close-text="Close" alt-input-formats="altInputFormats" />
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-default" ng-click="open1()"><i class="glyphicon glyphicon-calendar"></i></button>
                                </span>
                            </p>
                        </div>




                    </div>
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th ng-repeat="day in dayname track by $index">{{day.substring(0, 1)}}</th>


                            </tr>
                        </thead>
                        <tbody>
                                <tr ng-repeat="studentdata in attendancedata track by $index">
                                <td>{{studentdata.student_name}}</td>
                     
                           
                            <td ng-repeat="record in attendancechart[$index] track by $index" class="{{record}}"></td>
                            
                          
                                
                            </tr>

                        </tbody>
                    </table>

<!--  <span ng-repeat="status in attendancechart track by $index">
                            <td ng-repeat="record in status  track by $index">{{record[$index]}}</td>
                        </span>-->


                </uib-tab>

            </uib-tabset>




        </div>






        <script type="text/javascript" src="js/angular.js"></script>
        <script type="text/javascript" src="js/ui-bootstrap-tpls-1.3.3.min.js"></script>
        <script type="text/javascript" src="js/attendance.js"></script>
        <script type="text/javascript" src="js/Services/CommonService.js"></script>
        <script type="text/javascript" src="js/Services/AttendanceService.js"></script>
        <script type="text/javascript" src="js/angular-animate.min.js"></script>
        <script type="text/javascript" src="js/toast.js"></script>


    </body>
</html>
