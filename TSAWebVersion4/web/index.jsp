<%-- 
    Document   : index
    Created on : Jun 28, 2016, 1:24:43 AM
    Author     : Yash
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>JSP Page</title>

        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/progressbar.css">
        <link rel="stylesheet" type="text/css" href="css/newcss.css">
        <link rel="stylesheet" type="text/css" href="css/new loader.css">

        <!--<link rel="stylesheet" type="text/css" href="css/mapping.css">-->
        <!--<link href='https://fonts.googleapis.com/css?family=Raleway:400,500,800' rel='stylesheet' type='text/css'>-->

        <link rel="stylesheet" type="text/css" href="css/dropzone.min.css">
        <link rel="stylesheet" type="text/css" href="css/rpTable.css">
        <link rel="stylesheet" type="text/css" href="css/Toast.css">
        <script type="text/javascript" src="js/angular.js"></script>

    </head>
    <body ng-app="myModule" ng-controller="main">

        <nav class="navbar navbar-default web-color navbar-fixed-top">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand text-color" href="#">
                        <!--<img alt="Brand" src="Images/logo.png">-->
                        Teacher Support Application
                    </a>
                </div>
            </div>
        </nav>
        

        <div class="container-fluid" style='padding-top:60px;'>

            <div class="loader-wrapper" ng-show="loaderhide">
                <div class="loader"></div>
            </div>


            <div class="card" ng-show="one">
                <div class="progress1">
                    <div class="circle active">
                        <span class="label">25%</span>
                        <span class="title">Objective</span>
                    </div>
                    <span class="bar half"></span>
                    <div class="circle">
                        <span class="label">50%</span>
                        <span class="title"> Activity</span>
                    </div>
                    <span class="bar"></span>
                    <div class="circle">
                        <span class="label">75%</span>
                        <span class="title">Rubrics</span>
                    </div>
                    <span class="bar"></span>
                    <div class="circle">
                        <span class="label">100%</span>
                        <span class="title">Lesson Plan</span>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <uib-tabset active="activeJustified" justified="true" class="vertical-layout">
                                <uib-tab index="0" heading=" Create Objective">
                                    <!--//tab content1-->


                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                            <form name="objectiveForm">
                                                <div class="form-group col-lg-6 col-md-6 col-sm-12">
                                                    <label for="curiicullum">Choose Curricullum</label>
                                                    <select class="form-control" id="curiicullum" ng-model="curi.curicullumid[0]" ng-options="curi.curicullum for curi in curicullum" ng-change="Fetch_class(curi.curicullumid[0])" required>
                                                        <option disabled selected value="">Choose your option</option>

                                                    </select>
                                                </div>
                                                <div class="form-group col-lg-6 col-md-6 col-sm-12">
                                                    <label for="Class">Choose Class</label>
                                                    <select class="form-control" id="Class" ng-model="class.class_id[0]" ng-options="class.class_name for class in class" ng-change="Fetch_Subject(class.class_id[0])" required>
                                                        <option disabled selected value="">Choose your option</option>
                                                    </select>
                                                </div>
                                                <div class="form-group col-lg-6 col-md-6 col-sm-12">
                                                    <label for="Subject">Choose Subject</label>
                                                    <select class="form-control" id="Subject" ng-model="subject.subject_id[0]" ng-options="subject.subject_name for subject in subject" ng-change="Fetch_chapter(subject.subject_id[0])" required>
                                                        <option disabled selected value="">Choose Subject</option>
                                                        <option ng:repeat="subject in subject" value="{{subject.subject_id}}">{{subject.subject_name}}</option>

                                                    </select>

                                                </div>

                                                <div class="form-group col-lg-6 col-md-6 col-sm-12">
                                                    <label for="Chapter">Choose Chapter</label>
                                                    <select class="form-control" id="Chapter" ng-model="chapter.chapter_id[0]" ng-options="chapter.chapter_name for chapter in chapter" ng-change="Select_chapter(chapter.chapter_id[0])" required>
                                                        <option disabled selected value="">Choose Chapter</option>
                                                    </select>

                                                </div>
                                                <div class="form-group col-lg-6 col-md-6 col-sm-12">
                                                    <label for="Name">Write Objective</label>
                                                    <input type="text" class="form-control" name="objective_name" placeholder="Objective" id="Name" ng-model="obj.objective_name" ng-pattern="/^[A-Za-z0-9 ]{3,20}$/" required>
                                                    <span ng-show="objectiveForm.objective_name.$touched && objectiveForm.objective_name.$error.required" ng-cloak>Objective Name Required</span>
                                                    <span ng-show="objectiveForm.objective_name.$error.pattern" ng-cloak>Only Characters and Numbers</span>
                                                </div>
                                                <div class="form-group col-lg-6 col-md-6 col-sm-12">
                                                    <label for="Description">Write Description</label>
                                                    <textarea class="form-control" rows="3" name="description" ng-model="obj.objective_description" required></textarea>
                                                    <span ng-show="objectiveForm.description.$touched && objectiveForm.description.$error.required" ng-cloak>Description Required</span>
                                                </div>
                                                <div class="form-group col-lg-12 col-md-12 col-sm-12">
                                                    <button type="button" class="btn btn-success pull-right" ng-click="createObjective(obj)" ng-disabled="objectiveForm.$invalid">Save Objective</button>

                                                </div>

                                            </form>


                                        </div>
                                    </div>

                                </uib-tab>
                                <uib-tab index="1" heading="Use Objective" select="fetch_Objective()" ng-cloak>
                                    <div class="row">
                                        <div class="col-lg-3 col-md-3 col-sm-12" ng-repeat="objective in objective">
                                            <div class="panel panel-default">
                                                <div class="panel-heading lessonplan-card">
                                                    <div class="heading pull-left">  {{objective.objective_name}}</div>

                                                    <div class="icon-tools pull-right">

                                                        <span class="glyphicon glyphicon-modal-window"></span>

                                                        <span class="glyphicon glyphicon-tag" ng-click="objectiveselected(this)"></span>
                                                    </div>
                                                </div>
                                                <div class="panel-body">

                                                    <div class="lessonplan-text"> 
                                                        <span class="pull-left">{{objective.objective_class}} Class</span>

                                                    </div>
                                                    <div class="lessonplan-text"> 
                                                        <span class="pull-left">{{objective.objective_subject}}</span>
                                                        <span class="pull-right">{{objective.objective_chapter}}</span>
                                                    </div>

                                                    <div class="description">
                                                        <p>{{objective.objective_description}}</p>
                                                        <!--<p> Home Activity: Color objects of different shapes </p>-->
                                                    </div><!--
                                                    -->
                                                </div>

                                            </div>
                                        </div>


                                    </div>

                                </uib-tab>

                            </uib-tabset>
                        </div>
                    </div>


                </div>




            </div>
            <div class="card" ng-show="two" ng-cloak>
                <div class="progress1">
                    <div class="circle done">
                        <span class="label">25%</span>
                        <span class="title">Objective</span>
                    </div>
                    <span class="bar done"></span>
                    <div class="circle active">
                        <span class="label">50%</span>
                        <span class="title"> Activity</span>
                    </div>
                    <span class="bar half"></span>
                    <div class="circle">
                        <span class="label">75%</span>
                        <span class="title">Rubrics</span>
                    </div>
                    <span class="bar"></span>
                    <div class="circle">
                        <span class="label">100%</span>
                        <span class="title">Lesson Plan</span>
                    </div>

                </div>

                <div class="container">
                    <div class="panel panel-default web-color text-color">
                        <div class="panel-body layout">
                            <div class="row">
                                <div class="col-lg-3 center">
                                    CBSE
                                </div>
                                <div class="col-lg-3 center">
                                    Taramani School
                                </div>
                                <div class="col-lg-3 center">
                                    Class iii
                                </div>
                                <div class="col-lg-3 center">
                                    Shapes
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <uib-tabset active="activeCreate" justified="true" class="vertical-layout">
                                <uib-tab index="0" heading="Create Activity">
                                    <!--//tab content1-->

                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                            <form>
                                                <div class="form-group col-lg-6 col-md-6 col-sm-12">
                                                    <label for="Objective1">Choose Objective</label>
                                                    <select class="form-control" id="Objective1" ng-model="objective.objective_id[0]" ng-options="objective.objective_name for objective in objective" ng-change="select_object(objective.objective_id[0])">
                                                        <option disabled selected vaue="">1</option>

                                                    </select>

                                                </div>
                                                <div class="form-group col-lg-6 col-md-6 col-sm-12">
                                                    <label for="AName">Write Name</label>
                                                    <input type="text" class="form-control" placeholder="Actvity Name" id="AName" ng-model="obj.name">
                                                </div>
                                                <div class="form-group col-lg-6 col-md-6 col-sm-12">
                                                    <label for="Material">Write Material</label>
                                                    <input type="text" class="form-control" placeholder="Material Required" id="Material" ng-model="obj.material">
                                                </div>
                                                <div class="form-group col-lg-6 col-md-6 col-sm-12">
                                                    <label for="Duration">Write Duration</label>
                                                    <input type="text" class="form-control" placeholder="Duraton Required" id="Duration" ng-model="obj.duration">

                                                </div>
                                                <div class="form-group col-lg-6 col-md-6 col-sm-12">
                                                    <label for="ADescription">How To Do</label>
                                                    <textarea class="form-control" rows="4" ng-model="obj.description"></textarea>

                                                </div>
                                                <div class='form-group col-lg-6 col-mg-6 col-sm-12'>
                                                    <label for="ADescription">Class Worksheets</label>
                                                    <div class="dropzone" options="dzOptions" callbacks="dzCallbacks" methods="dzMethods" ng-dropzone></div>
                                                </div>
                                                <button type="button" class="btn btn-success pull-right"ng-click="saveActivity(obj)">Save Activity</button>

                                            </form>


                                        </div>
                                    </div>

                                </uib-tab>
                                <uib-tab index="1" heading="Use Activity" select="useActivity()">
                                    <div class="row">
                                        <div class="col-lg-3 col-md-3 col-sm-12" ng-repeat="activity in activity">
                                            <div class="panel panel-default">
                                                <div class="panel-heading lessonplan-card">
                                                    <div class="heading pull-left">  {{activity.activity_name}}</div>

                                                    <div class="icon-tools pull-right">

                                                        <span class="glyphicon glyphicon-modal-window"></span>

                                                        <span class="glyphicon glyphicon-tag" ng-click="activitySelected(this)"></span>
                                                    </div>
                                                </div>
                                                <div class="panel-body">

                                                    <div class="lessonplan-text"> 
                                                        <span class="pull-left">{{activity.activity_class}} Class</span>
                                                        <span class="pull-right">{{activity.activity_curi}}</span>
                                                    </div>
                                                    <div class="lessonplan-text"> 
                                                        <span class="pull-left">{{activity.activity_subject}}</span>
                                                        <span class="pull-right">{{activity.activity_chapter}}</span>
                                                    </div>

                                                    <div class="description">
                                                        <p>{{activity.activty_description}}</p>
                                                        <!--<p> Home Activity: Color objects of different shapes </p>-->
                                                    </div><!--
                                                    -->
                                                </div>

                                            </div>
                                        </div>


                                    </div>


                                </uib-tab>

                            </uib-tabset>
                        </div>
                    </div>


                </div>





            </div>
            <div class="card" ng-show="three" ng-cloak>
                <div class="progress1">
                    <div class="circle done">
                        <span class="label">25%</span>
                        <span class="title">Objective</span>
                    </div>
                    <span class="bar done"></span>
                    <div class="circle done">
                        <span class="label">50%</span>
                        <span class="title"> Activity</span>
                    </div>
                    <span class="bar done"></span>
                    <div class="circle active">
                        <span class="label">75%</span>
                        <span class="title">Rubrics</span>
                    </div>
                    <span class="bar"></span>
                    <div class="circle">
                        <span class="label">100%</span>
                        <span class="title">Lesson Plan</span>
                    </div>
                </div>
                <div class="container">
                    <div class="panel panel-default web-color text-color">
                        <div class="panel-body layout">
                            <div class="row">
                                <div class="col-lg-3 center">
                                    CBSE
                                </div>
                                <div class="col-lg-3 center">
                                    Taramani School
                                </div>
                                <div class="col-lg-3 center">
                                    Class iii
                                </div>
                                <div class="col-lg-3 center">
                                    Shapes
                                </div>
                            </div>
                        </div>
                    </div>

                    <uib-tabset active="activeScholastic" justified="true">
                        <uib-tab index="0" heading="Scholastic" active>
                            <div>
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <div class="btn-group">
                                            <label class="btn btn-button" ng-model="radioModel" uib-btn-radio="'Holistic'" uncheckable>Holistic</label>
                                            <label class="btn btn-button" ng-model="radioModel" uib-btn-radio="'Analytic'" uncheckable>Analytic</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="vertical-layout-top">
                                    <div class="row" ng-show="radioModel == 'Holistic'">
                                        <form >
                                            <div class="form-group col-lg-6 col-md-6 col-sm-12">
                                                <label for="parameter">Choose No. of headings</label>
                                                <select class="form-control" id="parameter"  ng-model="val" ng-change="attributeCount(val)">

                                                    <option value="3">Three</option>
                                                    <option value="4">Four</option>
                                                    <option value="5">Five</option>

                                                </select>
                                            </div>
                                            <div class="form-group col-lg-6 col-md-6 col-sm-12">
                                                <label for="scale">Choose Scaling Parameters</label>
                                                <select class="form-control" id="scale"  ng-model="scale" ng-change="attributename(scale)">
                                                    <option disabled selected value="">Choose your item</option>
                                                    <option value="1">Grade A, Grade B, Grade C</option>
                                                    <option value="2">Below, Average, Good</option>
                                                    <option value="3">Bad, Good, Excellent</option>
                                                </select>
                                            </div>


                                            <div class="col-lg-12 col-md-12 col-sm-12">
                                                <div class="row" ng-repeat="x in xlist.name track by $index">
                                                    <div class="form-group col-lg-6 col-md-6 col-sm-12" >
                                                        <input type="text" class="form-control" placeholder="Heading Name" ng-model="xlist.name[$index]">
                                                    </div>
                                                    <div class="form-group col-lg-6 col-md-6 col-sm-12" >
                                                        <input type="text" class="form-control" placeholder="Description" ng-model="xlist.description[$index]">
                                                    </div>

                                                </div>
                                            </div>
                                        </form>
                                    </div>

                                    <div class="row" ng-show="radioModel == 'Analytic'">
                                        <form >
                                            <div class="form-group col-lg-4 col-md-4 col-sm-12">
                                                <label for="detailparameter">Choose No. of headings</label>
                                                <select class="form-control" id="detailparameter"  ng-model="detailparameter" ng-change="ParameterCount(detailparameter)">

                                                    <option value="3">Three</option>
                                                    <option value="4">Four</option>
                                                    <option value="5">Five</option>

                                                </select>
                                            </div>
                                            <div class="form-group col-lg-4 col-md-4 col-sm-12">
                                                <label for="parameter">Choose No. of headings</label>
                                                <select class="form-control" id="parameter"  ng-model="val" ng-change="attributeCount(val)">

                                                    <option value="3">Three</option>
                                                    <option value="4">Four</option>
                                                    <option value="5">Five</option>

                                                </select>
                                            </div>
                                            <div class="form-group col-lg-4 col-md-4 col-sm-12">
                                                <label for="scale">Choose Scaling Parameters</label>
                                                <select class="form-control" id="scale"  ng-model="scale" ng-change="attributename(scale)">
                                                    <option disabled selected value="">Choose your item</option>
                                                    <option value="1">Grade A, Grade B, Grade C</option>
                                                    <option value="2">Below, Average, Good</option>
                                                    <option value="3">Bad, Good, Excellent</option>
                                                </select>
                                            </div>



                                            <div class="col-lg-12 col-md-12 col-sm-12"> 
                                                <div class="row" ng-repeat="number in  inputs.parameter_name track by $index">

                                                    <div class="form-group col-lg-3 col-md-4 col-sm-12" >
                                                        <input type="text" class="form-control" placeholder="Parameters" ng-model="inputs.parameter_name[$index]">
                                                    </div>
                                                    <div class="form-group col-lg-3 col-md-4 col-sm-12" >
                                                        <input type="text" class="form-control" placeholder="Weightage" ng-model="inputs.weightage[$index]">
                                                    </div>
                                                    <div class="form-group col-lg-6 col-md-4 col-sm-12" >
                                                        <div class="row" ng-repeat="x in xlist.name track by $index">
                                                            <div class="col-lg-6 col-md-6 col-sm-12" >
                                                                <input type="text" class="form-control" placeholder="Heading Name" ng-model="xlist.name[$index]">
                                                            </div>
                                                            <div class="col-lg-6 col-md-6 col-sm-12" >
                                                                <input type="text" class="form-control" placeholder="Description" ng-model="xlist.description[$index][$parent.$index]">
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </form>

                                    </div>

                                    <button type="button" class="btn btn-success pull-right" ng-model="second" name="content" ng-click="createScholastic(data)">Create Scholastic</button>

                                </div>
                            </div>


                        </uib-tab>
                        <uib-tab index="1" heading="Co-Scholastic">
                            <div>
                                <div class="row">
                                    <div class="col-lg-8 col-md-8 col-sm-12">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                Select Your Skills
                                            </div>
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-lg-3 col-md-3 col-sm-3">
                                                        <div class="panel panel-default web-color icon-panel-head" ng-click="skillfunction($event)">
                                                            <div class="panel-body center icon-panel">
                                                                <img src="Images/business-partnership.svg">
                                                                <div class="text-color icon-text-2">Team Work</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3 col-md-3 col-sm-3">
                                                        <div class="panel panel-default web-color icon-panel-head" ng-click="skillfunction($event)">
                                                            <div class="panel-body center icon-panel">
                                                                <img src="Images/microphone-with-wire.svg">
                                                                <div class="text-color icon-text-2">Participation</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3 col-md-3 col-sm-3">
                                                        <div class="panel panel-default web-color icon-panel-head" ng-click="skillfunction($event)">
                                                            <div class="panel-body center icon-panel">
                                                                <img src="Images/search-optical-symbol.svg">
                                                                <div class="text-color icon-text-2">Curiosity</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3 col-md-3 col-sm-3">
                                                        <div class="panel panel-default web-color icon-panel-head" ng-click="skillfunction($event)">
                                                            <div class="panel-body center icon-panel">
                                                                <img src="Images/saturn.svg">
                                                                <div class="text-color icon-text-2">Resourcefulness</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3 col-md-3 col-sm-3">
                                                        <div class="panel panel-default web-color icon-panel-head" ng-click="skillfunction($event)">
                                                            <div class="panel-body center icon-panel">
                                                                <img src="Images/paper-plane.svg">
                                                                <div class="text-color icon-text-2">Hard Working</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3 col-md-3 col-sm-3">
                                                        <div class="panel panel-default web-color icon-panel-head" ng-click="skillfunction($event)">
                                                            <div class="panel-body center icon-panel">
                                                                <img src="Images/group.svg">
                                                                <div class="text-color icon-text-2">Helping Others</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3 col-md-3 col-sm-3">
                                                        <div class="panel panel-default web-color icon-panel-head" ng-click="skillfunction($event)">
                                                            <div class="panel-body center icon-panel">
                                                                <img src="Images/hierarchical-structure.svg">
                                                                <div class="text-color icon-text-2">Organizational</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3 col-md-3 col-sm-3">
                                                        <div class="panel panel-default web-color icon-panel-head" ng-click="skillfunction($event)">
                                                            <div class="panel-body center icon-panel">
                                                                <img src="Images/icon.svg">
                                                                <div class="text-color icon-text-2">Leadership</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3 col-md-3 col-sm-3">
                                                        <div class="panel panel-default web-color icon-panel-head" ng-click="skillfunction($event)">
                                                            <div class="panel-body center icon-panel">
                                                                <img src="Images/human-head-silhouette-with-cogwheels.svg">
                                                                <div class="text-color icon-text-2">Critical thinking</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3 col-md-3 col-sm-3">
                                                        <div class="panel panel-default web-color icon-panel-head" ng-click="skillfunction($event)">
                                                            <div class="panel-body center icon-panel">
                                                                <img src="Images/idea-and-creativity-symbol-of-a-lightbulb.svg">
                                                                <div class="text-color icon-text-2">Creative thinking</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3 col-md-3 col-sm-3">
                                                        <div class="panel panel-default web-color icon-panel-head" ng-click="skillfunction($event)">
                                                            <div class="panel-body center icon-panel">
                                                                <img src="Images/decision-making.svg">
                                                                <div class="text-color icon-text-2">Decision Making</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3 col-md-3 col-sm-3">
                                                        <div class="panel panel-default web-color icon-panel-head" ng-click="skillfunction($event)">
                                                            <div class="panel-body center icon-panel">
                                                                <img src="Images/men-couple-sitting-on-a-table-talking-about-business.svg">
                                                                <div class="text-color icon-text-2">Communication</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>



                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-lg-4 col-md-4 col-sm-12">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                Selected Skills
                                            </div>
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-lg-6" ng-repeat="skill in skills.name track by $index">
                                                        <div class="label-text">
                                                            {{skill}}
                                                            <span class=" glyphicon glyphicon-remove pull-right" ng-click="remove(this)"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </div>

                            </div>

                            <button type="button" class="btn btn-success pull-right" ng-model="second" name="content" ng-click="createCO_Scholastic()">Create Co-Scholastic</button>
                        </uib-tab>

                    </uib-tabset>






                </div>






            </div>

            <div class="card" ng-show="four" ng-cloak>
                <div class="progress1">
                    <div class="circle done">
                        <span class="label">25%</span>
                        <span class="title">Objective</span>
                    </div>
                    <span class="bar done"></span>
                    <div class="circle done">
                        <span class="label">50%</span>
                        <span class="title"> Activity</span>
                    </div>
                    <span class="bar done"></span>
                    <div class="circle done">
                        <span class="label">75%</span>
                        <span class="title">Rubrics</span>
                    </div>
                    <span class="bar done"></span>
                    <div class="circle active">
                        <span class="label">100%</span>
                        <span class="title">Lesson Plan</span>
                    </div>
                </div>
                <div class="container">
                    <div class="panel panel-default web-color text-color">
                        <div class="panel-body layout">
                            <div class="row">
                                <div class="col-lg-3 center">
                                    CBSE
                                </div>
                                <div class="col-lg-3 center">
                                    Taramani School
                                </div>
                                <div class="col-lg-3 center">
                                    Class iii
                                </div>
                                <div class="col-lg-3 center">
                                    Shapes
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <uib-tabset active="activecreatelessonplan" justified="true" class="vertical-layout">
                                <uib-tab index="0" heading="Create Lesson Plan">
                                    <!--//tab content1-->

                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                            <form>
                                                <div class="form-group col-lg-6 col-md-6 col-sm-12">
                                                    <label for="LessonPlan">Write Lesson Plan Name</label>
                                                    <input type="text" class="form-control" placeholder="LessonPlan Name" id="LessonPlan" ng-model="data.lessonplanname">
                                                </div>
                                                <div class="form-group col-lg-6 col-md-6 col-sm-12">
                                                    <label for="duration1">Write Duration</label>
                                                    <input type="text" class="form-control" placeholder="Duration" id="duration1" ng-model="data.lessonplanduration">
                                                </div>
                                                <div class="form-group col-lg-6 col-md-6 col-sm-12">
                                                    <label for="home">Write Home Activity</label>
                                                    <input type="text" class="form-control" placeholder="Home Activity" id="home" ng-model="data.homeactivity">

                                                </div>
                                                <div class="form-group col-lg-6 col-md-6 col-sm-12">

                                                    <label for="Observation">Write Observation</label>
                                                    <input type="text" class="form-control" placeholder="Observation while teaching" id="Observation" ng-model="data.observation">
                                                </div>
                                                <div class='form-group col-lg-4 col-mg-4 col-sm-12'>
                                                    <label for="worksheet">Home Worksheet</label>
                                                    <div class="dropzone" options="dzOptions" callbacks="Worksheets" methods="dzMethods" ng-dropzone></div>
                                                </div>
                                                <div class='form-group col-lg-4 col-mg-4 col-sm-12'>
                                                    <label for="Handouts">Handouts</label>
                                                    <div class="dropzone" options="dzOptions" callbacks="handouts" methods="dzMethods" ng-dropzone></div>
                                                </div>
                                                <div class='form-group col-lg-4 col-mg-4 col-sm-12'>
                                                    <label for="Videos">Videos</label>
                                                    <div class="dropzone" options="dzOptions" callbacks="Video" methods="dzMethods" ng-dropzone></div>
                                                </div>

                                                <button type="button" class="btn btn-success pull-right" ng-click="createLessonplan(data)">Create Lesson Plan</button>

                                            </form>


                                        </div>
                                    </div>

                                </uib-tab>
                                <uib-tab index="1" heading="View Lesson Plan" select= "fetchLessonPlan()"  ng-cloak>
                                    <div class="row">
                                        <div class="col-lg-3 col-md-3 col-sm-12" ng-repeat="lessonPlan in lessonPlan">
                                            <div class="panel panel-default">
                                                <div class="panel-heading lessonplan-card">
                                                    <div class="heading pull-left">  {{lessonPlan.lessonplan_name}}</div>

                                                    <div class="icon-tools pull-right">

                                                        <span class="glyphicon glyphicon-modal-window" ng-click="openlessonplanmodel(this)"></span>


                                                    </div>
                                                </div>
                                                <div class="panel-body">

                                                    <div class="lessonplan-text"> 
                                                        <span class="pull-left">{{lessonPlan.lessonplan_class}} Class</span>

                                                    </div>
                                                    <div class="lessonplan-text"> 
                                                        <span class="pull-left">{{lessonPlan.lessonplan_subject}}</span>
                                                        <span class="pull-right">{{lessonPlan.lessonplan_chapter}}</span>
                                                    </div>

                                                    <div class="description">
                                                        <p>{{lessonPlan.lessonplan_homeactivity}}</p>
                                                        <!--<p> Home Activity: Color objects of different shapes </p>-->
                                                    </div><!--
                                                    -->
                                                </div>

                                            </div>
                                        </div>


                                    </div>
                                </uib-tab>
                            </uib-tabset>
                        </div>
                    </div>


                </div>




            </div>
        </div>

        <script type="text/ng-template" id="rubricmodel.html">
            <div class="modal-header">
            <h3 class="modal-title">I'm a modal!</h3>
            </div>
            <div class="modal-body">
            <p>Do You really want to move forward? You have not created ant Ch-Scholastic Rubric.</p>
            </div>
            <div class="modal-footer">
            <button class="btn btn-primary" type="button" ng-click="ok()">Yes</button>
            <button class="btn btn-warning" type="button" ng-click="cancel()">No</button>
            </div>
        </script>


        <script type="text/ng-template" id="lessonplanmodel.html">
            <div class="modal-header">
            <h3 class="modal-title">{{lessonmodel.lessonplan_name}}</h3>
            </div>
            <div class="modal-body">
            <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
            <uib-tabset active="activeJustified" justified="true">
            <uib-tab index="0" heading="Basic Content">
            <div class="col-lg-12 col-md-12 col-sm-12 icon-tools">
            <span class="pull-left"> Curicullum: {{lessonmodel.lessonplan_curicullum}}</span>
            <span class="pull-right">{{lessonmodel.lessonplan_class}} Class</span>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 icon-tools">
            <span class="pull-left">Subject:  {{lessonmodel.lessonplan_subject}}</span>
            <span class="pull-right">Chapter: {{lessonmodel.lessonplan_chapter}}</span>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 icon-tools">
            <span>Objective Name: {{lessonmodel.lessonplan_objective_name}}</span>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12">
            <p>Objective Desctiption {{lessonmodel.lessonplan_objective_descp}}</p> 
            </div>

            <div class="col-lg-12 col-md-12 col-sm-12 center">

            <div class="col-lg-4 col-md-4 col-sm-12 center">
            <a href="DownloadFile?mypath={{lessonmodel.lessonplan_hadouts}}">
            <div class="donwload-card center">
            <span class="glyphicon glyphicon-tag"></span>

            Handouts
            </div>
            </a>
            </div>

            <div class="col-lg-4 col-md-4 col-sm-12 center">
            <a href="DownloadFile?mypath={{lessonmodel.lessonplan_worksheet}}">
            <div class="donwload-card center">
            <span class="glyphicon glyphicon-tag"></span>

            Worksheets
            </div>
            </a>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 center">
            <a>
            <div class="donwload-card center">
            <span class="glyphicon glyphicon-tag"></span>

            Video
            </div>
            </a>
            </div>
            </div>




            </uib-tab>
            <uib-tab index="1" heading="Activity Content">
            <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 icon-tools">
            <span class="pull-left"> Activity Name:  {{lessonmodel.lessonplan_activity_name}}</span>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 icon-tools">
            <span class="pull-left"> HomeActivity: {{lessonmodel.lessonplan_homeactivity}}</span>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 icon-tools">
            <p>Activity Description: {{lessonmodel.lessonplan_activity_descp}}</p>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 center col-lg-offset-4 col-lg-offset-4">
            <a href="DownloadFile?mypath={{lessonmodel.lessonplan_hadouts}}">
            <div class="donwload-card center">
            <span class="glyphicon glyphicon-tag"></span>
            Activity
            </div>
            </a>
            </div>


            </div>



            </uib-tab>

            </uib-tabset>
            </div>





            </div>
            </div>
            <div class="modal-footer">
            <button class="btn btn-primary" type="button" ng-click="ok()">Yes</button>
            <button class="btn btn-warning" type="button" ng-click="cancel()">No</button>
            </div>
        </script>








        <script type="text/javascript" src="js/ui-bootstrap-tpls-1.3.3.min.js"></script>
        <script type="text/javascript" src="js/app.js"></script>
        <script type="text/javascript" src="js/rpTable.js"></script>
        <script type='text/javascript' src='js/dropzone.min.js'></script>
        <script type='text/javascript' src='js/ngDropzone.js'></script>
        <script type="text/javascript" src="js/Services/CommonService.js"></script>
        <script type="text/javascript" src="js/Services/ObjectiveService.js"></script>
        <script type="text/javascript" src="js/angular-animate.min.js"></script>
        <script type="text/javascript" src="js/toast.js"></script>
        <script type="text/javascript" src="js/Services/LocalStorage.js"></script>
        <script type="text/javascript" src="js/Services/ValidationService.js"></script>
        <script type="text/javascript" src="js/Services/ActivityService.js"></script>
        <script type="text/javascript" src="js/Services/RubricServivce.js"></script>
        <script type="text/javascript" src="js/Services/LessonPlanService.js"></script>




    </body>
</html>
