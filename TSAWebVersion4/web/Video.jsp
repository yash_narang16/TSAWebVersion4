<%-- 
    Document   : Video
    Created on : Jul 5, 2016, 1:38:37 PM
    Author     : Yash
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <script type='text/javascript' src='js/loader.js'></script>
        <link rel="stylesheet" type="text/css" href="css/loader.css">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">

        <link rel="stylesheet" type="text/css" href="css/newcss.css">
        <link rel="stylesheet" type="text/css" href="css/video.css">
        <link rel="stylesheet" type="text/css" href="css/Toast.css">
        <!--<link href='https://fonts.googleapis.com/css?family=Raleway:400,500,800' rel='stylesheet' type='text/css'>-->

        <link rel="stylesheet" type="text/css" href="css/dropzone.min.css">
    </head>
    <body ng-app="video" ng-controller="videoctrl">
        <nav class="navbar navbar-default web-color navbar-fixed-top">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand text-color" href="#">
                        <!--<img alt="Brand" src="Images/logo.png">-->
                        Teacher Support Application
                    </a>
                </div>
            </div>
        </nav>
        <div class="container" style="padding-top:60px;">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <uib-tabset active="activeJustified" justified="true" class="vertical-layout">
                        <uib-tab index="0" heading=" Create Activity">
                            <!--//tab content1-->

                            <div class="row">
                                <div class="form-group col-lg-6 col-md-6 col-sm-12">
                                    <label for="curiicullum">Choose Curricullum</label>
                                    <select class="form-control" id="curiicullum" ng-model="curi.curicullumid[0]" ng-options="curi.curicullum for curi in curicullum" ng-change="Fetch_class(curi.curicullumid[0])" required>
                                        <option disabled selected value="">Choose your option</option>

                                    </select>
                                </div>
                                <div class="form-group col-lg-6 col-md-6 col-sm-12">
                                    <label for="Class">Choose Class</label>
                                    <select class="form-control" id="Class" ng-model="class.class_id[0]" ng-options="class.class_name for class in class" ng-change="Fetch_Subject(class.class_id[0])" required>
                                        <option disabled selected value="">Choose your option</option>
                                    </select>
                                </div>
                                <div class="form-group col-lg-6 col-md-6 col-sm-12">
                                    <label for="Subject">Choose Subject</label>
                                    <select class="form-control" id="Subject" ng-model="subject.subject_id[0]" ng-options="subject.subject_name for subject in subject" ng-change="Fetch_chapter(subject.subject_id[0])" required>
                                        <option disabled selected value="">Choose Subject</option>
                                        <option ng:repeat="subject in subject" value="{{subject.subject_id}}">{{subject.subject_name}}</option>

                                    </select>

                                </div>

                                <div class="form-group col-lg-6 col-md-6 col-sm-12">
                                    <label for="Chapter">Choose Chapter</label>
                                    <select class="form-control" id="Chapter" ng-model="chapter.chapter_id[0]" ng-options="chapter.chapter_name for chapter in chapter" ng-change="Select_chapter(chapter.chapter_id[0])" required>
                                        <option disabled selected value="">Choose Chapter</option>
                                    </select>

                                </div>
                                <div class="form-group col-lg-6 col-md-6 col-sm-12">
                                    <label for="Name">Write Name</label>
                                    <input type="text" class="form-control" placeholder="Video Name" id="Name" ng-model="data.videoname">

                                </div>

                                <div class='form-group col-lg-6 col-mg-6 col-sm-12'>
                                    <label for="A1Description">Videos Upload</label>
                                    <div class="dropzone" options="dzOptions" callbacks="dzCallbacks" methods="dzMethods" ng-dropzone></div>
                                </div>

                                <button type="button" class="btn btn-success pull-right" ng-click="createvideo(data)">Upload</button>
                                </form>


                            </div>


                        </uib-tab>
                        <uib-tab index="1" heading="Use Activity" select="fetch_video()" ng-cloak>
                            <div class="container">
                                <div class="row">
                                    <form>
                                        <div class="form-group col-lg-6 col-md-6 col-sm-12">
                                            <label for="Class">Choose Class</label>
                                            <select class="form-control" id="Class">
                                                <option disabled selected>1</option>
                                                <option>2</option>
                                                <option>3</option>
                                                <option>4</option>
                                                <option>5</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-lg-6 col-md-6 col-sm-12">
                                            <label for="Subject">Choose Subject</label>
                                            <select class="form-control" id="Subject">
                                                <option disabled selected>1</option>
                                                <option>2</option>
                                                <option>3</option>
                                                <option>4</option>
                                                <option>5</option>
                                            </select>
                                        </div>
                                    </form>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <div class="video-heading">
                                            Chapter Name
                                        </div>
                                        <div class="video-content" ng-repeat="video in videodata" ng-click="openvideomodal(this)">
                                            <div class="video-text">
                                                {{video.videoname}}
                                            </div>
                                            <div class="video-icon">
                                                <img src="Images/screen-player.png">
                                            
                                            </div>
                                          
                                        </div>

                                    </div>

                                </div>

                            </div>
                        </uib-tab>

                    </uib-tabset>
                </div>
            </div>


        </div>
<script type="text/ng-template" id="videomodel.html">
        <div class="modal-header">
            <h3 class="modal-title">{{videoname}}</h3>
        </div>
        <div class="modal-body">
           <video width="580" height="200" controls>
                                                <source ng-src="{{videomodel}}" type="video/mp4">
                                                <source ng-src="{{videomodel}}" type="video/ogg">
                                              
                                            </video>
        </div>
        <div class="modal-footer">
          
            <button class="btn btn-warning" type="button" ng-click="cancel()">Cancel</button>
        </div>
        </script>



        <script type="text/javascript" src="js/angular.js"></script>
        <script type="text/javascript" src="js/ui-bootstrap-tpls-1.3.3.min.js"></script>
        <script type="text/javascript" src="js/video.js"></script>
        <script type='text/javascript' src='js/dropzone.min.js'></script>
        <script type='text/javascript' src='js/ngDropzone.js'></script>
        <script type="text/javascript" src="js/Services/CommonService.js"></script>
        <script type='text/javascript' src='js/angular-animate.min.js'></script>
        <script type='text/javascript' src='js/toast.js'></script>
        <script type='text/javascript' src='js/Services/ObjectiveService.js'></script>
        <script type='text/javascript' src='js/Services/ValidationService.js'></script>
        <script type="text/javascript" src="js/Services/VideoService.js"></script>
    </body>
</html>
