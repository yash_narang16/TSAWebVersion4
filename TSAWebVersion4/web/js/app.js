/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var app = angular.module('myModule', ['ui.bootstrap', 'thatisuday.dropzone', 'rpTable', 'Services.Commonservice', 'ngAnimate', 'toastr', 'Services.ObjectiveService', 'Services.ValidationService', 'Services.ActvityService', 'Services.RubricService','Service.LessonPlanService','Services.localstorage'])
app.controller('main', function ($scope, $rootScope ,$CommonService, toastr, $ObjectiveService, $ValidationService, $ActivityService, $RubricService,$LessonPlanService,$LocalStorage,$uibModal) {
    $scope.one = true;
    $scope.two = false;
    $scope.three = false;
    $scope.four = false;

    $scope.showtwo = function () {
$scope.one = false;
                    $scope.two = true;
                    $scope.three = false;
                    $scope.four = false;
    }
    $scope.showthree = function () {
        $scope.one = false;
        $scope.two = false;
        $scope.three = true;
        $scope.four = false;
    }
    $scope.showfour = function () {
        $scope.one = false;
        $scope.two = false;
        $scope.three = false;
        $scope.four = true;
    }
    
    $rootScope.$on("brodcastfunction",function(){
        
        $scope.showfour();
    })
    
    $scope.radioModel = 'Holistic';
    $scope.content = 'option1';
    $scope.check1 = false;
    $scope.check2 = false;
    $scope.choosetype = function (item) {
        var ans = item.currentTarget.getAttribute("data-variable")
        if (ans == "0") {
            console.log("0");
            $scope.check1 = !$scope.check1;
        } else
        {
            console.log("1");
            $scope.check2 = !$scope.check2;
        }

    }
    //dynamic input fields






$scope.loaderhide=true;
    //first page angular code
    var response = $CommonService.fetchcuricullum();
    console.log(response);
    response.then(function (data) {
        console.log(data);
        $scope.curicullum = data;
        $scope.loaderhide=false;

    });


    $scope.Fetch_class = function (val) {
        $scope.curicullumvalue = val.curicullumid;

        console.log(val);
        var response = $CommonService.fetchClass();
        response.then(function (data) {
            console.log(data)
            $scope.class = data;
        })

    }


    $scope.Fetch_Subject = function (val1) {
        $scope.classvalue = val1.class_id;

        var response = $CommonService.fetchSubject();
        response.then(function (data) {
            console.log(data);
            $scope.subject = data;
        })
    }


    $scope.Fetch_chapter = function (val2) {
        $scope.subjectvalue = val2.subject_id;
        val2 = $scope.subjectvalue;
        console.log($scope.subjectvalue);
        var classid = $scope.classvalue;
        var resposne = $CommonService.fetchChapter(val2, classid, $scope.curicullumvalue);
        resposne.then(function (data) {
            console.log(data)
            if (data[0].status == "no") {
                toastr.error("No Record Found, Please select other option", "Error");
            } else {
                $scope.chapter = data;
            }
        })
    }

    $scope.Select_chapter = function (val) {
        $scope.chaptervalue = val.chapter_id;
    }


    $scope.dzOptions = {
        url: 'UploadFile', //servlet name
        paramName: 'photo',
        maxFilesize: '10', //Size
        addRemoveLinks: true,
    };
    $scope.dzCallbacks = {
        'addedfile': function (file) {
            console.log(file);
            $scope.newFile = file;
        },
        'success': function (file, xhr) {
            var result = JSON.parse(xhr);     //response from servlet after upload
            if (result.responseCode == 1) {
                toastr.success("File Uploaded Successfully", "Success");
                console.log(result.imagePath);
                $scope.imagepath = result.imagePath;
            } else {
                $scope.imagepath = "";
            }
        },
    };
    $scope.handouts = {
        'addedfile': function (file) {
            console.log(file);
            $scope.newFile = file;
        },
        'success': function (file, xhr) {
            var result = JSON.parse(xhr);     //response from servlet after upload
            if (result.responseCode == 1) {
                toastr.success("File Uploaded Successfully", "Success");
//                console.log(result.imagePath);
                $scope.handoutsPath = result.imagePath;
                console.log($scope.handoutsPath);
            } else {
                $scope.handoutsPath = "";
            }
        },
    };
    $scope.Worksheets = {
        'addedfile': function (file) {
            console.log(file);
            $scope.newFile = file;
        },
        'success': function (file, xhr) {
            var result = JSON.parse(xhr);     //response from servlet after upload
            if (result.responseCode == 1) {
                toastr.success("File Uploaded Successfully", "Success");
//                console.log(result.imagePath);
                $scope.WorksheetPath = result.imagePath;
                console.log($scope.WorksheetPath);
            } else {
                $scope.WorksheetPath = "";
            }
        },
    };
$scope.Video = {
        'addedfile': function (file) {
            console.log(file);
            $scope.newFile = file;
        },
        'success': function (file, xhr) {
            var result = JSON.parse(xhr);     //response from servlet after upload
            if (result.responseCode == 1) {
                toastr.success("File Uploaded Successfully", "Success");
//                console.log(result.imagePath);
                $scope.VideoPath = result.imagePath;
                console.log($scope.VideoPath);
            } else {
                $scope.VideoPath = "";
            }
        },
    };
$scope.objectives=[];

    $scope.createObjective = function (obj) {
        $scope.obj = {
            objective_name: obj.objective_name,
            objective_description: obj.objective_description
        };
        console.log($ValidationService.checkName($scope.obj.objective_name));



        if ($ValidationService.checkselect($scope.chaptervalue) && $ValidationService.checkselect($scope.subjectvalue) && $ValidationService.checkselect($scope.classvalue) && $ValidationService.checkselect($scope.curicullumvalue) && $ValidationService.checkName($scope.obj.objective_name)) {
            var response = $ObjectiveService.createObjective($scope.curicullumvalue, $scope.classvalue, $scope.subjectvalue, $scope.chaptervalue, $scope.obj.objective_name, $scope.obj.objective_description);
            response.then(function (data) {
                console.log(data);
                if (data.status == "Yes") {
                    toastr.success("Objective Created Sucessfully", "Success");
                    //next step of form
                    
                     $scope.fetch_Objective();

                    

                    $scope.select_object = function (val) {
                        console.log(val);
                        $scope.objective_value = val.objective_id;
                        $scope.objective_curi = val.objective_curiid;
                        $scope.objective_class = val.objective_classid;
                        $scope.objective_subject = val.objective_subjectid;
                        $scope.objective_chapter = val.objective_chapterid;
                    }
                }
                else{
                    
                    toastr.error("Something went wrong", "error");
                }
            })
        }
    }
    
   
    $scope.fetch_Objective=function(){
        

        var objective_fetch = $ObjectiveService.FetchObjective();  //fethcing objective from DB
                    objective_fetch.then(function (data) {
                        console.log(data);
                        $scope.objective = data;
                    })   
    }
    
    $scope.objectiveselected=function(val){
        console.log(val.objective.objective_id);
        $scope.lessonplanObjective=val.objective.objective_id;
        $scope.showtwo();
       toastr.success("You have Choose a valid Objective", "Success");
    }
                   

                    //file upload





                    //end of file uplod
                    $scope.saveActivity = function (obj)
                    {
                        $scope.obj = {
                            name: obj.name,
                            material: obj.material,
                            duration: obj.duration,
                            description: obj.description
                        }

                        var response = $ActivityService.saveActivity($scope.objective_curi, $scope.objective_class, $scope.objective_subject, $scope.objective_chapter, obj.name, obj.material, obj.duration, $scope.imagepath, obj.description);
                        response.then(function (data) {

                            console.log(data);
                            if (data.status == "Yes") {
                                toastr.success("Activity Created Successfully", "Success");
                                $scope.showthree();
                            } else {

                                toastr.error("Something Went Wrong!!", "Error");
                            }

                        })
                    

                }
                
                
                $scope.useActivity=function()
            {
                var response=$ActivityService.fetchActivity();
                response.then(function(data){
                    console.log(data);
                    $scope.activity=data;
                    
                })
                
                
            }  
            
            
            $scope.activitySelected=function(val){
                console.log(val.activity.activity_id);
                $scope.lessonplanActivity=val.activity.activity_id;
                $scope.showthree();
                toastr.success("You have Choose a valid Actvity", "Success");
            }
                
        
    





    //rubric code



    var parameter1 = ['Garde A', 'Grade B', 'Grade C', 'Grade D', 'Grade E'];
    var parameter2 = ['Below Average', 'Average', 'Good', 'very Good', 'Excellent'];
    var parameter3 = ['Inadequate', 'Marginal', 'Fair', 'Good', 'Excellent'];



    $scope.attributeCount = function (val) {
        console.log(val);
        $scope.count = val;
        $scope.scale = "";
    }

    $scope.attributename = function (val) {

        $scope.xlist = {
            'name': []
        };
        for (var i = 0; i < $scope.count; i++)
        {
            if (val == 1) {
                $scope.xlist.name.push(parameter1[i]);
            } else if (val == 2) {
                $scope.xlist.name.push(parameter2[i]);
            } else {
                $scope.xlist.name.push(parameter3[i]);
            }

        }

    }

    $scope.inputs = {
        parameter_name: [],
    };
    $scope.ParameterCount = function (val) {
        $scope.parameter_count = val;

        for (var i = 0; i < val; i++) {
            $scope.inputs.parameter_name.push(" ");
        }

    }



    $scope.createScholastic = function () {
        console.log($scope.inputs);
        var rubric_type;

        if ($scope.radioModel === "Holistic") {
            rubric_type = 2;
            $scope.parameter_count = 1;
            $scope.inputs.parameter_name.push("Defaultname");
            $scope.Parameter_name = $scope.inputs;


        } else {
            rubric_type = 1;
            $scope.Parameter_name = ($scope.inputs);
        }
        
        
        
        console.log($scope.xlist);
        console.log($scope.Parameter_name);
        var list = JSON.stringify($scope.xlist);
        var parameter_list = JSON.stringify($scope.Parameter_name);
        
        
        if($ValidationService.checkselect(list)&&$ValidationService.checkselect(parameter_list)&&$ValidationService.checkselect(rubric_type)&&$ValidationService.checkselect($scope.parameter_count)&&$ValidationService.checkselect($scope.count))
        {
        var response = $RubricService.saveRubric(rubric_type, list, $scope.count, $scope.parameter_count, parameter_list);
        response.then(function (data) {
            console.log(data);
            $scope.scholastic=data;
            if(data!=null){
             toastr.success("You have Created a scholatic rubric", "Success");
             console.log($scope.flag)
            if($scope.flag==false){
                $scope.open("sm");
            }
        }
        })
////             
    }
    else{
         toastr.error("Something Went Wrong","Error");
    }
    
    }


    $scope.skills = {
        name: []

    }
    $scope.skillfunction = function ($event) {

        console.log($event.target.innerText);

        $scope.skills.name.push($event.target.innerText);



    }

    $scope.remove = function (val) {
        console.log(val);
        $scope.skills.name.pop(val.$index);
    }


    $scope.flag=false;
    $scope.createCO_Scholastic = function ()
    {
        console.log($scope.skills.name)
        var list = JSON.stringify($scope.skills);
        var count=$scope.skills.name.length;
        var response = $RubricService.saveCoscholastic(list,count)
        response.then(function (data) {
            console.log(data);
            $scope.coScholastic=data;
            if($scope.coScholastic!=null){
                $scope.flag=true;
            }
            if($scope.coScholastic1=null&&$scope.scholastic!=null){
                $scope.showfour();
            }
//$scope.showfour();
        })
    }
 
     $scope.open = function (size) {

    var modalInstance = $uibModal.open({
      animation: true,
      templateUrl: 'rubricmodel.html',
      controller: 'rubricmodelctrl',
      size: size,
      resolve: {
        items: function () {
          return 
        }
      }
      
  })
     }
     
       
     
    
   
    
    
    
    
    
    
    
    
   //lesson plan saving code
   
   
   $scope.createLessonplan=function(data){
       $scope.data={
           lessonplanname:data.lessonplanname,
           lessonplanduration:data.lessonplanduration,
           homeactivity:data.homeactivity,
           observation:data.observation
           
       }
                var userid=JSON.parse($LocalStorage.getUserDetails("userinfo") || '{}');
       console.log(userid);
       if($ValidationService.checkselect($scope.data.lessonplanname)&&$ValidationService.checkselect($scope.data.lessonplanduration)&&$ValidationService.checkselect($scope.data.homeactivity)&&$ValidationService.checkselect($scope.data.observation)&&$ValidationService.checkselect(userid)){
       
       var response=$LessonPlanService.saveLessonPlan($scope.data.lessonplanname,$scope.data.lessonplanduration,$scope.data.homeactivity,$scope.data.observation,$scope.lessonplanObjective,$scope.lessonplanActivity,userid.user_id,$scope.WorksheetPath,$scope.handoutsPath,$scope.VideoPath)
       response.then(function(data){
           console.log(data);
           
           
       })
       
   }
   else{
       
        toastr.error("Something Went Wrong","Error");
   }
   }
   
   
   $scope.fetchLessonPlan=function(){
       var response = $LessonPlanService.fetchLessonPlan();
       response.then(function(data){
           console.log(data);
           $scope.lessonPlan=data;
           
       })
       
       
   }
   
   
   
   
    $scope.openlessonplanmodel = function (val) {

    var modalInstance = $uibModal.open({
      animation: true,
      templateUrl: 'lessonplanmodel.html',
      controller: 'lessonplanctrl',
     
      resolve: {
        items: function () {
          return val;
        }
      }
      
  })
     }




























    //end of first page   





         
    
    









})


        .controller("rubricmodelctrl",function($scope,$uibModalInstance,$rootScope){
            $scope.ok = function () {
                $rootScope.$emit("brodcastfunction",{});
    $uibModalInstance.close();
  };

  $scope.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };
            
        })
        
        
        .controller("lessonplanctrl",function($scope,$uibModalInstance,$rootScope,items){
            $scope.lessonmodel=items.lessonPlan;
            console.log($scope.lessonmodel)
            
            $scope.ok = function () {
            
    $uibModalInstance.close();
  };

  $scope.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };
            
        })




