/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var app=angular.module('app',['Services.loginservice',"Services.localstorage","ngAnimate","toastr"])
          app.controller('login', ['$scope', '$location', '$anchorScroll','$Loginservice',"$LocalStorage","toastr",
  function ($scope, $location, $anchorScroll,$Loginservice,$LocalStorage,toastr, $rootscope) {
      
    $scope.gotoBottom = function() {
      // set the location.hash to the id of
      // the element you wish to scroll to.
      $location.hash('login');

      // call $anchorScroll()
      $anchorScroll();
    };
    
    $scope.userLogin=function(user){     //userlogin method 
      //getting values from front -end
        $scope.user={
            username:user.username,
            password:user.password
        };
        
        console.log($scope.user.username+"1"+$scope.user.password);
   
        var response=$Loginservice.validateLogin($scope.user.username,$scope.user.password);        //calling service function for backend
        
        //returning promise so coverting into json object directly
        response.then(function(data){
            console.log(data)
         if(data.status=="Yes"){
            $LocalStorage.setUserDetails("userinfo",data);   //storing into localstroge for this check localstorag service
            
           window.location="Home.jsp";
          
           
            var test=$LocalStorage.getUserDetails("userinfo");        //getting values from localstorage fro  getUserDetails method
          console.log(JSON.parse(test || '{}'));
      }
      else{
          toastr.error('Your credentials are wrong', 'Error');
          
      }
        })
    }
    
    
  }]);

