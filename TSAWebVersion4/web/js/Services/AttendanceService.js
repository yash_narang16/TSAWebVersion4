/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


angular.module("Services.AttendanceService",[])

        .service("$AttendanceService",function($http,$q){
            var AttendanceService={};
    var fetch_act="fetch";
    var save_act="save";
    AttendanceService.fetch_attendance= function(classid){
            var defer = $q.defer();
                $http({
                    url: 'AttendanceServlet',
                    method: 'post',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    transformRequest: function (obj) {
                        var str = [];
                        for (var i in obj) {
                            str.push(encodeURIComponent(i) + "=" + encodeURIComponent(obj[i]));
                        }
                        return str.join('&');
                    },
                    data: {
                        act: fetch_act,
                classid:classid
                    }
                }).success(function (data) {
                    console.log(data);

                    defer.resolve(data);
                }).error(function (data, status) {


                })
                return defer.promise;



            }
        
        
        
        
        
        AttendanceService.save_attendance= function(studentdata){
            var defer = $q.defer();
                $http({
                    url: 'AttendanceServlet',
                    method: 'post',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    transformRequest: function (obj) {
                        var str = [];
                        for (var i in obj) {
                            str.push(encodeURIComponent(i) + "=" + encodeURIComponent(obj[i]));
                        }
                        return str.join('&');
                    },
                    data: {
                        act: save_act,
                studentdata:studentdata
                    }
                }).success(function (data) {
                    console.log(data);

                    defer.resolve(data);
                }).error(function (data, status) {


                })
                return defer.promise;



            }
            
            
            
            AttendanceService.view_attendance= function(){
            var defer = $q.defer();
                $http({
                    url: 'ViewAttendance',
                    method: 'post',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    transformRequest: function (obj) {
                        var str = [];
                        for (var i in obj) {
                            str.push(encodeURIComponent(i) + "=" + encodeURIComponent(obj[i]));
                        }
                        return str.join('&');
                    },
                    data: {
                        act: fetch_act,
               
                    }
                }).success(function (data) {
                    console.log(data);

                    defer.resolve(data);
                }).error(function (data, status) {


                })
                return defer.promise;



            }
        
    
    
            
            
            
            
    return AttendanceService;        
        })