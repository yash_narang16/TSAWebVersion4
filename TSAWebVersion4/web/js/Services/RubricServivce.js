/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


angular.module('Services.RubricService', [])
        .service('$RubricService', function ($http, $q) {
            var RubricService = {};


            var save = "save";

            RubricService.saveRubric = function (rubric_type, attributeList, count, parameter_count, parameter_name) {
                var defer = $q.defer();
                $http({
                    url: 'RubricServlet',
                    method: 'post',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    transformRequest: function (obj) {
                        var str = [];
                        for (var i in obj) {
                            str.push(encodeURIComponent(i) + "=" + encodeURIComponent(obj[i]));
                        }
                        return str.join('&');
                    },
                    data: {
                        act: save,
                        List: attributeList,
                        Type: rubric_type,
                        count: count,
                        parameter_count: parameter_count,
                        parameter_name: parameter_name
                    }
                    ,
                    dataType: 'json'


                }).success(function (data) {
                    console.log(data);

                    defer.resolve(data);
                }).error(function (data, status) {


                })
                return defer.promise;


            }



            RubricService.saveCoscholastic = function (list, count) {
                console.log(list,count);
                var defer = $q.defer();
                $http({
                    url: 'Co_ScholasticServlet',
                    method: 'post',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    transformRequest: function (obj) {
                        var str = [];
                        for (var i in obj) {
                            str.push(encodeURIComponent(i) + "=" + encodeURIComponent(obj[i]));
                        }
                        return str.join('&');
                    },
                    data: {
                        act: save,
                        List: list,
                        Count: count

                    }
                    ,
                    dataType: 'json'


                }).success(function (data) {
                    console.log(data);

                    defer.resolve(data);
                }).error(function (data, status) {


                })
                return defer.promise;


            }





            return RubricService;
        })