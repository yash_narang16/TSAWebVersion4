/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


angular.module('Service.MappingService', [])
        .service("$MappingService", function ($http, $q) {
            var MappingService = {};
            var act = "map";
            MappingService.maplessonplan = function (date, lessonplanid) {
console.log(date+"service");
                var defer = $q.defer();
                $http({
                    url: 'LessonplanMapping',
                    method: 'post',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    transformRequest: function (obj) {
                        var str = [];
                        for (var i in obj) {
                            str.push(encodeURIComponent(i) + "=" + encodeURIComponent(obj[i]));
                        }
                        return str.join('&');
                    },
                    data: {
                        act: act,
                        date: date,
                        id: lessonplanid

                    }
                }).success(function (data) {
                    console.log(data);

                    defer.resolve(data);
                }).error(function (data, status) {


                })
                return defer.promise;


            }



            var fetch_act = "fetch";
            MappingService.fetchmappedlesson = function () {


                var defer = $q.defer();
                $http({
                    url: 'LessonplanMapping',
                    method: 'post',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    transformRequest: function (obj) {
                        var str = [];
                        for (var i in obj) {
                            str.push(encodeURIComponent(i) + "=" + encodeURIComponent(obj[i]));
                        }
                        return str.join('&');
                    },
                    data: {
                        act: fetch_act,
                    }
                }).success(function (data) {
                    console.log(data);

                    defer.resolve(data);
                }).error(function (data, status) {


                })
                return defer.promise;





            }






            return MappingService;
        })


    