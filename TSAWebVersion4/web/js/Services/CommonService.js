/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
angular.module("Services.Commonservice", [])
        .service("$CommonService", function ($http, $q) {
            var CommonService = {};

            var act = "fetch";  //common variable for fetching
            
            
            //Fetching Curicullum data from DB
            CommonService.fetchcuricullum = function () {

                var defer = $q.defer();
                $http({
                    url: 'CuricullumServlet',
                    method: 'post',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    transformRequest: function (obj) {
                        var str = [];
                        for (var i in obj) {
                            str.push(encodeURIComponent(i) + "=" + encodeURIComponent(obj[i]));
                        }
                        return str.join('&');
                    },
                    data: {
                        act: act

                    }
                }).success(function (data) {
                    console.log(data);

                    defer.resolve(data);
                }).error(function (data, status) {


                })
                return defer.promise;
            }

        //Fetching Class data from DB
        CommonService.fetchClass = function () {

                var defer = $q.defer();
                $http({
                    url: 'ClassServlet',
                    method: 'post',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    transformRequest: function (obj) {
                        var str = [];
                        for (var i in obj) {
                            str.push(encodeURIComponent(i) + "=" + encodeURIComponent(obj[i]));
                        }
                        return str.join('&');
                    },
                    data: {
                        act: act

                    }
                }).success(function (data) {
                    console.log(data);

                    defer.resolve(data);
                }).error(function (data, status) {


                })
                return defer.promise;
            }



CommonService.fetchSubject = function () {

                var defer = $q.defer();
                $http({
                    url: 'SubjectServlet',
                    method: 'post',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    transformRequest: function (obj) {
                        var str = [];
                        for (var i in obj) {
                            str.push(encodeURIComponent(i) + "=" + encodeURIComponent(obj[i]));
                        }
                        return str.join('&');
                    },
                    data: {
                        act: act

                    }
                }).success(function (data) {
                    console.log(data);

                    defer.resolve(data);
                }).error(function (data, status) {


                })
                return defer.promise;
            }


CommonService.fetchChapter = function (subject, classid,curiculumid) {

                var defer = $q.defer();
                $http({
                    url: 'ChapterServlet',
                    method: 'post',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    transformRequest: function (obj) {
                        var str = [];
                        for (var i in obj) {
                            str.push(encodeURIComponent(i) + "=" + encodeURIComponent(obj[i]));
                        }
                        return str.join('&');
                    },
                    data: {
                        act: act,
                       subject:subject,
                       class:classid,
                       curicullum:curiculumid

                    }
                }).success(function (data) {
                    console.log(data);

                    defer.resolve(data);
                }).error(function (data, status) {


                })
                return defer.promise;
            }






            return CommonService;
        })

