/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
angular.module("Services.localstorage",[])
        .service("$LocalStorage",function(){
            
            var LocalStorage={};
    
    LocalStorage.setUserDetails= function(userkey,userdetails){
        
      window.localStorage.setItem(userkey,JSON.stringify(userdetails));
        return this;
    }
    
    LocalStorage.getUserDetails=function(userkey){
        return window.localStorage.getItem(userkey);
    }
    
    
    
    return LocalStorage;
            
            
        });

