<%-- 
    Document   : index
    Created on : Jun 28, 2016, 1:24:43 AM
    Author     : Yash
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>JSP Page</title>
        <script type='text/javascript' src='js/loader.js'></script>
        <link rel="stylesheet" type="text/css" href="css/loader.css">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/progressbar.css">
        <link rel="stylesheet" type="text/css" href="css/newcss.css">
        <!--<link href='https://fonts.googleapis.com/css?family=Raleway:400,500,800' rel='stylesheet' type='text/css'>-->

        <link rel="stylesheet" type="text/css" href="css/dropzone.min.css">
        <link rel="stylesheet" type="text/css" href="css/rpTable.css">
        <link rel="stylesheet" type="text/css" href="css/Toast.css">

    </head>
    <body ng-app="activity" ng-controller="main">
        <nav class="navbar navbar-default web-color navbar-fixed-top">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand text-color" href="#">
                        <!--<img alt="Brand" src="Images/logo.png">-->
                        Teacher Support Application
                    </a>
                </div>
            </div>
        </nav>
        <div class="container" style="padding-top:60px;">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <uib-tabset active="activeJustified" justified="true" class="vertical-layout">
                        <uib-tab index="0" heading=" Create Activity">
                            <!--//tab content1-->

                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <form>
                                        <div class="form-group col-lg-6 col-md-6 col-sm-12">
                                            <label for="curiicullum">Choose Curricullum</label>
                                            <select class="form-control" id="curiicullum" ng-model="curi.curicullumid[0]" ng-options="curi.curicullum for curi in curicullum" ng-change="Fetch_class(curi.curicullumid[0])">
                                                <option disabled selected value="">Choose your option</option>
                                            
                                            </select>
                                        </div>
                                        <div class="form-group col-lg-6 col-md-6 col-sm-12">
                                            <label for="Class">Choose Class</label>
                                            <select class="form-control" id="Class" ng-model="class.class_id[0]" ng-options="class.class_name for class in class" ng-change="Fetch_Subject(class.class_id[0])">
                                                  <option disabled selected value="">Choose your option</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-lg-6 col-md-6 col-sm-12">
                                            <label for="Subject">Choose Subject</label>
                                            <select class="form-control" id="Subject" ng-model="subject.subject_id[0]" ng-options="subject.subject_name for subject in subject" ng-change="Fetch_chapter(subject.subject_id[0])">
                                              <option disabled selected value="">Choose your option</option>
                                            </select>

                                        </div>
                                        <div class="form-group col-lg-6 col-md-6 col-sm-12">
                                            <label for="Chapter">Choose Chapter</label>
                                                  <select class="form-control" id="Chapter" ng-model="chapter.chapter_id[0]" ng-options="chapter.chapter_name for chapter in chapter" ng-change="Select_chapter(chapter.chapter_id[0])">
                                                 <option disabled selected value="">Choose your option</option>
                                            </select>

                                        </div>
                                        <div class="form-group col-lg-6 col-md-6 col-sm-12">
                                            <label for="AName">Write Name</label>
                                            <input type="text" class="form-control" placeholder="Actvity Name" id="AName" ng-model="obj.name">
                                        </div>
                                        <div class="form-group col-lg-6 col-md-6 col-sm-12">
                                            <label for="Material">Write Material</label>
                                            <input type="text" class="form-control" placeholder="Material Required" id="Material" ng-model="obj.material">
                                        </div>

                                        <div class="form-group col-lg-6 col-md-6 col-sm-12">
                                            <label for="ADescription">How To Do</label>
                                            <textarea class="form-control" rows="4" ng-model="obj.description"></textarea>

                                        </div>
                                        <div class="form-group col-lg-6 col-md-6 col-sm-12">
                                            <label for="Duration">Write Duration</label>
                                            <input type="text" class="form-control" placeholder="Duraton Required" id="Duration" ng-model="obj.duration">

                                        </div>
                                        <div class='form-group col-lg-6 col-mg-6 col-sm-12'>
                                            <label for="A1Description">Class Worksheets</label>
                                            <div class="dropzone" options="dzOptions" callbacks="dzCallbacks" methods="dzMethods" ng-dropzone></div>
                                        </div>
                                        <div class='form-group col-lg-12 col-mg-12 col-sm-12'>
                                            <button type="button" class="btn btn-success pull-right" ng-click="saveActivity(obj)">Save Activity</button>
                                        </div>
                                    </form>


                                </div>
                            </div>

                        </uib-tab>
                        <uib-tab index="1" heading="Use Activity" ng-click="useActivity()">
                           
                        </uib-tab>

                    </uib-tabset>
                </div>
            </div>


        </div>










        <script type="text/javascript" src="js/angular.js"></script>
        <script type="text/javascript" src="js/ui-bootstrap-tpls-1.3.3.min.js"></script>
        <script type="text/javascript" src="js/activity.js"></script>
        <script type="text/javascript" src="js/rpTable.js"></script>
        <script type='text/javascript' src='js/dropzone.min.js'></script>
        <script type='text/javascript' src='js/ngDropzone.js'></script>
        <script type='text/javascript' src='js/angular-animate.min.js'></script>
        <script type='text/javascript' src='js/toast.js'></script>
        <script type='text/javascript' src='js/Services/CommonService.js'></script>
        <script type='text/javascript' src='js/Services/ActivityService.js'></script>
    </body>
</html>
