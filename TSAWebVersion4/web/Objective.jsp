<%-- 
    Document   : index
    Created on : Jun 28, 2016, 1:24:43 AM
    Author     : Yash
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>JSP Page</title>
        <script type='text/javascript' src='js/loader.js'></script>
        <link rel="stylesheet" type="text/css" href="css/loader.css">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/progressbar.css">
        <link rel="stylesheet" type="text/css" href="css/newcss.css">
        <!--<link href='https://fonts.googleapis.com/css?family=Raleway:400,500,800' rel='stylesheet' type='text/css'>-->

        <link rel="stylesheet" type="text/css" href="css/Toast.css">
        <link rel="stylesheet" type="text/css" href="css/rpTable.css">

    </head>
    <body ng-app="objective" ng-controller="mainctrl">
        <nav class="navbar navbar-default web-color navbar-fixed-top">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand text-color" href="#">
                        <!--<img alt="Brand" src="Images/logo.png">-->
                        Teacher Support Application
                    </a>
                </div>
            </div>
        </nav>

        <div class="container" style="padding-top:60px;">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <uib-tabset active="activeJustified" justified="true" class="vertical-layout">
                        <uib-tab index="0" heading=" Create Objective">
                            <!--//tab content1-->

                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <form name="objectiveForm">
                                     <div class="form-group col-lg-6 col-md-6 col-sm-12">
                                                    <label for="curiicullum">Choose Curricullum</label>
                                                    <select class="form-control" id="curiicullum" ng-model="curi.curicullumid[0]" ng-options="curi.curicullum for curi in curicullum" ng-change="Fetch_class(curi.curicullumid[0])" required>
                                                        <option disabled selected value="">Choose your option</option>

                                                    </select>
                                                </div>
                                                <div class="form-group col-lg-6 col-md-6 col-sm-12">
                                                    <label for="Class">Choose Class</label>
                                                    <select class="form-control" id="Class" ng-model="class.class_id[0]" ng-options="class.class_name for class in class" ng-change="Fetch_Subject(class.class_id[0])" required>
                                                        <option disabled selected value="">Choose your option</option>
                                                    </select>
                                                </div>
                                        <div class="form-group col-lg-6 col-md-6 col-sm-12">
                                            <label for="Subject">Choose Subject</label>
                                            <select class="form-control" id="Subject" ng-model="subject.subject_id[0]" ng-options="subject.subject_name for subject in subject" ng-change="Fetch_chapter(subject.subject_id[0])" required>
                                                <option disabled selected value="">Choose Subject</option>
                                                <option ng:repeat="subject in subject" value="{{subject.subject_id}}">{{subject.subject_name}}</option>

                                            </select>

                                        </div>

                                        <div class="form-group col-lg-6 col-md-6 col-sm-12">
                                            <label for="Chapter">Choose Chapter</label>
                                            <select class="form-control" id="Chapter" ng-model="chapter.chapter_id[0]" ng-options="chapter.chapter_name for chapter in chapter" ng-change="Select_chapter(chapter.chapter_id[0])" required>
                                                <option disabled selected value="">Choose Chapter</option>
                                            </select>

                                        </div>
                                        <div class="form-group col-lg-6 col-md-6 col-sm-12">
                                            <label for="Name">Write Objective</label>
                                            <input type="text" class="form-control" name="objective_name" placeholder="Objective" id="Name" ng-model="obj.objective_name" ng-pattern="/^[A-Za-z0-9 ]{1,100}$/" required>
                                            <span ng-show="objectiveForm.objective_name.$touched && objectiveForm.objective_name.$error.required" ng-cloak>Objective Name Required</span>
                                            <span ng-show="objectiveForm.objective_name.$error.pattern" ng-cloak>Only Characters and Numbers</span>
                                        </div>
                                        <div class="form-group col-lg-6 col-md-6 col-sm-12">
                                            <label for="Description">Write Description</label>
                                            <textarea class="form-control" rows="3" name="description" ng-model="obj.objective_description" required></textarea>
 <span ng-show="objectiveForm.description.$touched && objectiveForm.description.$error.required" ng-cloak>Description Required</span>
                                        </div>
                                        <div class="form-group col-lg-12 col-md-12 col-sm-12">
                                            <button type="button" class="btn btn-success pull-right" ng-click="createObjective(obj)" ng-disabled="objectiveForm.$invalid">Save Objective</button>

                                        </div>

                                    </form>


                                </div>
                            </div>

                        </uib-tab>
                        <uib-tab index="1" heading="View Objective" select="fetch_Objective()">
                             <div class="row">
                                                <div class="col-lg-3 col-md-3 col-sm-12" ng-repeat="objective in objective">
                                    <div class="panel panel-default">
                                        <div class="panel-heading lessonplan-card">
                                            <div class="heading pull-left">  {{objective.objective_name}}</div>
                                            <div class="icon-tools pull-right">

                                                <span class="glyphicon glyphicon-modal-window"></span>

                                                <span class="glyphicon glyphicon-tag"></span>
                                            </div>
                                        </div>
                                        <div class="panel-body">
                                         
                                            <div class="lessonplan-text"> 
                                                <span class="pull-left">{{objective.objective_class}} Class</span>
                                                 <span class="pull-right">{{objective.objective_curi}}</span>
                                         
                                            </div>
                                            <div class="lessonplan-text"> 
                                                <span class="pull-left">{{objective.objective_subject}}</span>
                                                <span class="pull-right">{{objective.objective_chapter}}</span>
                                            </div>
                                            <div class="description"> 
                                                <p>{{objective.objective_description}}</p>

                                                <!--<p> Home Activity: Color objects of different shapes </p>-->
                                            </div><!--
                                            -->
                                        </div>

                                    </div>
                                </div>
                         
                                
                            </div>
                        </uib-tab>

                    </uib-tabset>
                </div>
            </div>


        </div>










        <script type="text/javascript" src="js/angular.js"></script>
        <script type="text/javascript" src="js/ui-bootstrap-tpls-1.3.3.min.js"></script>
        <script type="text/javascript" src="js/objective.js"></script>
        <script type="text/javascript" src="js/rpTable.js"></script>
        <script type="text/javascript" src="js/Services/CommonService.js"></script>
        <script type='text/javascript' src='js/angular-animate.min.js'></script>
        <script type='text/javascript' src='js/toast.js'></script>
        <script type='text/javascript' src='js/Services/ObjectiveService.js'></script>
        <script type='text/javascript' src='js/Services/ValidationService.js'></script>



    </body>
</html>
