CREATE DATABASE  IF NOT EXISTS `TSA_V4` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `TSA_V4`;
-- MySQL dump 10.13  Distrib 5.6.22, for osx10.8 (x86_64)
--
-- Host: 127.0.0.1    Database: TSA_V4
-- ------------------------------------------------------
-- Server version	5.6.24

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Attendance_Master`
--

DROP TABLE IF EXISTS `Attendance_Master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Attendance_Master` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_name` text,
  `class_name` int(11) DEFAULT NULL,
  `date_time` datetime DEFAULT NULL,
  `status` int(11) NOT NULL,
  `student_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `class_idx` (`class_name`),
  KEY `student_id_idx` (`student_id`),
  CONSTRAINT `class_name` FOREIGN KEY (`class_name`) REFERENCES `Class_master` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `student_id` FOREIGN KEY (`student_id`) REFERENCES `Student_Master` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=97 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Attendance_Master`
--

LOCK TABLES `Attendance_Master` WRITE;
/*!40000 ALTER TABLE `Attendance_Master` DISABLE KEYS */;
INSERT INTO `Attendance_Master` VALUES (33,'Yash Narang',1,'2016-08-09 10:36:19',1,1),(34,'Haider',1,'2016-08-09 10:36:19',1,2),(35,'Ankit Panth',1,'2016-08-09 10:36:19',0,3),(36,'Harsh',1,'2016-08-09 10:36:19',1,4),(37,'Devan',1,'2016-08-09 10:36:19',1,5),(38,'Sethu',1,'2016-08-09 10:36:19',0,6),(39,'Nagendar Singh',1,'2016-08-09 10:36:19',1,7),(49,'Priyanka',1,'2016-08-09 23:30:54',1,8),(57,'Yash Narang',1,'2016-08-10 00:39:24',1,1),(58,'Haider',1,'2016-08-10 00:39:24',0,2),(59,'Ankit Panth',1,'2016-08-10 00:39:24',1,3),(60,'Harsh',1,'2016-08-10 00:39:24',1,4),(61,'Devan',1,'2016-08-10 00:39:24',1,5),(62,'Sethu',1,'2016-08-10 00:39:24',1,6),(63,'Nagendar Singh',1,'2016-08-10 00:39:24',0,7),(64,'Priyanka Sharma',1,'2016-08-10 00:39:24',0,8),(65,'Yash Narang',1,'2016-08-11 00:07:34',0,1),(66,'Haider',1,'2016-08-11 00:07:34',0,2),(67,'Ankit Panth',1,'2016-08-11 00:07:34',1,3),(68,'Harsh',1,'2016-08-11 00:07:34',0,4),(69,'Devan',1,'2016-08-11 00:07:34',1,5),(70,'Sethu',1,'2016-08-11 00:07:34',1,6),(71,'Nagendar Singh',1,'2016-08-11 00:07:34',0,7),(72,'Priyanka Sharma',1,'2016-08-11 00:07:34',1,8),(73,'Yash Narang',1,'2016-08-11 00:17:48',1,1),(74,'Haider',1,'2016-08-11 00:17:48',1,2),(75,'Ankit Panth',1,'2016-08-11 00:17:48',0,3),(76,'Harsh',1,'2016-08-11 00:17:48',1,4),(77,'Devan',1,'2016-08-11 00:17:48',1,5),(78,'Sethu',1,'2016-08-11 00:17:48',0,6),(79,'Nagendar Singh',1,'2016-08-11 00:17:48',1,7),(80,'Priyanka Sharma',1,'2016-08-11 00:17:48',1,8),(81,'Yash Narang',1,'2016-08-11 00:18:27',1,1),(82,'Haider',1,'2016-08-11 00:18:27',1,2),(83,'Ankit Panth',1,'2016-08-11 00:18:27',1,3),(84,'Harsh',1,'2016-08-11 00:18:27',1,4),(85,'Devan',1,'2016-08-11 00:18:27',1,5),(86,'Sethu',1,'2016-08-11 00:18:27',1,6),(87,'Nagendar Singh',1,'2016-08-11 00:18:27',1,7),(88,'Priyanka Sharma',1,'2016-08-11 00:18:27',1,8),(89,'Yash Narang',1,'2016-08-11 09:54:38',1,1),(90,'Haider',1,'2016-08-11 09:54:38',1,2),(91,'Ankit Panth',1,'2016-08-11 09:54:38',1,3),(92,'Harsh',1,'2016-08-11 09:54:38',1,4),(93,'Devan',1,'2016-08-11 09:54:38',1,5),(94,'Sethu',1,'2016-08-11 09:54:38',0,6),(95,'Nagendar Singh',1,'2016-08-11 09:54:38',1,7),(96,'Priyanka Sharma',1,'2016-08-11 09:54:38',1,8);
/*!40000 ALTER TABLE `Attendance_Master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ChapterMaster`
--

DROP TABLE IF EXISTS `ChapterMaster`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ChapterMaster` (
  `id` int(11) NOT NULL,
  `ChapterName` varchar(45) DEFAULT NULL,
  `subjectid` int(11) DEFAULT NULL,
  `classid` int(11) DEFAULT NULL,
  `curicullumid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `subjectid_idx` (`subjectid`),
  KEY `classid_idx` (`classid`),
  KEY `curicullumid_idx` (`curicullumid`),
  CONSTRAINT `classid` FOREIGN KEY (`classid`) REFERENCES `Class_master` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `curicullumid` FOREIGN KEY (`curicullumid`) REFERENCES `curicullum_master` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `subjectid` FOREIGN KEY (`subjectid`) REFERENCES `Subject_master` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ChapterMaster`
--

LOCK TABLES `ChapterMaster` WRITE;
/*!40000 ALTER TABLE `ChapterMaster` DISABLE KEYS */;
INSERT INTO `ChapterMaster` VALUES (1,'Numbers',2,1,1),(2,'English Version',1,2,2),(3,'Numbers Theory',2,2,1),(4,'Shapes and Spaces',2,2,1),(5,'English in class',1,2,1),(6,'Spot the object',1,2,1);
/*!40000 ALTER TABLE `ChapterMaster` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Class_master`
--

DROP TABLE IF EXISTS `Class_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Class_master` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `class_name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Class_master`
--

LOCK TABLES `Class_master` WRITE;
/*!40000 ALTER TABLE `Class_master` DISABLE KEYS */;
INSERT INTO `Class_master` VALUES (1,'First'),(2,'Second'),(3,'Third');
/*!40000 ALTER TABLE `Class_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Coscholatic_master`
--

DROP TABLE IF EXISTS `Coscholatic_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Coscholatic_master` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value_count` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Coscholatic_master`
--

LOCK TABLES `Coscholatic_master` WRITE;
/*!40000 ALTER TABLE `Coscholatic_master` DISABLE KEYS */;
INSERT INTO `Coscholatic_master` VALUES (1,2),(2,4),(3,4),(4,4),(5,4),(6,0),(7,0);
/*!40000 ALTER TABLE `Coscholatic_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `LessonPlan_Master`
--

DROP TABLE IF EXISTS `LessonPlan_Master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `LessonPlan_Master` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `LessonPlan_name` text,
  `Activity_id` int(11) DEFAULT NULL,
  `Objective_id` int(11) DEFAULT NULL,
  `User_id` int(11) DEFAULT NULL,
  `Rubric_id` int(11) DEFAULT NULL,
  `Co_Scholastic_id` int(11) DEFAULT NULL,
  `handouts` text,
  `worksheets` text,
  `video_id` int(11) DEFAULT NULL,
  `Home_acitivity` text,
  `Duration` int(11) DEFAULT NULL,
  `Observation` text,
  PRIMARY KEY (`id`),
  KEY `activity_id_idx` (`Activity_id`),
  KEY `rubric_id_idx` (`Rubric_id`),
  KEY `objective_id_idx` (`Objective_id`),
  KEY `user_id_idx` (`User_id`),
  KEY `user_id_idx1` (`Co_Scholastic_id`),
  CONSTRAINT `Co_scholatsic_id` FOREIGN KEY (`Co_Scholastic_id`) REFERENCES `Coscholatic_master` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `activity_id` FOREIGN KEY (`Activity_id`) REFERENCES `activity` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `objective_id` FOREIGN KEY (`Objective_id`) REFERENCES `objective` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `rubric` FOREIGN KEY (`Rubric_id`) REFERENCES `rubric_master` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `user_id` FOREIGN KEY (`User_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `LessonPlan_Master`
--

LOCK TABLES `LessonPlan_Master` WRITE;
/*!40000 ALTER TABLE `LessonPlan_Master` DISABLE KEYS */;
INSERT INTO `LessonPlan_Master` VALUES (1,'counting',7,23,1,44,5,'/Users/Manish/NetBeansProjects/TSAWebVersion4/build/web/upload/1469710828474Screen Shot 2016-07-22 at 11.02.24 am.png','/Users/Manish/NetBeansProjects/TSAWebVersion4/build/web/upload/test.pdf',NULL,'counting at home',45,'pending');
/*!40000 ALTER TABLE `LessonPlan_Master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Parameter_Master`
--

DROP TABLE IF EXISTS `Parameter_Master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Parameter_Master` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text,
  `rubric_id` int(11) DEFAULT NULL,
  `Score` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `rubricid_idx` (`rubric_id`),
  CONSTRAINT `rubricid` FOREIGN KEY (`rubric_id`) REFERENCES `rubric_master` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Parameter_Master`
--

LOCK TABLES `Parameter_Master` WRITE;
/*!40000 ALTER TABLE `Parameter_Master` DISABLE KEYS */;
INSERT INTO `Parameter_Master` VALUES (1,'deafultname',22,100),(3,'deafultname',24,100),(4,'deafultname',25,100),(5,'kjebcw',29,34),(6,'cwc',29,34),(7,'ewcwcewe',29,34),(8,'kjebcw',30,34),(9,'cwc',30,34),(10,'ewcwcewe',30,34),(11,'vfeevfe',31,56),(12,'vv',31,55),(13,'ervev',31,59),(14,'vfeevfe',32,56),(15,'vv',32,55),(16,'ervev',32,59),(17,'vfeevfe',32,56),(18,'vv',32,55),(19,'ervev',32,59),(20,'vfeevfe',32,56),(21,'vv',32,55),(22,'ervev',32,59),(23,'vfeevfe',33,56),(24,'vfeevfe',33,56),(25,'vfeevfe',33,56),(26,'vv',33,55),(27,'vv',33,55),(28,'vv',33,55),(29,'ervev',33,59),(30,'ervev',33,59),(31,'ervev',33,59),(32,'kjbe',34,45),(33,'kjbe',34,45),(34,'kjbe',34,45),(35,'ecwec',34,45),(36,'ecwec',34,45),(37,'ecwec',34,45),(38,'wecwec',34,45),(39,'wecwec',34,45),(40,'wecwec',34,45),(41,'kjbe',35,45),(42,'kjbe',35,45),(43,'kjbe',35,45),(44,'ecwec',35,45),(45,'ecwec',35,45),(46,'ecwec',35,45),(47,'wecwec',35,45),(48,'wecwec',35,45),(49,'wecwec',35,45),(50,'jhgv',36,78),(51,'kjbhj',36,78),(52,'kjbkj',36,76),(56,'Defaultname',40,100),(57,'Defaultname',41,100),(58,'Defaultname',42,100),(59,'Defaultname',43,100),(60,'Defaultname',44,100),(61,'Defaultname',45,100),(62,'Defaultname',46,100),(63,'Defaultname',47,100),(64,'Defaultname',48,100),(65,'Defaultname',49,100),(66,'Defaultname',50,100);
/*!40000 ALTER TABLE `Parameter_Master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ScholasticMaster`
--

DROP TABLE IF EXISTS `ScholasticMaster`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ScholasticMaster` (
  `id` int(11) NOT NULL,
  `rubric_type` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ScholasticMaster`
--

LOCK TABLES `ScholasticMaster` WRITE;
/*!40000 ALTER TABLE `ScholasticMaster` DISABLE KEYS */;
INSERT INTO `ScholasticMaster` VALUES (1,'Analytic'),(2,'Holistic');
/*!40000 ALTER TABLE `ScholasticMaster` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Student_Master`
--

DROP TABLE IF EXISTS `Student_Master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Student_Master` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Student_name` text,
  `class_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `class_id_idx` (`class_id`),
  CONSTRAINT `class_id` FOREIGN KEY (`class_id`) REFERENCES `Class_master` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Student_Master`
--

LOCK TABLES `Student_Master` WRITE;
/*!40000 ALTER TABLE `Student_Master` DISABLE KEYS */;
INSERT INTO `Student_Master` VALUES (1,'Yash Narang',1),(2,'Haider',1),(3,'Ankit Panth',1),(4,'Harsh',1),(5,'Devan',1),(6,'Sethu',1),(7,'Nagendar Singh',1),(8,'Priyanka Sharma',1);
/*!40000 ALTER TABLE `Student_Master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Subject_master`
--

DROP TABLE IF EXISTS `Subject_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Subject_master` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject_name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Subject_master`
--

LOCK TABLES `Subject_master` WRITE;
/*!40000 ALTER TABLE `Subject_master` DISABLE KEYS */;
INSERT INTO `Subject_master` VALUES (1,'English'),(2,'Maths'),(3,'Hindi'),(4,'EVS'),(5,'Science');
/*!40000 ALTER TABLE `Subject_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Value_master`
--

DROP TABLE IF EXISTS `Value_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Value_master` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Coscholastic_id` int(11) DEFAULT NULL,
  `value_name` text,
  PRIMARY KEY (`id`),
  KEY `coscholasticid_idx` (`Coscholastic_id`),
  CONSTRAINT `coscholasticid` FOREIGN KEY (`Coscholastic_id`) REFERENCES `Coscholatic_master` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Value_master`
--

LOCK TABLES `Value_master` WRITE;
/*!40000 ALTER TABLE `Value_master` DISABLE KEYS */;
INSERT INTO `Value_master` VALUES (1,1,'Leadership'),(2,1,'Resourcefulness'),(3,2,'Curiosity'),(4,2,'Decision Making'),(5,2,'Communication'),(6,2,'Leadership'),(7,3,'Participation'),(8,3,'Team Work'),(9,3,'Helping Others'),(10,3,'Leadership'),(11,4,'Leadership'),(12,4,'Communication'),(13,4,'Decision Making'),(14,4,'Organizational'),(15,5,'Resourcefulness'),(16,5,'Leadership'),(17,5,'Organizational'),(18,5,'Curiosity');
/*!40000 ALTER TABLE `Value_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Video_Master`
--

DROP TABLE IF EXISTS `Video_Master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Video_Master` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Video_name` text,
  `chapter_id` int(11) DEFAULT NULL,
  `filepath` text,
  PRIMARY KEY (`id`),
  KEY `chapter_id_idx` (`chapter_id`),
  CONSTRAINT `chapter_id` FOREIGN KEY (`chapter_id`) REFERENCES `ChapterMaster` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Video_Master`
--

LOCK TABLES `Video_Master` WRITE;
/*!40000 ALTER TABLE `Video_Master` DISABLE KEYS */;
INSERT INTO `Video_Master` VALUES (1,'Counting',3,'/Users/Manish/NetBeansProjects/TSAWebVersion4/build/web/upload/14702152308873.mp4'),(2,'English',5,'/Users/Manish/NetBeansProjects/TSAWebVersion4/build/web/upload/14703867913683.mp4'),(3,'Numbers',3,'/Users/Manish/NetBeansProjects/TSAWebVersion4/build/web/upload/14703873266763.mp4');
/*!40000 ALTER TABLE `Video_Master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `activity`
--

DROP TABLE IF EXISTS `activity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `activity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `activityname` text,
  `curicullumid` int(11) DEFAULT NULL,
  `classid` int(11) DEFAULT NULL,
  `subjectid` int(11) DEFAULT NULL,
  `chapterid` int(11) DEFAULT NULL,
  `material_required` varchar(45) DEFAULT NULL,
  `duration` varchar(15) DEFAULT NULL,
  `description` text,
  `filepath` text,
  PRIMARY KEY (`id`),
  KEY `chapterid_idx` (`chapterid`),
  CONSTRAINT `Chapter` FOREIGN KEY (`chapterid`) REFERENCES `ChapterMaster` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `activity`
--

LOCK TABLES `activity` WRITE;
/*!40000 ALTER TABLE `activity` DISABLE KEYS */;
INSERT INTO `activity` VALUES (3,'yvjsbxjw',1,1,2,1,'ambcxhja','55','kjcbkwjcb','/Users/Manish/NetBeansProjects/TSAWebVersion4/build/web/upload/1468997632397test.svg'),(4,'kjbcekjbc',1,1,2,1,'ekcjbec','34','eckjbec','/Users/Manish/NetBeansProjects/TSAWebVersion4/build/web/upload/1468997849052statistics.svg'),(5,'numbr',1,1,2,1,'pen','45','jbjchwjhcb','/Users/Manish/NetBeansProjects/TSAWebVersion4/build/web/upload/1468998153962TSAbackground 2.png'),(6,'kbvdhjved',2,2,1,2,'evwdjevd','45','kejbdkjebdke','undefined'),(7,'counting',1,1,2,1,'pen and paper','45','v2edjv2jh3vjb3jc','/Users/Manish/NetBeansProjects/TSAWebVersion4/build/web/upload/1469002401281Printouts_Feedback activities_1 copy per Batch.doc'),(8,'Number system',1,1,2,1,'Numbers chart','20 mins','kuch bhi','/Users/Manish/NetBeansProjects/TSAWebVersion4/build/web/upload/1469003685403Screen Shot 2016-06-09 at 12.13.09 PM.png'),(9,'kjbjheb',1,1,2,1,'wecbvwjec','45','wc,bwc','/Users/Manish/NetBeansProjects/TSAWebVersion4/build/web/upload/1469004624624light-bulb.svg'),(10,'dcdc',1,1,2,1,'dcdcdc','dcdc','cdcdcdcdc','/Users/Manish/NetBeansProjects/TSAWebVersion4/build/web/upload/1469165863719statistics.svg');
/*!40000 ALTER TABLE `activity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `attribute_Master`
--

DROP TABLE IF EXISTS `attribute_Master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attribute_Master` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attribueid` int(11) DEFAULT NULL,
  `attributename` varchar(100) DEFAULT NULL,
  `score` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `attributeid_idx` (`attribueid`),
  CONSTRAINT `attributeid` FOREIGN KEY (`attribueid`) REFERENCES `rubric_master` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=104 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `attribute_Master`
--

LOCK TABLES `attribute_Master` WRITE;
/*!40000 ALTER TABLE `attribute_Master` DISABLE KEYS */;
INSERT INTO `attribute_Master` VALUES (1,1,'Grade A',30),(2,1,'Grade B',30),(3,1,'Grade C',40),(8,15,'Garde A',89),(9,15,'Grade B',89),(10,15,'Grade C',89),(11,16,'Garde A',90),(12,16,'Grade B',90),(13,16,'Grade C',10),(14,17,'Garde A',90),(15,17,'Grade B',90),(16,17,'Grade C',10),(17,21,'Garde A',2),(18,21,'Grade B',22),(19,21,'Grade C',33),(20,22,'Garde A',34),(21,22,'Grade B',34),(22,22,'Grade C',34),(26,24,'Garde A',34),(27,24,'Grade B',34),(28,24,'Grade C',34),(29,25,'Garde A',34),(30,25,'Grade B',34),(31,25,'Grade C',34),(32,29,'Garde A',NULL),(33,29,'Grade B',NULL),(34,29,'Grade C',NULL),(35,30,'Garde A',NULL),(36,30,'Grade B',NULL),(37,30,'Grade C',NULL),(38,31,'Garde A',NULL),(39,31,'Grade B',NULL),(40,31,'Grade C',NULL),(41,31,'Garde A',NULL),(42,31,'Grade B',NULL),(43,31,'Grade C',NULL),(44,31,'Garde A',NULL),(45,31,'Grade B',NULL),(46,31,'Grade C',NULL),(47,32,'Garde A',NULL),(48,32,'Grade B',NULL),(49,32,'Grade C',NULL),(50,33,'Garde A',NULL),(51,33,'Grade B',NULL),(52,33,'Grade C',NULL),(53,34,'Garde A',NULL),(54,34,'Grade B',NULL),(55,34,'Grade C',NULL),(56,35,'Garde A',NULL),(57,35,'Grade B',NULL),(58,35,'Grade C',NULL),(59,36,'Garde A',NULL),(60,36,'Grade B',NULL),(61,36,'Grade C',NULL),(68,39,'Garde A',NULL),(69,39,'Grade B',NULL),(70,39,'Grade C',NULL),(71,40,'Garde A',NULL),(72,40,'Grade B',NULL),(73,40,'Grade C',NULL),(74,41,'Garde A',NULL),(75,41,'Grade B',NULL),(76,41,'Grade C',NULL),(77,42,'Garde A',NULL),(78,42,'Grade B',NULL),(79,42,'Grade C',NULL),(80,43,'Garde A',NULL),(81,43,'Grade B',NULL),(82,43,'Grade C',NULL),(83,44,'Garde A',NULL),(84,44,'Grade B',NULL),(85,44,'Grade C',NULL),(86,45,'Garde A',NULL),(87,45,'Grade B',NULL),(88,45,'Grade C',NULL),(89,46,'Garde A',NULL),(90,46,'Grade B',NULL),(91,46,'Grade C',NULL),(92,47,'Garde A',NULL),(93,47,'Grade B',NULL),(94,47,'Grade C',NULL),(95,48,'Garde A',NULL),(96,48,'Grade B',NULL),(97,48,'Grade C',NULL),(98,49,'Garde A',NULL),(99,49,'Grade B',NULL),(100,49,'Grade C',NULL),(101,50,'Garde A',NULL),(102,50,'Grade B',NULL),(103,50,'Grade C',NULL);
/*!40000 ALTER TABLE `attribute_Master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `curicullum_master`
--

DROP TABLE IF EXISTS `curicullum_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `curicullum_master` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `curicullum_name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `curicullum_master`
--

LOCK TABLES `curicullum_master` WRITE;
/*!40000 ALTER TABLE `curicullum_master` DISABLE KEYS */;
INSERT INTO `curicullum_master` VALUES (1,'CBSE'),(2,'ICSE');
/*!40000 ALTER TABLE `curicullum_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mapping_master`
--

DROP TABLE IF EXISTS `mapping_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mapping_master` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lessonplan_id` int(11) DEFAULT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `lessonplan_id_idx` (`lessonplan_id`),
  CONSTRAINT `lessonplan_id` FOREIGN KEY (`lessonplan_id`) REFERENCES `LessonPlan_Master` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mapping_master`
--

LOCK TABLES `mapping_master` WRITE;
/*!40000 ALTER TABLE `mapping_master` DISABLE KEYS */;
INSERT INTO `mapping_master` VALUES (1,1,'2016-08-27 00:00:00'),(5,1,'2015-12-27 00:00:00'),(6,1,'2015-12-27 00:00:00'),(7,1,'2016-08-18 00:00:00'),(8,1,'2016-08-06 00:00:00'),(9,1,'2016-08-18 00:00:00'),(10,1,'2016-08-08 00:00:00');
/*!40000 ALTER TABLE `mapping_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `objective`
--

DROP TABLE IF EXISTS `objective`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `objective` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `objectivename` varchar(45) DEFAULT NULL,
  `curicullumid` int(11) DEFAULT NULL,
  `classid` int(11) DEFAULT NULL,
  `subjectid` int(11) DEFAULT NULL,
  `chapterid` int(11) DEFAULT NULL,
  `description` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `chapterid_idx` (`chapterid`),
  CONSTRAINT `chapterid` FOREIGN KEY (`chapterid`) REFERENCES `ChapterMaster` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `objective`
--

LOCK TABLES `objective` WRITE;
/*!40000 ALTER TABLE `objective` DISABLE KEYS */;
INSERT INTO `objective` VALUES (21,'Count given set of objects',1,2,2,3,'SWBAT count given set of objects by making bundles of tens and ones (concrete and abstract)'),(22,'Place Value',1,2,2,3,'SWBAT identify the number of tens and ones in a given number'),(23,'Place Value numbers in hundreds',1,2,2,3,'SWBAT represent any given number less than 100 by making bundles of tens and ones (concrete and abstract)');
/*!40000 ALTER TABLE `objective` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `parameter_details`
--

DROP TABLE IF EXISTS `parameter_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `parameter_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rubricid` int(11) DEFAULT NULL,
  `parameter_id` int(11) DEFAULT NULL,
  `attribute_id` int(11) DEFAULT NULL,
  `description` text,
  `score` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `rubric_id_idx` (`rubricid`),
  KEY `parameter_id_idx` (`parameter_id`),
  KEY `attribute_id_idx` (`attribute_id`),
  CONSTRAINT `attribute_id` FOREIGN KEY (`attribute_id`) REFERENCES `attribute_Master` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `parameter_id` FOREIGN KEY (`parameter_id`) REFERENCES `Parameter_Master` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `rubric_id` FOREIGN KEY (`rubricid`) REFERENCES `rubric_master` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=139 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `parameter_details`
--

LOCK TABLES `parameter_details` WRITE;
/*!40000 ALTER TABLE `parameter_details` DISABLE KEYS */;
INSERT INTO `parameter_details` VALUES (1,22,1,22,'hghgxc',NULL),(2,22,1,22,'saasas',NULL),(3,22,1,22,'casc',NULL),(7,24,3,28,'hghgxc',NULL),(8,24,3,28,'saasas',NULL),(9,24,3,28,'casc',NULL),(10,25,4,29,'hghgxc',NULL),(11,25,4,30,'saasas',NULL),(12,25,4,31,'casc',NULL),(13,29,7,32,'{\"0\":\"wkcb\",\"1\":\"ewbc\",\"2\":\"wkjbc\"}',NULL),(14,29,7,33,'{\"0\":\"wkbc\",\"1\":\"wkjec\",\"2\":\"kwce\"}',NULL),(15,29,7,34,'{\"0\":\"wcejkb\",\"1\":\"webcjk\",\"2\":\"wkjhcv\"}',NULL),(16,30,10,35,'wkcb',NULL),(17,30,10,35,'ewbc',NULL),(18,30,10,35,'wkjbc',NULL),(19,30,10,36,'wkbc',NULL),(20,30,10,36,'wkjec',NULL),(21,30,10,36,'kwce',NULL),(22,30,10,37,'wcejkb',NULL),(23,30,10,37,'webcjk',NULL),(46,31,13,45,'wxqwxq',NULL),(52,32,14,47,'hgvqx',NULL),(53,32,15,47,'qwxqw',NULL),(54,32,16,47,'qwxqwx',NULL),(55,32,17,48,'wxqwxq',NULL),(56,32,18,48,'qwwx',NULL),(57,32,19,48,'qwxw',NULL),(58,32,20,49,'wxqwx',NULL),(59,32,21,49,'qwxw',NULL),(70,34,32,53,'wjhcvw',NULL),(71,34,33,53,'wec',NULL),(72,34,34,53,'wec',NULL),(73,34,35,54,'weuc',NULL),(74,34,36,54,'wec',NULL),(75,34,37,54,'wec',NULL),(76,34,38,55,'wce',NULL),(77,34,39,55,'wec',NULL),(78,34,40,55,'wec',NULL),(79,35,41,56,'wjhcvw',NULL),(80,35,42,56,'wec',NULL),(81,35,43,56,'wec',NULL),(82,35,44,57,'weuc',NULL),(83,35,45,57,'wec',NULL),(84,35,46,57,'wec',NULL),(85,35,47,58,'wce',NULL),(86,35,48,58,'wec',NULL),(87,35,49,58,'wec',NULL),(88,36,50,59,'kjwqbd',NULL),(89,36,50,59,'qwkjdhb',NULL),(90,36,50,59,'qwhjdbv',NULL),(91,36,51,60,'qwkjdb',NULL),(92,36,51,60,'wqdkjbx',NULL),(93,36,51,60,'mqwbdv',NULL),(94,36,52,61,'qwjhdbv',NULL),(95,36,52,61,'qwjhdvb',NULL),(96,36,52,61,'qwndv',NULL),(106,40,56,71,'dc',NULL),(107,40,56,72,'wcwc',NULL),(108,40,56,73,'wcwc',NULL),(109,41,57,74,'Excellent in Counting',NULL),(110,41,57,75,'Medium result from students side',NULL),(111,41,57,76,'Need to improve alot',NULL),(112,42,58,77,'best in counting',NULL),(113,42,58,78,'average in counting',NULL),(114,42,58,79,'poor in counting needs to improve',NULL),(115,43,59,80,'best',NULL),(116,43,59,81,'average',NULL),(117,43,59,82,'need to improve',NULL),(118,44,60,83,'best',NULL),(119,44,60,84,'average',NULL),(120,44,60,85,'poor',NULL),(121,45,61,86,'wefwwe',NULL),(122,45,61,87,'cwecwewec',NULL),(123,45,61,88,'wcwec',NULL),(124,46,62,89,'vev',NULL),(125,46,62,90,'evev',NULL),(126,46,62,91,'evev',NULL),(127,47,63,92,'efew',NULL),(128,47,63,93,'wefwef',NULL),(129,47,63,94,'wefwef',NULL),(130,48,64,95,'ecewc',NULL),(131,48,64,96,'ccwef',NULL),(132,48,64,97,'acewewc',NULL),(133,49,65,98,'jhv',NULL),(134,49,65,99,'jhbjhb',NULL),(135,49,65,100,'jhbvhj',NULL),(136,50,66,101,'jhv',NULL),(137,50,66,102,'jhbjhb',NULL),(138,50,66,103,'jhbvhj',NULL);
/*!40000 ALTER TABLE `parameter_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_master`
--

DROP TABLE IF EXISTS `role_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_master` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_master`
--

LOCK TABLES `role_master` WRITE;
/*!40000 ALTER TABLE `role_master` DISABLE KEYS */;
INSERT INTO `role_master` VALUES (1,'User'),(2,'Admin');
/*!40000 ALTER TABLE `role_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rubric_master`
--

DROP TABLE IF EXISTS `rubric_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rubric_master` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `scholasticid` int(11) DEFAULT NULL,
  `attributecount` int(11) DEFAULT NULL,
  `parametercount` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `scholasticid_idx` (`scholasticid`),
  CONSTRAINT `scholasticid` FOREIGN KEY (`scholasticid`) REFERENCES `ScholasticMaster` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rubric_master`
--

LOCK TABLES `rubric_master` WRITE;
/*!40000 ALTER TABLE `rubric_master` DISABLE KEYS */;
INSERT INTO `rubric_master` VALUES (1,1,4,3),(4,2,3,1),(5,2,3,1),(6,2,3,1),(7,2,3,1),(8,2,3,1),(9,2,3,1),(10,2,3,1),(11,2,3,1),(12,2,3,1),(13,2,3,1),(14,2,3,1),(15,2,3,1),(16,2,3,1),(17,1,3,1),(21,2,3,1),(22,2,3,1),(24,2,3,1),(25,2,3,1),(29,1,3,3),(30,1,3,3),(31,1,3,3),(32,1,3,3),(33,1,3,3),(34,1,3,3),(35,1,3,3),(36,1,3,3),(37,2,3,1),(39,2,3,1),(40,2,3,1),(41,2,3,1),(42,2,3,1),(43,2,3,1),(44,2,3,1),(45,2,3,1),(46,2,3,1),(47,2,3,1),(48,2,3,1),(49,2,3,1),(50,2,3,1);
/*!40000 ALTER TABLE `rubric_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(45) DEFAULT NULL,
  `pasword` varchar(45) DEFAULT NULL,
  `role` int(10) DEFAULT NULL,
  `school` varchar(45) DEFAULT NULL,
  `curicullum` int(11) DEFAULT NULL,
  `class` int(11) DEFAULT NULL,
  `subject` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `role_idx` (`role`),
  KEY `curiculllum_idx` (`curicullum`),
  KEY `class_idx` (`class`),
  KEY `subject_idx` (`subject`),
  CONSTRAINT `class` FOREIGN KEY (`class`) REFERENCES `Class_master` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `curiculllum` FOREIGN KEY (`curicullum`) REFERENCES `curicullum_master` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `role` FOREIGN KEY (`role`) REFERENCES `role_master` (`role_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `subject` FOREIGN KEY (`subject`) REFERENCES `Subject_master` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'yash.narang@tcs.com','tcs#1234',1,'DAV Public School',1,3,NULL),(2,'yashnarang876@gmail.com','1234',1,'MLN Public School',2,1,NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-08-22  9:48:27
