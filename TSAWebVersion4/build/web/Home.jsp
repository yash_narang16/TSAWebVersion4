<%-- 
    Document   : Home
    Created on : Jul 1, 2016, 2:12:54 PM
    Author     : Yash
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <script type='text/javascript' src='js/loader.js'></script>
        <link rel="stylesheet" type="text/css" href="css/loader.css">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/newcss.css">
        <link rel="stylesheet" type="text/css" href="css/home.css">
    </head>
    <body ng-app="home", ng-controller="homectrl">
      <nav class="navbar navbar-default web-color navbar-fixed-top">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand text-color" href="#">
                        <!--<img alt="Brand" src="Images/logo.png">-->
                        Teacher Support Application
                    </a>
                </div>
                 <ul class="nav navbar-nav pull-right">
      <li><a class ="text-color" href="#">Home</a></li>
      <li><a class ="text-color" href="#">Plan Your lesson</a></li>
      <li><a class ="text-color" href="#">{{userdetails.user_name}}</a></li> 
     
    </ul>
            </div>
        </nav>
        <div class="container-fluid" style="padding-top: 130px;">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-12">
                    <a href="index.jsp">
                    <div class="panel panel-primary">
                        <div class="panel-body">
                            <div class="icon-back">
                                <img src="Images/teacher-lecture-in-front-an-auditory.svg" alt="mesage">
                           
                            </div>
                            <div class="icon-text">Lesson Plan</div>
                        </div>
                    </div>
                    </a>
                </div>
           
                <div class="col-lg-4 col-md-4 col-sm-12">
                    <a href="Activity.jsp">
                    <div class="panel panel-primary">
                        <div class="panel-body">
                            <div class="icon-back">
                                <img src="Images/briefcase-and-tools-for-school.svg" alt="mesage">
                           
                            </div>
                            <div class="icon-text">Activity</div>
                        </div>
                    </div>
                     </a>
                </div>
           
                <div class="col-lg-4 col-md-4 col-sm-12">
                    <a href="Objective.jsp">
                    <div class="panel panel-primary">
                        <div class="panel-body">
                            <div class="icon-back">
                                <img src="Images/paper-sheets-with-text-lines-and-a-pen-at-right-side-from-top-view.svg" alt="mesage">
                           
                            </div>
                            <div class="icon-text">Objectives</div>
                        </div>
                    </div>
                     </a>
                </div>
           
                <div class="col-lg-4 col-md-4 col-sm-12">
                  
                    <div class="panel panel-primary">
                        <div class="panel-body">
                            <div class="icon-back">
                                <img src="Images/movie-projection-to-class-students.svg" alt="mesage">
                           
                            </div>
                            <div class="icon-text">Video Gallery</div>
                        </div>
                    </div>
                 
                </div>
            
                <div class="col-lg-4 col-md-4 col-sm-12">
                    <div class="panel panel-primary">
                        <div class="panel-body">
                            <div class="icon-back">
                                <img src="Images/books-and-clock-with-alarm.svg" alt="mesage">
                           
                            </div>
                            <div class="icon-text">Record Attendance</div>
                        </div>
                    </div>
                </div>
           
                <div class="col-lg-4 col-md-4 col-sm-12">
                    <div class="panel panel-primary">
                        <div class="panel-body">
                            <div class="icon-back">
                                <img src="Images/descendant-graphic.svg" alt="mesage">
                           
                            </div>
                            <div class="icon-text">Analyze Reports</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        
        
        <script type="text/javascript" src="js/angular.js"></script>
        <script type="text/javascript" src="js/ui-bootstrap-tpls-1.3.3.min.js"></script>
        <script type="text/javascript" src="js/home.js"></script>
        <script type="text/javascript" src="js/Services/LocalStorage.js"></script>
      
    </body>
</html>
