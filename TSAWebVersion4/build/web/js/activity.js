/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


angular.module("activity", ['ui.bootstrap', 'thatisuday.dropzone', 'rpTable', 'ngAnimate', 'toastr', 'Services.ActvityService', 'Services.Commonservice'])

        .controller("main", function ($scope, toastr, $CommonService, $ActivityService) {
            var response = $CommonService.fetchcuricullum();
            console.log(response);
            response.then(function (data) {
                console.log(data);
                $scope.curicullum = data;

            });


            $scope.Fetch_class = function (val) {
                $scope.curicullumvalue = val.curicullumid;

                console.log(val);
                var response = $CommonService.fetchClass();
                response.then(function (data) {
                    console.log(data)
                    $scope.class = data;
                })

            }


            $scope.Fetch_Subject = function (val1) {
                $scope.classvalue = val1.class_id;

                var response = $CommonService.fetchSubject();
                response.then(function (data) {
                    console.log(data);
                    $scope.subject = data;
                })
            }


            $scope.Fetch_chapter = function (val2) {
                $scope.subjectvalue = val2.subject_id;
                val2 = $scope.subjectvalue;
                console.log($scope.subjectvalue);
                var classid = $scope.classvalue;
                var resposne = $CommonService.fetchChapter(val2, classid, $scope.curicullumvalue);
                resposne.then(function (data) {
                    console.log(data)
                    if (data[0].status == "no") {
                        toastr.error("No Record Found, Please select other option", "Error");
                    } else {
                        $scope.chapter = data;
                    }
                })
            }

            $scope.Select_chapter = function (val) {
                $scope.chaptervalue = val.chapter_id;
            }





//file upload
            $scope.dzOptions = {
                url: 'UploadFile', //servlet name
                paramName: 'photo',
                maxFilesize: '10', //Size
                addRemoveLinks: true,
            };
            $scope.dzCallbacks = {
                'addedfile': function (file) {
                    console.log(file);
                    $scope.newFile = file;
                },
                'success': function (file, xhr) {
                    var result = JSON.parse(xhr);     //response from servlet after upload
                    if (result.responseCode == 1) {
                        toastr.success("File Uploaded Successfully", "Success");
                        console.log(result.imagePath);
                        $scope.imagepath = result.imagePath;
                    } else {
                        $scope.imagepath = "";
                    }
                },
            };



            $scope.dzMethods = {};
            $scope.removeNewFile = function () {
                $scope.dzMethods.removeFile($scope.newFile); //We got $scope.newFile from 'addedfile' event callback
            }

            //create activity  hitting servlet for saving results

            $scope.saveActivity = function (obj)
            {
                $scope.obj = {
                    name: obj.name,
                    material: obj.material,
                    duration: obj.duration,
                    description: obj.description
                }

                var response = $ActivityService.saveActivity($scope.curicullumvalue, $scope.classvalue, $scope.subjectvalue, $scope.chaptervalue, obj.name, obj.material, obj.duration, $scope.imagepath,obj.description);
                response.then(function (data) {

                    console.log(data);
                    if(data.status=="Yes"){
                         toastr.success("Activity Created Successfully", "Success");
                        
                    }
                    else{
                        
                        toastr.error("Something Went Wrong!!", "Error");
                    }

                })
            }
            
            
            
            $scope.useActivity=function()
            {
                var response=$ActivityService.fetchActivity();
                response.then(function(data){
                    console.log(data);
                    
                })
                
                
            }            






      




        })
