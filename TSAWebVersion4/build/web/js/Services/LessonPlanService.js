/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

angular.module("Service.LessonPlanService",[])
        .service("$LessonPlanService",function($http,$q){
            var LessonPlanService={};
            
            var save_act="save";
            var fetch_act="fetch";
            LessonPlanService.saveLessonPlan= function(lessonplanName,lessonplanduration,homeactivity,observation,objectiveid,activityid,userid,worksheetpath,handoutpath,videopath){
                console.log(lessonplanName,lessonplanduration,homeactivity,observation,objectiveid,activityid,userid,worksheetpath,handoutpath,videopath);
                var defer = $q.defer();
                $http({
                    url: 'LessonPlanServlet',
                    method: 'post',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    transformRequest: function (obj) {
                        var str = [];
                        for (var i in obj) {
                            str.push(encodeURIComponent(i) + "=" + encodeURIComponent(obj[i]));
                        }
                        return str.join('&');
                    },
                    data: {
                        act: save_act,
                        lessonplanName:lessonplanName ,
                        lessonplanduration: lessonplanduration,
                        homeactivity: homeactivity,
                        observation: observation,
                        objectiveid: objectiveid,
                        activityid: activityid,
                        userid:userid,
                        worksheetpath:worksheetpath,
                        handoutpath:handoutpath,
                        videopath:videopath
                    }
                }).success(function (data) {
                    console.log(data);

                    defer.resolve(data);
                }).error(function (data, status) {


                })
                return defer.promise;
                
                
                
            }
            
            
            
            LessonPlanService.fetchLessonPlan= function(){
//                console.log(lessonplanName,lessonplanduration,homeactivity,observation,objectiveid,activityid,userid,worksheetpath,handoutpath,videopath);
                var defer = $q.defer();
                $http({
                    url: 'LessonPlanServlet',
                    method: 'post',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    transformRequest: function (obj) {
                        var str = [];
                        for (var i in obj) {
                            str.push(encodeURIComponent(i) + "=" + encodeURIComponent(obj[i]));
                        }
                        return str.join('&');
                    },
                    data: {
                        act: fetch_act,
                       
                    }
                }).success(function (data) {
                    console.log(data);

                    defer.resolve(data);
                }).error(function (data, status) {


                })
                return defer.promise;
                
                
                
            }
            
            
            
            
            
            return LessonPlanService;
        })
