/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

angular.module("Services.ActvityService",[])
        .service("$ActivityService",function($http,$q){
            var ActivityService={};
     var act_save="save";
     var act_fetch="fetch";
      ActivityService.saveActivity=function(curi, classval,subject,chapter,name,material,duration,filepath,description){
          console.log(curi, classval,subject,chapter,name,material,duration,filepath,description);
           var defer = $q.defer();
                $http({
                    url: 'ActivityServlet',
                    method: 'post',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    transformRequest: function (obj) {
                        var str = [];
                        for (var i in obj) {
                            str.push(encodeURIComponent(i) + "=" + encodeURIComponent(obj[i]));
                        }
                        return str.join('&');
                    },
                    data: {
                      act:act_save,
                      curicullum:curi,
                      classobj:classval,
                      subject:subject,
                      chapter:chapter,
                      name:name,
                      material:material,
                      duration:duration,
                      filepath:filepath,
                      description:description
                    }
                }).success(function (data) {
                    console.log(data);
                    
                    defer.resolve(data);
                }).error(function (data, status) {
      

                })
                return defer.promise;
          
          
          
      }
    
    
    
    
     ActivityService.fetchActivity=function(){
         
           var defer = $q.defer();
                $http({
                    url: 'ActivityServlet',
                    method: 'post',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    transformRequest: function (obj) {
                        var str = [];
                        for (var i in obj) {
                            str.push(encodeURIComponent(i) + "=" + encodeURIComponent(obj[i]));
                        }
                        return str.join('&');
                    },
                    data: {
                      act:act_fetch,
                      
                    }
                }).success(function (data) {
                    console.log(data);
                    
                    defer.resolve(data);
                }).error(function (data, status) {
      

                })
                return defer.promise;
          
          
          
      }
    
    
    
    
    
            
            
    return ActivityService;        
        })


