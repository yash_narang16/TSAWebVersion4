/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

angular.module("Services.VideoService", [])

        .service("$VideoService", function ($http, $q) {
            var VideoService = {};
            var save_act = "save";
            var fetch_act="fetch";
            VideoService.createVideo = function (chapterId,name, filepath) {
                var defer = $q.defer();
                $http({
                    url: 'VideoServlet',
                    method: 'post',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    transformRequest: function (obj) {
                        var str = [];
                        for (var i in obj) {
                            str.push(encodeURIComponent(i) + "=" + encodeURIComponent(obj[i]));
                        }
                        return str.join('&');
                    },
                    data: {
                        act: save_act,
                        chapterId: chapterId,
                        name: name,
                        filepath: filepath

                    }
                }).success(function (data) {
                    console.log(data);

                    defer.resolve(data);
                }).error(function (data, status) {


                })
                return defer.promise;




            }
            
            
            
            
            
            
             VideoService.fetchVideo = function () {
                var defer = $q.defer();
                $http({
                    url: 'VideoServlet',
                    method: 'post',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    transformRequest: function (obj) {
                        var str = [];
                        for (var i in obj) {
                            str.push(encodeURIComponent(i) + "=" + encodeURIComponent(obj[i]));
                        }
                        return str.join('&');
                    },
                    data: {
                        act: fetch_act,
                    

                    }
                }).success(function (data) {
                    console.log(data);

                    defer.resolve(data);
                }).error(function (data, status) {


                })
                return defer.promise;




            }






            return VideoService;
        })

