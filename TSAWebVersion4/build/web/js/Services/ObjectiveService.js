/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
angular.module("Services.ObjectiveService", [])
        .service("$ObjectiveService", function ($http, $q) {
            var ObjectiveService = {};
            var fetch_act = "fetch";
            var save_act = "save";
            ObjectiveService.createObjective = function (curiId, classId, subjectId, chapterId, objective_name, description) {

                console.log(curiId + " " + classId + " " + subjectId + " " + chapterId + " " + objective_name + " " + description);
                var defer = $q.defer();
                $http({
                    url: 'ObjectiveServlet',
                    method: 'post',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    transformRequest: function (obj) {
                        var str = [];
                        for (var i in obj) {
                            str.push(encodeURIComponent(i) + "=" + encodeURIComponent(obj[i]));
                        }
                        return str.join('&');
                    },
                    data: {
                        act: save_act,
                        curId: curiId,
                        classId: classId,
                        subjectId: subjectId,
                        chapterId: chapterId,
                        name: objective_name,
                        description: description
                    }
                }).success(function (data) {
                    console.log(data);

                    defer.resolve(data);
                }).error(function (data, status) {


                })
                return defer.promise;


            }






            ObjectiveService.FetchObjective = function () {
                var defer = $q.defer();
                $http({
                    url: 'ObjectiveServlet',
                    method: 'post',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    transformRequest: function (obj) {
                        var str = [];
                        for (var i in obj) {
                            str.push(encodeURIComponent(i) + "=" + encodeURIComponent(obj[i]));
                        }
                        return str.join('&');
                    },
                    data: {
                        act: fetch_act
                    }
                }).success(function (data) {
                    console.log(data);

                    defer.resolve(data);
                }).error(function (data, status) {


                })
                return defer.promise;



            }







            return ObjectiveService;

        })



