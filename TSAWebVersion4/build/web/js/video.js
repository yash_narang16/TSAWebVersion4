/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
angular.module('video', ['ui.bootstrap', 'thatisuday.dropzone', "Services.Commonservice", "ngAnimate", "toastr", "Services.ObjectiveService", "Services.ValidationService", "Services.VideoService"])

        .controller('videoctrl', function ($scope, $CommonService, toastr, $ObjectiveService, $ValidationService, $VideoService,$uibModal) {

            var response = $CommonService.fetchcuricullum();
            console.log(response);
            response.then(function (data) {
                console.log(data);
                $scope.curicullum = data;

            });


            $scope.Fetch_class = function (val) {
                $scope.curicullumvalue = val.curicullumid;

                console.log(val);
                var response = $CommonService.fetchClass();
                response.then(function (data) {
                    console.log(data)
                    $scope.class = data;
                })

            }


            $scope.Fetch_Subject = function (val1) {
                $scope.classvalue = val1.class_id;

                var response = $CommonService.fetchSubject();
                response.then(function (data) {
                    console.log(data);
                    $scope.subject = data;
                })
            }


            $scope.Fetch_chapter = function (val2) {
                $scope.subjectvalue = val2.subject_id;
                val2 = $scope.subjectvalue;
                console.log($scope.subjectvalue);
                var classid = $scope.classvalue;
                if ($ValidationService.checkselect($scope.curicullumvalue) && $ValidationService.checkselect($scope.subjectvalue) && $ValidationService.checkselect($scope.classvalue)) {
                    var resposne = $CommonService.fetchChapter(val2, classid, $scope.curicullumvalue);
                    resposne.then(function (data) {
                        console.log(data)
                        if (data[0].status == "no") {
                            toastr.error("No Record Found, Please select other option", "Error");
                        } else {
                            $scope.chapter = data;
                        }
                    })
                } else {
                    toastr.error("Opps!! You miss Something", "Error");
                }
            }

            $scope.Select_chapter = function (val) {
                $scope.chaptervalue = val.chapter_id;
            }

            $scope.dzOptions = {
                url: 'UploadFile',
                paramName: 'photo',
                maxFilesize: '10',
                acceptedFiles : 'video/mp4',
                addRemoveLinks: true,
            };



            $scope.dzCallbacks = {
                'addedfile': function (file) {
                    console.log(file);
                    $scope.newFile = file;
                },
                'success': function (file, xhr) {
                    console.log(file, xhr);
                    var result = JSON.parse(xhr);     //response from servlet after upload
                    if (result.responseCode == 1) {
                        toastr.success("File Uploaded Successfully", "Success");
                        console.log(result.imagePath);
                        $scope.videopath = result.imagePath;
                    } else {
                        $scope.videopath = "";
                    }
                },
            };



            $scope.dzMethods = {};
            $scope.removeNewFile = function () {
                $scope.dzMethods.removeFile($scope.newFile); //We got $scope.newFile from 'addedfile' event callback
            }








            $scope.createvideo = function (data) {

                $scope.obj = {
                    name: data.videoname
                }

                var response = $VideoService.createVideo($scope.chaptervalue, $scope.obj.name, $scope.videopath)
                response.then(function (data) {
                    console.log(data);

                })

            }
            
            
            $scope.fetch_video=function(){
                
                var response=$VideoService.fetchVideo();
                response.then(function(data){
                    console.log(data);
                    $scope.videodata=data;
                    
                })
                
            }
            
            
            $scope.openvideomodal = function (val) {

    var modalInstance = $uibModal.open({
      animation: true,
      templateUrl: 'videomodel.html',
      controller: 'videomodelctrl',
    
      resolve: {
        items: function () {
          return val;
        }
      }
      
  })
     }
     
     
     
            
        })















       
        
    .controller("videomodelctrl",function($scope,$uibModalInstance,$rootScope,items){
        console.log(items);
            $scope.videomodel=items.video.videopath;
            $scope.videoname=items.video.videoname;
            console.log($scope.videomodel);
            
            $scope.ok = function () {
            
    $uibModalInstance.close();
  };

  $scope.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };
    });