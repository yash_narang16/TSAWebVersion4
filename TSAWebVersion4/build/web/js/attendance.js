/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


angular.module('attendance', ['ui.bootstrap', 'Services.Commonservice', 'Services.AttendanceService', 'ngAnimate', 'toastr'])
        .controller('attendancectrl', function ($scope, $CommonService, $AttendanceService, toastr, $filter) {
            $scope.open1 = function () {
                $scope.popup1.opened = true;
            };
            $scope.popup1 = {
                opened: false
            };

            $scope.dt = new Date();
            $scope.curdate = new Date();
            $scope.month = $scope.dt.getMonth();
            $scope.year = $scope.dt.getFullYear();
           $scope.monthwisedate=getDateforMonth($scope.month,$scope.year);
           
            $scope.filterdate = function (val) {
                $scope.dt = val;
                console.log($scope.dt);
                $scope.month = $scope.dt.getMonth();
                $scope.year = $scope.dt.getFullYear();
                $scope.monthwisedate =getDateforMonth($scope.month,$scope.year);
                $scope.display_attendance();
                console.log($scope.monthwisedate);
                dayofname(getDaysInMonth($scope.month, $scope.year));
            }

            function getDaysInMonth(month, year) {
                // Since no month has fewer than 28 days
                var date = new Date(year, month, 1);
                $scope.attendance_chart = $filter('date')(date, "yyyy-MM-dd");
                console.log($scope.attendance_chart);
                var days = [];
//                console.log('month', month, 'date.getMonth()', date.getMonth())
                while (date.getMonth() === month) {
                    days.push(new Date(date).getDay());
                    date.setDate(date.getDate() + 1);
                }
                return days;
            }

            var day = getDaysInMonth($scope.month, $scope.year);
            dayofname(day);

            function dayofname(day) {
                $scope.dayname = [];
                for (var i = 0; i < day.length; i++) {
                    if (day[i] == 0) {
                        $scope.dayname.push("Sunday")
                    } else if (day[i] == 1) {
                        $scope.dayname.push("Monday")
                    } else if (day[i] == 2) {
                        $scope.dayname.push("Tuesday")
                    } else if (day[i] == 3) {
                        $scope.dayname.push("Wednesday")
                    } else if (day[i] == 4) {
                        $scope.dayname.push("Thrusday")
                    } else if (day[i] == 5) {
                        $scope.dayname.push("Friday")
                    } else {
                        $scope.dayname.push("Saturaday");
                    }
                }
            }

            $scope.loaderhide = true;
            $scope.showbutton = false;
            var response = $CommonService.fetchClass();
            response.then(function (data) {
                $scope.class = data;
                $scope.loaderhide = false;

            })

            $scope.Fetch_Subject = function (val) {
                $scope.classvalue = val.class_id;
                $scope.showbutton = true;
                var response = $AttendanceService.fetch_attendance($scope.classvalue);
                response.then(function (data) {
                    $scope.studentdata = data;
                    console.log(data);
                })
            }


            $scope.clearAll = function () {
                angular.forEach($scope.studentdata, function (check) {
                    check.check = false;
                });
            };

            $scope.selectAll = function () {
                angular.forEach($scope.studentdata, function (check) {
                    check.check = true;
                });
            };


            $scope.saveAttendance = function () {
                $scope.loaderhide = true;
                console.log($scope.studentdata)
                var stringdata = JSON.stringify($scope.studentdata)
                var response = $AttendanceService.save_attendance(stringdata);
                response.then(function (data) {
                    console.log(data);
                    $scope.loaderhide = false;
                    if (data.status == "Yes") {
                        toastr.success("Attendance Recorded Successfully", "Success");

                    }
                })




            }





            $scope.display_attendance = function () {

                var response = $AttendanceService.view_attendance();
                response.then(function (data) {
                    console.log(data);
                    $scope.attendancedata = data;
                    var nextresultattendance = [];
                    for (var x = 0; x < data.length; x++) {

//                  console.log(data[x].arraydata.length);
                        var resultattendance = [];
                        var finalattendance = [];
                        var newarray = [];
                        for (var j = 0; j <$scope.monthwisedate.length; j++) {
                                
                                newarray.push("pe");
                            

                            }
                        for (var i = 0; i < data[x].arraydata.length; i++) {

                            var date = new Date(data[x].arraydata[i].attendance_date);

                            var newdate = $filter('date')(date, "yyyy-MM-dd");
//                 console.log(newdate);
                            var alldate = getDateforMonth(date.getMonth(), date.getFullYear());
                            var dateArray = $scope.monthwisedate;
                console.log(dateArray);
                           

                            


                            for (var j = 0; j < dateArray.length; j++) {
                                if ((newdate === dateArray[j]) && (data[x].arraydata[i].attendance_status === 1)) {
//                                    
                                    newarray[j] = "pr";
                                    console.log("present");

                                } else if ((newdate === dateArray[j]) && (data[x].arraydata[i].attendance_status === 0)) {
//                               
                                    newarray[j] = "ab";
                                    console.log("absent");
                                }



                            }
                            resultattendance.push(newarray);


                        }
                        console.log(newarray);
                        nextresultattendance.push(resultattendance);

                    }

                    $scope.attendancechart = [];
                    console.log(nextresultattendance);
                    for (var i = 0; i < nextresultattendance.length; i++) {

                        $scope.attendancechart.push(nextresultattendance[i][0]);


                    }
                    console.log($scope.attendancechart);
                })

            }

            function getDateforMonth(month, year) {
                var firstdate = new Date(year, month, 1);
//            console.log(firstdate);
                var date = [];
                while (firstdate.getMonth() === month) {
//              console.log(firstdate);
                    var filterdate = $filter('date')(firstdate, 'yyyy-MM-dd')
                    date.push((filterdate));
                    firstdate.setDate(firstdate.getDate() + 1);

                }

                return date;
            }
























        })