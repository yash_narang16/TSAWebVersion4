<%-- 
    Document   : Login
    Created on : Jun 29, 2016, 2:20:18 PM
    Author     : Yash
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>JSP Page</title>
        <script type='text/javascript' src='js/loader.js'></script>
        <link rel="stylesheet" type="text/css" href="css/loader.css">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">

        <link rel="stylesheet" type="text/css" href="css/newcss.css">
        <link rel="stylesheet" type="text/css" href="css/Toast.css">

    </head>
    <body ng-app="app" ng-controller="login">

        <div class="container-fluid backgroun-img">
            <div class="row">
                <div class="col-lg-6 col-md-12 col-sm-12">

                    <span class="landingpage text-color">Teacher Support Application</span>
                </div>

                <div class="col-lg-1 col-lg-offset-4 col-md-12 col-sm-12" style="padding: 6px;">
                    <button type="button" class="btn btn-button-default" ng-click="gotoBottom()">Login Here</button>
                </div>
                <div class="col-lg-1 col-md-12 col-sm-12" style="padding: 6px;">
                    <button type="button" class="btn btn-button-default">Sign up Here</button>
                </div>



            </div>
        </div>
        <div class="content-page">

            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-12 divde-line">
                    <div class="image-card">
                    <div class="thumbnail">
                        <img src="Images/blackboard.svg" alt="...">
                        <div class="caption">
                            <h3>Thumbnail label</h3>
                            <p>...</p>
                            <!--<p><a href="#" class="btn btn-primary" role="button">Button</a> <a href="#" class="btn btn-default" role="button">Button</a></p>-->
                        </div>
                    </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-12 divde-line">
                     <div class="image-card">
                    <div class="thumbnail">
                        <img src="Images/alarm-clock.svg" alt="...">
                        <div class="caption">
                            <h3>Thumbnail label</h3>
                            <p>...</p>
                            <!--<p><a href="#" class="btn btn-primary" role="button">Button</a> <a href="#" class="btn btn-default" role="button">Button</a></p>-->
                        </div>
                    </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-12 divde-line">
                  <div class="image-card">
                    <div class="thumbnail">
                        <img src="Images/open-book.svg" alt="...">
                        <div class="caption">
                            <h3>Thumbnail label</h3>
                            <p>...</p>
                            <!--<p><a href="#" class="btn btn-primary" role="button">Button</a> <a href="#" class="btn btn-default" role="button">Button</a></p>-->
                        </div>
                    </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-12">
                  <div class="image-card">
                    <div class="thumbnail">
                        <img src="Images/diploma.svg" alt="...">
                        <div class="caption">
                            <h3>Thumbnail label</h3>
                            <p>...</p>
                            <!--<p><a href="#" class="btn btn-primary" role="button">Button</a> <a href="#" class="btn btn-default" role="button">Button</a></p>-->
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="backgroun-img2"  id="login">
            <div class="row" style="padding-top:200px;">
                <div class="col-lg-4 col-lg-offset-4">
                    <div class="well well-lg login">
                        <form>
                            <div class="row">
                                <div class="form-group col-lg-12">
                                    <label for="exampleInputEmail1">Email address</label>
                                    <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Email" ng-model="user.username">
                                </div>
                                <div class="form-group col-lg-12">
                                    <label for="exampleInputPassword1">Password</label>
                                    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" ng-model="user.password">
                                </div>


                                <button type="button" class="btn btn-success pull-right" ng-click="userLogin(user)">Submit</button>

                       
                    </div>
                        </form>
                </div>



            </div>
        </div>
    </div>



    <script type="text/javascript" src="js/angular.js"></script>
    <script type="text/javascript" src="js/ui-bootstrap-tpls-1.3.3.min.js"></script>
    <script type="text/javascript" src="js/login.js"></script>
    <script type="text/javascript" src="js/Services/LoginService.js"></script>
    <script type="text/javascript" src="js/Services/LocalStorage.js"></script>
    <script type="text/javascript" src="js/angular-animate.min.js"></script>
    <script type="text/javascript" src="js/toast.js"></script>
    

</body>
</html>
