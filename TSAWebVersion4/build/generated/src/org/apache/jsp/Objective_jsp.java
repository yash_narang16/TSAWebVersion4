package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class Objective_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n");
      out.write("        <title>JSP Page</title>\n");
      out.write("        <script type='text/javascript' src='js/loader.js'></script>\n");
      out.write("        <link rel=\"stylesheet\" type=\"text/css\" href=\"css/loader.css\">\n");
      out.write("        <link rel=\"stylesheet\" type=\"text/css\" href=\"css/bootstrap.min.css\">\n");
      out.write("        <link rel=\"stylesheet\" type=\"text/css\" href=\"css/progressbar.css\">\n");
      out.write("        <link rel=\"stylesheet\" type=\"text/css\" href=\"css/newcss.css\">\n");
      out.write("        <!--<link href='https://fonts.googleapis.com/css?family=Raleway:400,500,800' rel='stylesheet' type='text/css'>-->\n");
      out.write("\n");
      out.write("        <link rel=\"stylesheet\" type=\"text/css\" href=\"css/Toast.css\">\n");
      out.write("        <link rel=\"stylesheet\" type=\"text/css\" href=\"css/rpTable.css\">\n");
      out.write("\n");
      out.write("    </head>\n");
      out.write("    <body ng-app=\"objective\" ng-controller=\"mainctrl\">\n");
      out.write("        <nav class=\"navbar navbar-default web-color navbar-fixed-top\">\n");
      out.write("            <div class=\"container-fluid\">\n");
      out.write("                <div class=\"navbar-header\">\n");
      out.write("                    <a class=\"navbar-brand text-color\" href=\"#\">\n");
      out.write("                        <!--<img alt=\"Brand\" src=\"Images/logo.png\">-->\n");
      out.write("                        Teacher Support Application\n");
      out.write("                    </a>\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("        </nav>\n");
      out.write("\n");
      out.write("        <div class=\"container\" style=\"padding-top:60px;\">\n");
      out.write("            <div class=\"row\">\n");
      out.write("                <div class=\"col-lg-12 col-md-12 col-sm-12\">\n");
      out.write("                    <uib-tabset active=\"activeJustified\" justified=\"true\" class=\"vertical-layout\">\n");
      out.write("                        <uib-tab index=\"0\" heading=\" Create Objective\">\n");
      out.write("                            <!--//tab content1-->\n");
      out.write("\n");
      out.write("                            <div class=\"row\">\n");
      out.write("                                <div class=\"col-lg-12 col-md-12 col-sm-12\">\n");
      out.write("                                    <form name=\"objectiveForm\">\n");
      out.write("                                     <div class=\"form-group col-lg-6 col-md-6 col-sm-12\">\n");
      out.write("                                                    <label for=\"curiicullum\">Choose Curricullum</label>\n");
      out.write("                                                    <select class=\"form-control\" id=\"curiicullum\" ng-model=\"curi.curicullumid[0]\" ng-options=\"curi.curicullum for curi in curicullum\" ng-change=\"Fetch_class(curi.curicullumid[0])\" required>\n");
      out.write("                                                        <option disabled selected value=\"\">Choose your option</option>\n");
      out.write("\n");
      out.write("                                                    </select>\n");
      out.write("                                                </div>\n");
      out.write("                                                <div class=\"form-group col-lg-6 col-md-6 col-sm-12\">\n");
      out.write("                                                    <label for=\"Class\">Choose Class</label>\n");
      out.write("                                                    <select class=\"form-control\" id=\"Class\" ng-model=\"class.class_id[0]\" ng-options=\"class.class_name for class in class\" ng-change=\"Fetch_Subject(class.class_id[0])\" required>\n");
      out.write("                                                        <option disabled selected value=\"\">Choose your option</option>\n");
      out.write("                                                    </select>\n");
      out.write("                                                </div>\n");
      out.write("                                        <div class=\"form-group col-lg-6 col-md-6 col-sm-12\">\n");
      out.write("                                            <label for=\"Subject\">Choose Subject</label>\n");
      out.write("                                            <select class=\"form-control\" id=\"Subject\" ng-model=\"subject.subject_id[0]\" ng-options=\"subject.subject_name for subject in subject\" ng-change=\"Fetch_chapter(subject.subject_id[0])\" required>\n");
      out.write("                                                <option disabled selected value=\"\">Choose Subject</option>\n");
      out.write("                                                <option ng:repeat=\"subject in subject\" value=\"{{subject.subject_id}}\">{{subject.subject_name}}</option>\n");
      out.write("\n");
      out.write("                                            </select>\n");
      out.write("\n");
      out.write("                                        </div>\n");
      out.write("\n");
      out.write("                                        <div class=\"form-group col-lg-6 col-md-6 col-sm-12\">\n");
      out.write("                                            <label for=\"Chapter\">Choose Chapter</label>\n");
      out.write("                                            <select class=\"form-control\" id=\"Chapter\" ng-model=\"chapter.chapter_id[0]\" ng-options=\"chapter.chapter_name for chapter in chapter\" ng-change=\"Select_chapter(chapter.chapter_id[0])\" required>\n");
      out.write("                                                <option disabled selected value=\"\">Choose Chapter</option>\n");
      out.write("                                            </select>\n");
      out.write("\n");
      out.write("                                        </div>\n");
      out.write("                                        <div class=\"form-group col-lg-6 col-md-6 col-sm-12\">\n");
      out.write("                                            <label for=\"Name\">Write Objective</label>\n");
      out.write("                                            <input type=\"text\" class=\"form-control\" name=\"objective_name\" placeholder=\"Objective\" id=\"Name\" ng-model=\"obj.objective_name\" ng-pattern=\"/^[A-Za-z0-9 ]{1,100}$/\" required>\n");
      out.write("                                            <span ng-show=\"objectiveForm.objective_name.$touched && objectiveForm.objective_name.$error.required\" ng-cloak>Objective Name Required</span>\n");
      out.write("                                            <span ng-show=\"objectiveForm.objective_name.$error.pattern\" ng-cloak>Only Characters and Numbers</span>\n");
      out.write("                                        </div>\n");
      out.write("                                        <div class=\"form-group col-lg-6 col-md-6 col-sm-12\">\n");
      out.write("                                            <label for=\"Description\">Write Description</label>\n");
      out.write("                                            <textarea class=\"form-control\" rows=\"3\" name=\"description\" ng-model=\"obj.objective_description\" required></textarea>\n");
      out.write(" <span ng-show=\"objectiveForm.description.$touched && objectiveForm.description.$error.required\" ng-cloak>Description Required</span>\n");
      out.write("                                        </div>\n");
      out.write("                                        <div class=\"form-group col-lg-12 col-md-12 col-sm-12\">\n");
      out.write("                                            <button type=\"button\" class=\"btn btn-success pull-right\" ng-click=\"createObjective(obj)\" ng-disabled=\"objectiveForm.$invalid\">Save Objective</button>\n");
      out.write("\n");
      out.write("                                        </div>\n");
      out.write("\n");
      out.write("                                    </form>\n");
      out.write("\n");
      out.write("\n");
      out.write("                                </div>\n");
      out.write("                            </div>\n");
      out.write("\n");
      out.write("                        </uib-tab>\n");
      out.write("                        <uib-tab index=\"1\" heading=\"View Objective\" select=\"fetch_Objective()\">\n");
      out.write("                             <div class=\"row\">\n");
      out.write("                                                <div class=\"col-lg-3 col-md-3 col-sm-12\" ng-repeat=\"objective in objective\">\n");
      out.write("                                    <div class=\"panel panel-default\">\n");
      out.write("                                        <div class=\"panel-heading lessonplan-card\">\n");
      out.write("                                            {{objective.objective_name}}\n");
      out.write("                                            <div class=\"icon-tools pull-right\">\n");
      out.write("\n");
      out.write("                                                <span class=\"glyphicon glyphicon-modal-window\"></span>\n");
      out.write("\n");
      out.write("                                                <span class=\"glyphicon glyphicon-tag\"></span>\n");
      out.write("                                            </div>\n");
      out.write("                                        </div>\n");
      out.write("                                        <div class=\"panel-body\">\n");
      out.write("                                         \n");
      out.write("                                            <div class=\"lessonplan-text\"> \n");
      out.write("                                                <span class=\"pull-left\">{{objective.objective_class}} Class</span>\n");
      out.write("                                                 <span class=\"pull-right\">{{objective.objective_curi}}</span>\n");
      out.write("                                         \n");
      out.write("                                            </div>\n");
      out.write("                                            <div class=\"lessonplan-text\"> \n");
      out.write("                                                <span class=\"pull-left\">{{objective.objective_subject}}</span>\n");
      out.write("                                                <span class=\"pull-right\">{{objective.objective_chapter}}</span>\n");
      out.write("                                            </div>\n");
      out.write("                                            <div> \n");
      out.write("                                                <p>{{objective.objective_description}}</p>\n");
      out.write("\n");
      out.write("                                                <!--<p> Home Activity: Color objects of different shapes </p>-->\n");
      out.write("                                            </div><!--\n");
      out.write("                                            -->\n");
      out.write("                                        </div>\n");
      out.write("\n");
      out.write("                                    </div>\n");
      out.write("                                </div>\n");
      out.write("                         \n");
      out.write("                                \n");
      out.write("                            </div>\n");
      out.write("                        </uib-tab>\n");
      out.write("\n");
      out.write("                    </uib-tabset>\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("\n");
      out.write("\n");
      out.write("        </div>\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("        <script type=\"text/javascript\" src=\"js/angular.js\"></script>\n");
      out.write("        <script type=\"text/javascript\" src=\"js/ui-bootstrap-tpls-1.3.3.min.js\"></script>\n");
      out.write("        <script type=\"text/javascript\" src=\"js/objective.js\"></script>\n");
      out.write("        <script type=\"text/javascript\" src=\"js/rpTable.js\"></script>\n");
      out.write("        <script type=\"text/javascript\" src=\"js/Services/CommonService.js\"></script>\n");
      out.write("        <script type='text/javascript' src='js/angular-animate.min.js'></script>\n");
      out.write("        <script type='text/javascript' src='js/toast.js'></script>\n");
      out.write("        <script type='text/javascript' src='js/Services/ObjectiveService.js'></script>\n");
      out.write("        <script type='text/javascript' src='js/Services/ValidationService.js'></script>\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("    </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
