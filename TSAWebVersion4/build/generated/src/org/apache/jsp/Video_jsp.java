package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class Video_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <title>JSP Page</title>\n");
      out.write("        <script type='text/javascript' src='js/loader.js'></script>\n");
      out.write("        <link rel=\"stylesheet\" type=\"text/css\" href=\"css/loader.css\">\n");
      out.write("        <link rel=\"stylesheet\" type=\"text/css\" href=\"css/bootstrap.min.css\">\n");
      out.write("\n");
      out.write("        <link rel=\"stylesheet\" type=\"text/css\" href=\"css/newcss.css\">\n");
      out.write("        <link rel=\"stylesheet\" type=\"text/css\" href=\"css/video.css\">\n");
      out.write("        <link rel=\"stylesheet\" type=\"text/css\" href=\"css/Toast.css\">\n");
      out.write("        <!--<link href='https://fonts.googleapis.com/css?family=Raleway:400,500,800' rel='stylesheet' type='text/css'>-->\n");
      out.write("\n");
      out.write("        <link rel=\"stylesheet\" type=\"text/css\" href=\"css/dropzone.min.css\">\n");
      out.write("    </head>\n");
      out.write("    <body ng-app=\"video\" ng-controller=\"videoctrl\">\n");
      out.write("        <nav class=\"navbar navbar-default web-color navbar-fixed-top\">\n");
      out.write("            <div class=\"container-fluid\">\n");
      out.write("                <div class=\"navbar-header\">\n");
      out.write("                    <a class=\"navbar-brand text-color\" href=\"#\">\n");
      out.write("                        <!--<img alt=\"Brand\" src=\"Images/logo.png\">-->\n");
      out.write("                        Teacher Support Application\n");
      out.write("                    </a>\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("        </nav>\n");
      out.write("        <div class=\"container\" style=\"padding-top:60px;\">\n");
      out.write("            <div class=\"row\">\n");
      out.write("                <div class=\"col-lg-12 col-md-12 col-sm-12\">\n");
      out.write("                    <uib-tabset active=\"activeJustified\" justified=\"true\" class=\"vertical-layout\">\n");
      out.write("                        <uib-tab index=\"0\" heading=\" Create Activity\">\n");
      out.write("                            <!--//tab content1-->\n");
      out.write("\n");
      out.write("                            <div class=\"row\">\n");
      out.write("                                <div class=\"form-group col-lg-6 col-md-6 col-sm-12\">\n");
      out.write("                                    <label for=\"curiicullum\">Choose Curricullum</label>\n");
      out.write("                                    <select class=\"form-control\" id=\"curiicullum\" ng-model=\"curi.curicullumid[0]\" ng-options=\"curi.curicullum for curi in curicullum\" ng-change=\"Fetch_class(curi.curicullumid[0])\" required>\n");
      out.write("                                        <option disabled selected value=\"\">Choose your option</option>\n");
      out.write("\n");
      out.write("                                    </select>\n");
      out.write("                                </div>\n");
      out.write("                                <div class=\"form-group col-lg-6 col-md-6 col-sm-12\">\n");
      out.write("                                    <label for=\"Class\">Choose Class</label>\n");
      out.write("                                    <select class=\"form-control\" id=\"Class\" ng-model=\"class.class_id[0]\" ng-options=\"class.class_name for class in class\" ng-change=\"Fetch_Subject(class.class_id[0])\" required>\n");
      out.write("                                        <option disabled selected value=\"\">Choose your option</option>\n");
      out.write("                                    </select>\n");
      out.write("                                </div>\n");
      out.write("                                <div class=\"form-group col-lg-6 col-md-6 col-sm-12\">\n");
      out.write("                                    <label for=\"Subject\">Choose Subject</label>\n");
      out.write("                                    <select class=\"form-control\" id=\"Subject\" ng-model=\"subject.subject_id[0]\" ng-options=\"subject.subject_name for subject in subject\" ng-change=\"Fetch_chapter(subject.subject_id[0])\" required>\n");
      out.write("                                        <option disabled selected value=\"\">Choose Subject</option>\n");
      out.write("                                        <option ng:repeat=\"subject in subject\" value=\"{{subject.subject_id}}\">{{subject.subject_name}}</option>\n");
      out.write("\n");
      out.write("                                    </select>\n");
      out.write("\n");
      out.write("                                </div>\n");
      out.write("\n");
      out.write("                                <div class=\"form-group col-lg-6 col-md-6 col-sm-12\">\n");
      out.write("                                    <label for=\"Chapter\">Choose Chapter</label>\n");
      out.write("                                    <select class=\"form-control\" id=\"Chapter\" ng-model=\"chapter.chapter_id[0]\" ng-options=\"chapter.chapter_name for chapter in chapter\" ng-change=\"Select_chapter(chapter.chapter_id[0])\" required>\n");
      out.write("                                        <option disabled selected value=\"\">Choose Chapter</option>\n");
      out.write("                                    </select>\n");
      out.write("\n");
      out.write("                                </div>\n");
      out.write("                                <div class=\"form-group col-lg-6 col-md-6 col-sm-12\">\n");
      out.write("                                    <label for=\"Name\">Write Name</label>\n");
      out.write("                                    <input type=\"text\" class=\"form-control\" placeholder=\"Video Name\" id=\"Name\" ng-model=\"data.videoname\">\n");
      out.write("\n");
      out.write("                                </div>\n");
      out.write("\n");
      out.write("                                <div class='form-group col-lg-6 col-mg-6 col-sm-12'>\n");
      out.write("                                    <label for=\"A1Description\">Videos Upload</label>\n");
      out.write("                                    <div class=\"dropzone\" options=\"dzOptions\" callbacks=\"dzCallbacks\" methods=\"dzMethods\" ng-dropzone></div>\n");
      out.write("                                </div>\n");
      out.write("\n");
      out.write("                                <button type=\"button\" class=\"btn btn-success pull-right\" ng-click=\"createvideo(data)\">Upload</button>\n");
      out.write("                                </form>\n");
      out.write("\n");
      out.write("\n");
      out.write("                            </div>\n");
      out.write("\n");
      out.write("\n");
      out.write("                        </uib-tab>\n");
      out.write("                        <uib-tab index=\"1\" heading=\"Use Activity\" select=\"fetch_video()\" ng-cloak>\n");
      out.write("                            <div class=\"container\">\n");
      out.write("                                <div class=\"row\">\n");
      out.write("                                    <form>\n");
      out.write("                                        <div class=\"form-group col-lg-6 col-md-6 col-sm-12\">\n");
      out.write("                                            <label for=\"Class\">Choose Class</label>\n");
      out.write("                                            <select class=\"form-control\" id=\"Class\">\n");
      out.write("                                                <option disabled selected>1</option>\n");
      out.write("                                                <option>2</option>\n");
      out.write("                                                <option>3</option>\n");
      out.write("                                                <option>4</option>\n");
      out.write("                                                <option>5</option>\n");
      out.write("                                            </select>\n");
      out.write("                                        </div>\n");
      out.write("                                        <div class=\"form-group col-lg-6 col-md-6 col-sm-12\">\n");
      out.write("                                            <label for=\"Subject\">Choose Subject</label>\n");
      out.write("                                            <select class=\"form-control\" id=\"Subject\">\n");
      out.write("                                                <option disabled selected>1</option>\n");
      out.write("                                                <option>2</option>\n");
      out.write("                                                <option>3</option>\n");
      out.write("                                                <option>4</option>\n");
      out.write("                                                <option>5</option>\n");
      out.write("                                            </select>\n");
      out.write("                                        </div>\n");
      out.write("                                    </form>\n");
      out.write("                                </div>\n");
      out.write("                                <div class=\"row\">\n");
      out.write("                                    <div class=\"col-lg-12 col-md-12 col-sm-12\">\n");
      out.write("                                        <div class=\"video-heading\">\n");
      out.write("                                            Chapter Name\n");
      out.write("                                        </div>\n");
      out.write("                                        <div class=\"video-content\" ng-repeat=\"video in videodata\" ng-click=\"openvideomodal(this)\">\n");
      out.write("                                            <div class=\"video-text\">\n");
      out.write("                                                {{video.videoname}}\n");
      out.write("                                            </div>\n");
      out.write("                                            <div class=\"video-icon\">\n");
      out.write("                                                <img src=\"Images/screen-player.png\">\n");
      out.write("                                            \n");
      out.write("                                            </div>\n");
      out.write("                                          \n");
      out.write("                                        </div>\n");
      out.write("\n");
      out.write("                                    </div>\n");
      out.write("\n");
      out.write("                                </div>\n");
      out.write("\n");
      out.write("                            </div>\n");
      out.write("                        </uib-tab>\n");
      out.write("\n");
      out.write("                    </uib-tabset>\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("\n");
      out.write("\n");
      out.write("        </div>\n");
      out.write("<script type=\"text/ng-template\" id=\"videomodel.html\">\n");
      out.write("        <div class=\"modal-header\">\n");
      out.write("            <h3 class=\"modal-title\">{{videoname}}</h3>\n");
      out.write("        </div>\n");
      out.write("        <div class=\"modal-body\">\n");
      out.write("           <video width=\"580\" height=\"200\" controls>\n");
      out.write("                                                <source ng-src=\"{{videomodel}}\" type=\"video/mp4\">\n");
      out.write("                                                <source ng-src=\"{{videomodel}}\" type=\"video/ogg\">\n");
      out.write("                                              \n");
      out.write("                                            </video>\n");
      out.write("        </div>\n");
      out.write("        <div class=\"modal-footer\">\n");
      out.write("          \n");
      out.write("            <button class=\"btn btn-warning\" type=\"button\" ng-click=\"cancel()\">Cancel</button>\n");
      out.write("        </div>\n");
      out.write("        </script>\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("        <script type=\"text/javascript\" src=\"js/angular.js\"></script>\n");
      out.write("        <script type=\"text/javascript\" src=\"js/ui-bootstrap-tpls-1.3.3.min.js\"></script>\n");
      out.write("        <script type=\"text/javascript\" src=\"js/video.js\"></script>\n");
      out.write("        <script type='text/javascript' src='js/dropzone.min.js'></script>\n");
      out.write("        <script type='text/javascript' src='js/ngDropzone.js'></script>\n");
      out.write("        <script type=\"text/javascript\" src=\"js/Services/CommonService.js\"></script>\n");
      out.write("        <script type='text/javascript' src='js/angular-animate.min.js'></script>\n");
      out.write("        <script type='text/javascript' src='js/toast.js'></script>\n");
      out.write("        <script type='text/javascript' src='js/Services/ObjectiveService.js'></script>\n");
      out.write("        <script type='text/javascript' src='js/Services/ValidationService.js'></script>\n");
      out.write("        <script type=\"text/javascript\" src=\"js/Services/VideoService.js\"></script>\n");
      out.write("    </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
