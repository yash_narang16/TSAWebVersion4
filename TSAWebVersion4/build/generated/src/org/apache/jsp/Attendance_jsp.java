package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class Attendance_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <title>JSP Page</title>\n");
      out.write("\n");
      out.write("        <link rel=\"stylesheet\" type=\"text/css\" href=\"css/bootstrap.min.css\">\n");
      out.write("        <link rel=\"stylesheet\" type=\"text/css\" href=\"css/newcss.css\">\n");
      out.write("        <link rel=\"stylesheet\" type=\"text/css\" href=\"css/attendance.css\">\n");
      out.write("        <link rel=\"stylesheet\" type=\"text/css\" href=\"css/Toast.cssmy\">\n");
      out.write("        <link rel=\"stylesheet\" type=\"text/css\" href=\"css/new loader.css\">\n");
      out.write("\n");
      out.write("    </head>\n");
      out.write("    <body ng-app=\"attendance\" ng-controller=\"attendancectrl\">\n");
      out.write("        <nav class=\"navbar navbar-default web-color navbar-fixed-top\">\n");
      out.write("            <div class=\"container-fluid\">\n");
      out.write("                <div class=\"navbar-header\">\n");
      out.write("                    <a class=\"navbar-brand text-color\" href=\"#\">\n");
      out.write("                        <!--<img alt=\"Brand\" src=\"Images/logo.png\">-->\n");
      out.write("                        Teacher Support Application\n");
      out.write("                    </a>\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("        </nav>\n");
      out.write("        <div class=\"container\" style=\"padding: 60px;\">\n");
      out.write("            <div class=\"loader-wrapper\" ng-show=\"loaderhide\">\n");
      out.write("                <div class=\"loader\"></div>\n");
      out.write("            </div>\n");
      out.write("\n");
      out.write("            <uib-tabset active=\"activeJustified\" justified=\"true\" ng-cloak>\n");
      out.write("                <uib-tab index=\"0\" heading=\"Record Attendance\">\n");
      out.write("\n");
      out.write("                    <div class=\"row\">\n");
      out.write("                        <div class=\"form-group col-lg-6 col-md-6 col-sm-12\">\n");
      out.write("                            <label for=\"Class\">Choose Class</label>\n");
      out.write("                            <select class=\"form-control\" id=\"Class\" ng-model=\"class.class_id[0]\" ng-options=\"class.class_name for class in class\" ng-change=\"Fetch_Subject(class.class_id[0])\" required>\n");
      out.write("                                <option disabled selected value=\"\">Choose your option</option>\n");
      out.write("                            </select>\n");
      out.write("                        </div>\n");
      out.write("                        <!--                         <div class=\"col-lg-6 col-md-6 col-sm-12\">\n");
      out.write("                                                     <div class=\"curdate-box pull-right\">\n");
      out.write("                                                        {{curdate | date:'yyyy-MM-dd'}}\n");
      out.write("                                                     </div>\n");
      out.write("                                                </div>-->\n");
      out.write("                    </div>\n");
      out.write("\n");
      out.write("\n");
      out.write("                    <div class=\"row\">\n");
      out.write("                        <div class=\"col-lg-12 col-md-12 col-sm-12\">\n");
      out.write("                            <div class=\"heading-attendance\">\n");
      out.write("                              Student  Record\n");
      out.write("                                <div class=\"pull-right\" ng-show=\"showbutton\" ng-cloak>\n");
      out.write("                                     <button type=\"button\" class=\"btn btn-warning\" ng-click=\"clearAll()\">\n");
      out.write("                                    <span class=\"glyphicon glyphicon-remove\" aria-hidden=\"true\" ></span> Deselect All\n");
      out.write("                                </button>\n");
      out.write("                                     <button type=\"button\" class=\"btn btn-primary\" ng-click=\"selectAll()\">\n");
      out.write("                                    <span class=\"glyphicon glyphicon-asterisk\" aria-hidden=\"true\" ></span> Select All\n");
      out.write("                                </button>\n");
      out.write("                                <button type=\"button\" class=\"btn btn-success\" ng-click=\"saveAttendance()\">\n");
      out.write("                                    <span class=\"glyphicon glyphicon-ok\" aria-hidden=\"true\" ></span> Submit\n");
      out.write("                                </button>\n");
      out.write("<!--                                <span> {{curdate| date:'yyyy-MM-dd'}}</span>-->\n");
      out.write("                                </div>\n");
      out.write("                            </div>\n");
      out.write("                        </div>\n");
      out.write("                        <div class=\"col-lg-3 col-md-3 col-sm-12\" ng-repeat=\"student in studentdata track by $index\">\n");
      out.write("\n");
      out.write("                            <div class=\"student-name\">\n");
      out.write("                                <input  id=\"{{student.studentname}}\" class=\"checkbox-custom\" type=\"checkbox\" ng-model=\"student.check\">\n");
      out.write("                                <label for=\"{{student.studentname}}\" class=\"checkbox-custom-label\">  {{student.studentname}}</label>  \n");
      out.write("\n");
      out.write("\n");
      out.write("                            </div>\n");
      out.write("\n");
      out.write("\n");
      out.write("                        </div>\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("                    </div>\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("                </uib-tab>\n");
      out.write("                <uib-tab index=\"1\" heading=\"View Attendance\" ng-cloak>\n");
      out.write("\n");
      out.write("                    <div class=\"row\">\n");
      out.write("                        <div class=\"form-group col-lg-6 col-md-6 col-sm-12\">\n");
      out.write("\n");
      out.write("                            <label for=\"Class\">Choose Class</label>\n");
      out.write("                            <select class=\"form-control\" id=\"Class\">\n");
      out.write("                                <option>1</option>\n");
      out.write("                                <option>2</option>\n");
      out.write("                                <option>3</option>\n");
      out.write("                                <option>4</option>\n");
      out.write("                                <option>5</option>\n");
      out.write("                            </select>\n");
      out.write("\n");
      out.write("\n");
      out.write("                        </div>\n");
      out.write("                        <div class=\"form-group col-lg-6 col-md-6 col-sm-12\">\n");
      out.write("                            <label for=\"Class\">Select Date</label>\n");
      out.write("\n");
      out.write("                            <p class=\"input-group\">\n");
      out.write("                                <input type=\"text\" class=\"form-control\" uib-datepicker-popup=\"{{format}}\" ng-model=\"dt\" is-open=\"popup1.opened\" ng-change=\"filterdate(dt)\" datepicker-options=\"dateOptions\" ng-required=\"true\" close-text=\"Close\" alt-input-formats=\"altInputFormats\" />\n");
      out.write("                                <span class=\"input-group-btn\">\n");
      out.write("                                    <button type=\"button\" class=\"btn btn-default\" ng-click=\"open1()\"><i class=\"glyphicon glyphicon-calendar\"></i></button>\n");
      out.write("                                </span>\n");
      out.write("                            </p>\n");
      out.write("                        </div>\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("                    </div>\n");
      out.write("                    <table class=\"table\">\n");
      out.write("                        <thead>\n");
      out.write("                            <tr>\n");
      out.write("                                <th>Name</th>\n");
      out.write("                                <th ng-repeat=\"day in dayname track by $index\">{{day.substring(0, 1)}}</th>\n");
      out.write("\n");
      out.write("\n");
      out.write("                            </tr>\n");
      out.write("                        </thead>\n");
      out.write("                        <tbody>\n");
      out.write("                            <tr>\n");
      out.write("                                <td>Student name</td>\n");
      out.write("                                <td ng-repeat=\"day in dayname track by $index\">P</td>\n");
      out.write("                            </tr>\n");
      out.write("\n");
      out.write("                        </tbody>\n");
      out.write("                    </table>\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("                </uib-tab>\n");
      out.write("\n");
      out.write("            </uib-tabset>\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("        </div>\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("        <script type=\"text/javascript\" src=\"js/angular.js\"></script>\n");
      out.write("        <script type=\"text/javascript\" src=\"js/ui-bootstrap-tpls-1.3.3.min.js\"></script>\n");
      out.write("        <script type=\"text/javascript\" src=\"js/attendance.js\"></script>\n");
      out.write("        <script type=\"text/javascript\" src=\"js/Services/CommonService.js\"></script>\n");
      out.write("        <script type=\"text/javascript\" src=\"js/Services/AttendanceService.js\"></script>\n");
      out.write("        <script type=\"text/javascript\" src=\"js/angular-animate.min.js\"></script>\n");
      out.write("        <script type=\"text/javascript\" src=\"js/toast.js\"></script>\n");
      out.write("\n");
      out.write("\n");
      out.write("    </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
