package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class Mapping_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <title>JSP Page</title>\n");
      out.write("        <script type='text/javascript' src='js/loader.js'></script>\n");
      out.write("        <link rel=\"stylesheet\" type=\"text/css\" href=\"css/loader.css\">\n");
      out.write("        <link rel=\"stylesheet\" type=\"text/css\" href=\"css/bootstrap.min.css\">\n");
      out.write("\n");
      out.write("        <link rel=\"stylesheet\" type=\"text/css\" href=\"css/newcss.css\">\n");
      out.write("        <link rel=\"stylesheet\" type=\"text/css\" href=\"css/calendar.css\">\n");
      out.write("        <link rel=\"stylesheet\" type=\"text/css\" href=\"css/mapping.css\">\n");
      out.write("\n");
      out.write("    </head>\n");
      out.write("    <body ng-app=\"mapping\" ng-controller=\"mappingctrl\">\n");
      out.write("        <nav class=\"navbar navbar-default web-color navbar-fixed-top\">\n");
      out.write("            <div class=\"container-fluid\">\n");
      out.write("                <div class=\"navbar-header\">\n");
      out.write("                    <a class=\"navbar-brand text-color\" href=\"Home.jsp\">\n");
      out.write("                        <!--<img alt=\"Brand\" src=\"Images/logo.png\">-->\n");
      out.write("                        Teacher Support Application\n");
      out.write("                    </a>\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("        </nav>\n");
      out.write("        <div class=\"container\" style=\"padding:60px;\">\n");
      out.write("            <div class=\"row\">\n");
      out.write("                <div class=\"col-lg-12 col-md-12 col-sm-12\">\n");
      out.write("                    <uib-tabset active=\"activeJustified\" justified=\"true\" class=\"vertical-layout\">\n");
      out.write("                        <uib-tab index=\"0\" heading=\" Map Lesson Plan\">\n");
      out.write("                            <!--//tab content1-->\n");
      out.write("                            <div class=\"row\">\n");
      out.write("                                <form>\n");
      out.write("                                    <div class=\"form-group col-lg-6 col-md-6 col-sm-12\">\n");
      out.write("                                        <label for=\"Class\">Choose Class</label>\n");
      out.write("                                        <select class=\"form-control\" id=\"Class\">\n");
      out.write("                                            <option>1</option>\n");
      out.write("                                            <option>2</option>\n");
      out.write("                                            <option>3</option>\n");
      out.write("                                            <option>4</option>\n");
      out.write("                                            <option>5</option>\n");
      out.write("                                        </select>\n");
      out.write("                                    </div>\n");
      out.write("\n");
      out.write("                                    <div class=\"form-group col-lg-6 col-md-6 col-sm-12\">\n");
      out.write("                                        <label for=\"Subject\">Choose Subject</label>\n");
      out.write("                                        <select class=\"form-control\" id=\"Subject\">\n");
      out.write("                                            <option>1</option>\n");
      out.write("                                            <option>2</option>\n");
      out.write("                                            <option>3</option>\n");
      out.write("                                            <option>4</option>\n");
      out.write("                                            <option>5</option>\n");
      out.write("                                        </select>\n");
      out.write("                                    </div>\n");
      out.write("                                </form>\n");
      out.write("\n");
      out.write("                            </div>\n");
      out.write("                            <div class=\"row\">\n");
      out.write("                                <div class=\"col-lg-3 col-md-3 col-sm-12\">\n");
      out.write("                                    <div class=\"panel panel-default\">\n");
      out.write("                                        <div class=\"panel-heading lessonplan-card\">\n");
      out.write("                                            Lesson plan\n");
      out.write("                                            <div class=\"icon-tools pull-right\">\n");
      out.write("\n");
      out.write("                                                <span class=\"glyphicon glyphicon-modal-window\" ng-click=\"open('lg')\"></span>\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("                                                <span class=\"glyphicon glyphicon-calendar\" ng-click=\"opencalendar()\"></span>\n");
      out.write("                                                <span class=\"glyphicon glyphicon-tag\"></span>\n");
      out.write("                                            </div>\n");
      out.write("                                        </div>\n");
      out.write("                                        <div class=\"panel-body\">\n");
      out.write("                                            <!--<input class=\"form-control\" uib-datepicker-popup=\"{{format}}\" ng-model=\"dt\" is-open=\"popup1.opened\" datepicker-options=\"dateOptions\" ng-required=\"true\" close-text=\"Close\" alt-input-formats=\"altInputFormats\" style=\"display:none;\">-->\n");
      out.write("                                            <div class=\"lessonplan-text\"> \n");
      out.write("                                                <span class=\"pull-left\">Third Class</span>\n");
      out.write("                                                <!--<span class=\"pull-right\">{{dt | date:'yyyy-MM-dd'}}</span>-->\n");
      out.write("                                            </div>\n");
      out.write("                                            <div class=\"lessonplan-text\"> \n");
      out.write("                                                <span class=\"pull-left\">Maths</span>\n");
      out.write("                                                <span class=\"pull-right\">45 min</span>\n");
      out.write("                                            </div>\n");
      out.write("                                            <div> \n");
      out.write("                                                <p> Objectives: To understand different shapes and spaces </p>\n");
      out.write("\n");
      out.write("                                                <p> Home Activity: Color objects of different shapes </p>\n");
      out.write("                                            </div><!--\n");
      out.write("                                            -->\n");
      out.write("                                        </div>\n");
      out.write("\n");
      out.write("                                    </div>\n");
      out.write("                                </div>\n");
      out.write("                                <div class=\"col-lg-3 col-md-3 col-sm-12\">\n");
      out.write("                                    <div class=\"panel panel-default\">\n");
      out.write("                                        <div class=\"panel-heading lessonplan-card\">\n");
      out.write("                                            Lesson plan\n");
      out.write("                                            <div class=\"icon-tools pull-right\">\n");
      out.write("\n");
      out.write("                                                <span class=\"glyphicon glyphicon-modal-window\" ng-click=\"open('lg')\"></span>\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("                                                <span class=\"glyphicon glyphicon-calendar\" ng-click=\"opencalendar()\"></span>\n");
      out.write("                                                <span class=\"glyphicon glyphicon-tag\"></span>\n");
      out.write("                                            </div>\n");
      out.write("                                        </div>\n");
      out.write("                                        <div class=\"panel-body\">\n");
      out.write("                                            <!--<input class=\"form-control\" uib-datepicker-popup=\"{{format}}\" ng-model=\"dt\" is-open=\"popup1.opened\" datepicker-options=\"dateOptions\" ng-required=\"true\" close-text=\"Close\" alt-input-formats=\"altInputFormats\" style=\"display:none;\">-->\n");
      out.write("                                            <div class=\"lessonplan-text\"> \n");
      out.write("                                                <span class=\"pull-left\">Third Class</span>\n");
      out.write("                                                <!--<span class=\"pull-right\">{{dt | date:'yyyy-MM-dd'}}</span>-->\n");
      out.write("                                            </div>\n");
      out.write("                                            <div class=\"lessonplan-text\"> \n");
      out.write("                                                <span class=\"pull-left\">Maths</span>\n");
      out.write("                                                <span class=\"pull-right\">45 min</span>\n");
      out.write("                                            </div>\n");
      out.write("                                            <div> \n");
      out.write("                                                <p> Objectives: To understand different shapes and spaces </p>\n");
      out.write("\n");
      out.write("                                                <p> Home Activity: Color objects of different shapes </p>\n");
      out.write("                                            </div><!--\n");
      out.write("                                            -->\n");
      out.write("                                        </div>\n");
      out.write("\n");
      out.write("                                    </div>\n");
      out.write("                                </div>\n");
      out.write("                                \n");
      out.write("                            </div>\n");
      out.write("\n");
      out.write("                        </uib-tab>\n");
      out.write("                        <uib-tab index=\"1\" heading=\"View Mapped Lesson Plan\" ng-init=\"loadEvents()\">\n");
      out.write("                            <calendar ng-model=\"currentDate\" calendar-mode=\"mode\" event-source=\"eventSource\"\n");
      out.write("                                      range-changed=\"reloadSource(startTime, endTime)\"\n");
      out.write("                                      event-selected=\"onEventSelected(event)\"\n");
      out.write("                                      time-selected=\"onTimeSelected(selectedTime)\"></calendar>\n");
      out.write("\n");
      out.write("                        </uib-tab>\n");
      out.write("\n");
      out.write("                    </uib-tabset>\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("        </div>\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("        <script type=\"text/javascript\" src=\"js/angular.js\"></script>\n");
      out.write("        <script type=\"text/javascript\" src=\"js/ui-bootstrap-tpls-1.3.3.min.js\"></script>\n");
      out.write("        <script type=\"text/javascript\" src=\"js/calendar-tpls.min.js\"></script>\n");
      out.write("        <script type=\"text/javascript\" src=\"js/mapping.js\"></script>\n");
      out.write("        <script type=\"text/ng-template\" id=\"openview.html\">\n");
      out.write("            <div class=\"modal-header\">\n");
      out.write("            <h3 class=\"modal-title\">Lesson Plan</h3>\n");
      out.write("            </div>\n");
      out.write("            <div class=\"modal-body\">\n");
      out.write("\n");
      out.write("            </div>\n");
      out.write("            <div class=\"modal-footer\">\n");
      out.write("            <button class=\"btn btn-primary\" type=\"button\" ng-click=\"ok()\">OK</button>\n");
      out.write("            <button class=\"btn btn-warning\" type=\"button\" ng-click=\"cancel()\">Cancel</button>\n");
      out.write("            </div>\n");
      out.write("        </script>\n");
      out.write("<script type=\"text/ng-template\" id=\"calendar.html\">\n");
      out.write("            <div class=\"modal-header\">\n");
      out.write("            <h3 class=\"modal-title\">Lesson Plan</h3>\n");
      out.write("            </div>\n");
      out.write("            <div class=\"modal-body\" ng-init=\"loadEvents()\">\n");
      out.write("  <calendar ng-model=\"currentDate\" calendar-mode=\"mode\" event-source=\"eventSource\"\n");
      out.write("                                      range-changed=\"reloadSource(startTime, endTime)\"\n");
      out.write("                                      event-selected=\"onEventSelected(event)\"\n");
      out.write("                                      time-selected=\"onTimeSelected(selectedTime)\"></calendar>\n");
      out.write("            </div>\n");
      out.write("            <div class=\"modal-footer\">\n");
      out.write("            <button class=\"btn btn-primary\" type=\"button\" ng-click=\"ok()\">OK</button>\n");
      out.write("            <button class=\"btn btn-warning\" type=\"button\" ng-click=\"cancel()\">Cancel</button>\n");
      out.write("            </div>\n");
      out.write("        </script>\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("    </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
