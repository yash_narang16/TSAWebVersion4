package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class Login_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n");
      out.write("        <title>JSP Page</title>\n");
      out.write("        <script type='text/javascript' src='js/loader.js'></script>\n");
      out.write("        <link rel=\"stylesheet\" type=\"text/css\" href=\"css/loader.css\">\n");
      out.write("        <link rel=\"stylesheet\" type=\"text/css\" href=\"css/bootstrap.min.css\">\n");
      out.write("\n");
      out.write("        <link rel=\"stylesheet\" type=\"text/css\" href=\"css/newcss.css\">\n");
      out.write("        <link rel=\"stylesheet\" type=\"text/css\" href=\"css/Toast.css\">\n");
      out.write("\n");
      out.write("    </head>\n");
      out.write("    <body ng-app=\"app\" ng-controller=\"login\">\n");
      out.write("\n");
      out.write("        <div class=\"container-fluid backgroun-img\">\n");
      out.write("            <div class=\"row\">\n");
      out.write("                <div class=\"col-lg-6 col-md-12 col-sm-12\">\n");
      out.write("\n");
      out.write("                    <span class=\"landingpage text-color\">Teacher Support Application</span>\n");
      out.write("                </div>\n");
      out.write("\n");
      out.write("                <div class=\"col-lg-1 col-lg-offset-4 col-md-12 col-sm-12\" style=\"padding: 6px;\">\n");
      out.write("                    <button type=\"button\" class=\"btn btn-button-default\" ng-click=\"gotoBottom()\">Login Here</button>\n");
      out.write("                </div>\n");
      out.write("                <div class=\"col-lg-1 col-md-12 col-sm-12\" style=\"padding: 6px;\">\n");
      out.write("                    <button type=\"button\" class=\"btn btn-button-default\">Sign up Here</button>\n");
      out.write("                </div>\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("            </div>\n");
      out.write("        </div>\n");
      out.write("        <div class=\"content-page\">\n");
      out.write("\n");
      out.write("            <div class=\"row\">\n");
      out.write("                <div class=\"col-lg-3 col-md-3 col-sm-12 divde-line\">\n");
      out.write("                    <div class=\"image-card\">\n");
      out.write("                    <div class=\"thumbnail\">\n");
      out.write("                        <img src=\"Images/blackboard.svg\" alt=\"...\">\n");
      out.write("                        <div class=\"caption\">\n");
      out.write("                            <h3>Thumbnail label</h3>\n");
      out.write("                            <p>...</p>\n");
      out.write("                            <!--<p><a href=\"#\" class=\"btn btn-primary\" role=\"button\">Button</a> <a href=\"#\" class=\"btn btn-default\" role=\"button\">Button</a></p>-->\n");
      out.write("                        </div>\n");
      out.write("                    </div>\n");
      out.write("                    </div>\n");
      out.write("                </div>\n");
      out.write("                <div class=\"col-lg-3 col-md-3 col-sm-12 divde-line\">\n");
      out.write("                     <div class=\"image-card\">\n");
      out.write("                    <div class=\"thumbnail\">\n");
      out.write("                        <img src=\"Images/alarm-clock.svg\" alt=\"...\">\n");
      out.write("                        <div class=\"caption\">\n");
      out.write("                            <h3>Thumbnail label</h3>\n");
      out.write("                            <p>...</p>\n");
      out.write("                            <!--<p><a href=\"#\" class=\"btn btn-primary\" role=\"button\">Button</a> <a href=\"#\" class=\"btn btn-default\" role=\"button\">Button</a></p>-->\n");
      out.write("                        </div>\n");
      out.write("                    </div>\n");
      out.write("                    </div>\n");
      out.write("                </div>\n");
      out.write("                <div class=\"col-lg-3 col-md-3 col-sm-12 divde-line\">\n");
      out.write("                  <div class=\"image-card\">\n");
      out.write("                    <div class=\"thumbnail\">\n");
      out.write("                        <img src=\"Images/open-book.svg\" alt=\"...\">\n");
      out.write("                        <div class=\"caption\">\n");
      out.write("                            <h3>Thumbnail label</h3>\n");
      out.write("                            <p>...</p>\n");
      out.write("                            <!--<p><a href=\"#\" class=\"btn btn-primary\" role=\"button\">Button</a> <a href=\"#\" class=\"btn btn-default\" role=\"button\">Button</a></p>-->\n");
      out.write("                        </div>\n");
      out.write("                    </div>\n");
      out.write("                    </div>\n");
      out.write("                </div>\n");
      out.write("                <div class=\"col-lg-3 col-md-3 col-sm-12\">\n");
      out.write("                  <div class=\"image-card\">\n");
      out.write("                    <div class=\"thumbnail\">\n");
      out.write("                        <img src=\"Images/diploma.svg\" alt=\"...\">\n");
      out.write("                        <div class=\"caption\">\n");
      out.write("                            <h3>Thumbnail label</h3>\n");
      out.write("                            <p>...</p>\n");
      out.write("                            <!--<p><a href=\"#\" class=\"btn btn-primary\" role=\"button\">Button</a> <a href=\"#\" class=\"btn btn-default\" role=\"button\">Button</a></p>-->\n");
      out.write("                        </div>\n");
      out.write("                    </div>\n");
      out.write("                    </div>\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("        </div>\n");
      out.write("        <div class=\"backgroun-img2\"  id=\"login\">\n");
      out.write("            <div class=\"row\" style=\"padding-top:200px;\">\n");
      out.write("                <div class=\"col-lg-4 col-lg-offset-4\">\n");
      out.write("                    <div class=\"well well-lg login\">\n");
      out.write("                        <form>\n");
      out.write("                            <div class=\"row\">\n");
      out.write("                                <div class=\"form-group col-lg-12\">\n");
      out.write("                                    <label for=\"exampleInputEmail1\">Email address</label>\n");
      out.write("                                    <input type=\"email\" class=\"form-control\" id=\"exampleInputEmail1\" placeholder=\"Email\" ng-model=\"user.username\">\n");
      out.write("                                </div>\n");
      out.write("                                <div class=\"form-group col-lg-12\">\n");
      out.write("                                    <label for=\"exampleInputPassword1\">Password</label>\n");
      out.write("                                    <input type=\"password\" class=\"form-control\" id=\"exampleInputPassword1\" placeholder=\"Password\" ng-model=\"user.password\">\n");
      out.write("                                </div>\n");
      out.write("\n");
      out.write("\n");
      out.write("                                <button type=\"button\" class=\"btn btn-success pull-right\" ng-click=\"userLogin(user)\">Submit</button>\n");
      out.write("\n");
      out.write("                       \n");
      out.write("                    </div>\n");
      out.write("                        </form>\n");
      out.write("                </div>\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("            </div>\n");
      out.write("        </div>\n");
      out.write("    </div>\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("    <script type=\"text/javascript\" src=\"js/angular.js\"></script>\n");
      out.write("    <script type=\"text/javascript\" src=\"js/ui-bootstrap-tpls-1.3.3.min.js\"></script>\n");
      out.write("    <script type=\"text/javascript\" src=\"js/login.js\"></script>\n");
      out.write("    <script type=\"text/javascript\" src=\"js/Services/LoginService.js\"></script>\n");
      out.write("    <script type=\"text/javascript\" src=\"js/Services/LocalStorage.js\"></script>\n");
      out.write("    <script type=\"text/javascript\" src=\"js/angular-animate.min.js\"></script>\n");
      out.write("    <script type=\"text/javascript\" src=\"js/toast.js\"></script>\n");
      out.write("    \n");
      out.write("\n");
      out.write("</body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
