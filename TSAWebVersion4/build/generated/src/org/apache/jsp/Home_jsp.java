package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class Home_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <title>JSP Page</title>\n");
      out.write("        <script type='text/javascript' src='js/loader.js'></script>\n");
      out.write("        <link rel=\"stylesheet\" type=\"text/css\" href=\"css/loader.css\">\n");
      out.write("        <link rel=\"stylesheet\" type=\"text/css\" href=\"css/bootstrap.min.css\">\n");
      out.write("        <link rel=\"stylesheet\" type=\"text/css\" href=\"css/newcss.css\">\n");
      out.write("        <link rel=\"stylesheet\" type=\"text/css\" href=\"css/home.css\">\n");
      out.write("    </head>\n");
      out.write("    <body ng-app=\"home\", ng-controller=\"homectrl\">\n");
      out.write("      <nav class=\"navbar navbar-default web-color navbar-fixed-top\">\n");
      out.write("            <div class=\"container-fluid\">\n");
      out.write("                <div class=\"navbar-header\">\n");
      out.write("                    <a class=\"navbar-brand text-color\" href=\"#\">\n");
      out.write("                        <!--<img alt=\"Brand\" src=\"Images/logo.png\">-->\n");
      out.write("                        Teacher Support Application\n");
      out.write("                    </a>\n");
      out.write("                </div>\n");
      out.write("                 <ul class=\"nav navbar-nav pull-right\">\n");
      out.write("      <li><a class =\"text-color\" href=\"#\">Home</a></li>\n");
      out.write("      <li><a class =\"text-color\" href=\"#\">Plan Your lesson</a></li>\n");
      out.write("      <li><a class =\"text-color\" href=\"#\">{{userdetails.user_name}}</a></li> \n");
      out.write("     \n");
      out.write("    </ul>\n");
      out.write("            </div>\n");
      out.write("        </nav>\n");
      out.write("        <div class=\"container-fluid\" style=\"padding-top: 130px;\">\n");
      out.write("            <div class=\"row\">\n");
      out.write("                <div class=\"col-lg-4 col-md-4 col-sm-12\">\n");
      out.write("                    <a href=\"index.jsp\">\n");
      out.write("                    <div class=\"panel panel-primary\">\n");
      out.write("                        <div class=\"panel-body\">\n");
      out.write("                            <div class=\"icon-back\">\n");
      out.write("                                <img src=\"Images/teacher-lecture-in-front-an-auditory.svg\" alt=\"mesage\">\n");
      out.write("                           \n");
      out.write("                            </div>\n");
      out.write("                            <div class=\"icon-text\">Lesson Plan</div>\n");
      out.write("                        </div>\n");
      out.write("                    </div>\n");
      out.write("                    </a>\n");
      out.write("                </div>\n");
      out.write("           \n");
      out.write("                <div class=\"col-lg-4 col-md-4 col-sm-12\">\n");
      out.write("                    <a href=\"Activity.jsp\">\n");
      out.write("                    <div class=\"panel panel-primary\">\n");
      out.write("                        <div class=\"panel-body\">\n");
      out.write("                            <div class=\"icon-back\">\n");
      out.write("                                <img src=\"Images/briefcase-and-tools-for-school.svg\" alt=\"mesage\">\n");
      out.write("                           \n");
      out.write("                            </div>\n");
      out.write("                            <div class=\"icon-text\">Activity</div>\n");
      out.write("                        </div>\n");
      out.write("                    </div>\n");
      out.write("                     </a>\n");
      out.write("                </div>\n");
      out.write("           \n");
      out.write("                <div class=\"col-lg-4 col-md-4 col-sm-12\">\n");
      out.write("                    <a href=\"Objective.jsp\">\n");
      out.write("                    <div class=\"panel panel-primary\">\n");
      out.write("                        <div class=\"panel-body\">\n");
      out.write("                            <div class=\"icon-back\">\n");
      out.write("                                <img src=\"Images/paper-sheets-with-text-lines-and-a-pen-at-right-side-from-top-view.svg\" alt=\"mesage\">\n");
      out.write("                           \n");
      out.write("                            </div>\n");
      out.write("                            <div class=\"icon-text\">Objectives</div>\n");
      out.write("                        </div>\n");
      out.write("                    </div>\n");
      out.write("                     </a>\n");
      out.write("                </div>\n");
      out.write("           \n");
      out.write("                <div class=\"col-lg-4 col-md-4 col-sm-12\">\n");
      out.write("                  \n");
      out.write("                    <div class=\"panel panel-primary\">\n");
      out.write("                        <div class=\"panel-body\">\n");
      out.write("                            <div class=\"icon-back\">\n");
      out.write("                                <img src=\"Images/movie-projection-to-class-students.svg\" alt=\"mesage\">\n");
      out.write("                           \n");
      out.write("                            </div>\n");
      out.write("                            <div class=\"icon-text\">Video Gallery</div>\n");
      out.write("                        </div>\n");
      out.write("                    </div>\n");
      out.write("                 \n");
      out.write("                </div>\n");
      out.write("            \n");
      out.write("                <div class=\"col-lg-4 col-md-4 col-sm-12\">\n");
      out.write("                    <div class=\"panel panel-primary\">\n");
      out.write("                        <div class=\"panel-body\">\n");
      out.write("                            <div class=\"icon-back\">\n");
      out.write("                                <img src=\"Images/books-and-clock-with-alarm.svg\" alt=\"mesage\">\n");
      out.write("                           \n");
      out.write("                            </div>\n");
      out.write("                            <div class=\"icon-text\">Record Attendance</div>\n");
      out.write("                        </div>\n");
      out.write("                    </div>\n");
      out.write("                </div>\n");
      out.write("           \n");
      out.write("                <div class=\"col-lg-4 col-md-4 col-sm-12\">\n");
      out.write("                    <div class=\"panel panel-primary\">\n");
      out.write("                        <div class=\"panel-body\">\n");
      out.write("                            <div class=\"icon-back\">\n");
      out.write("                                <img src=\"Images/descendant-graphic.svg\" alt=\"mesage\">\n");
      out.write("                           \n");
      out.write("                            </div>\n");
      out.write("                            <div class=\"icon-text\">Analyze Reports</div>\n");
      out.write("                        </div>\n");
      out.write("                    </div>\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("        </div>\n");
      out.write("        \n");
      out.write("        \n");
      out.write("        \n");
      out.write("        <script type=\"text/javascript\" src=\"js/angular.js\"></script>\n");
      out.write("        <script type=\"text/javascript\" src=\"js/ui-bootstrap-tpls-1.3.3.min.js\"></script>\n");
      out.write("        <script type=\"text/javascript\" src=\"js/home.js\"></script>\n");
      out.write("        <script type=\"text/javascript\" src=\"js/Services/LocalStorage.js\"></script>\n");
      out.write("      \n");
      out.write("    </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
