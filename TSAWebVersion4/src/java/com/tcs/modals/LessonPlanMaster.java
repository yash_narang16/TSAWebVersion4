/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tcs.modals;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Yash
 */
@Entity
@Table(name = "LessonPlan_Master", catalog = "TSA_V4", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "LessonPlanMaster.findAll", query = "SELECT l FROM LessonPlanMaster l"),
    @NamedQuery(name = "LessonPlanMaster.findById", query = "SELECT l FROM LessonPlanMaster l WHERE l.id = :id"),
    @NamedQuery(name = "LessonPlanMaster.findByVideoId", query = "SELECT l FROM LessonPlanMaster l WHERE l.videoId = :videoId"),
    @NamedQuery(name = "LessonPlanMaster.findByDuration", query = "SELECT l FROM LessonPlanMaster l WHERE l.duration = :duration")})
public class LessonPlanMaster implements Serializable {

    @OneToMany(mappedBy = "lessonplanId")
    private Collection<MappingMaster> mappingMasterCollection;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Lob
    @Column(name = "LessonPlan_name", length = 65535)
    private String lessonPlanname;
    @Lob
    @Column(name = "handouts", length = 65535)
    private String handouts;
    @Lob
    @Column(name = "worksheets", length = 65535)
    private String worksheets;
    @Column(name = "video_id")
    private Integer videoId;
    @Lob
    @Column(name = "Home_acitivity", length = 65535)
    private String homeacitivity;
    @Column(name = "Duration")
    private Integer duration;
    @Lob
    @Column(name = "Observation", length = 65535)
    private String observation;
    @JoinColumn(name = "Co_Scholastic_id", referencedColumnName = "id")
    @ManyToOne
    private Coscholaticmaster coScholasticid;
    @JoinColumn(name = "Activity_id", referencedColumnName = "id")
    @ManyToOne
    private Activity activityid;
    @JoinColumn(name = "Objective_id", referencedColumnName = "id")
    @ManyToOne
    private Objective objectiveid;
    @JoinColumn(name = "Rubric_id", referencedColumnName = "id")
    @ManyToOne
    private RubricMaster rubricid;
    @JoinColumn(name = "User_id", referencedColumnName = "id")
    @ManyToOne
    private User userid;

    public LessonPlanMaster() {
    }

    public LessonPlanMaster(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLessonPlanname() {
        return lessonPlanname;
    }

    public void setLessonPlanname(String lessonPlanname) {
        this.lessonPlanname = lessonPlanname;
    }

    public String getHandouts() {
        return handouts;
    }

    public void setHandouts(String handouts) {
        this.handouts = handouts;
    }

    public String getWorksheets() {
        return worksheets;
    }

    public void setWorksheets(String worksheets) {
        this.worksheets = worksheets;
    }

    public Integer getVideoId() {
        return videoId;
    }

    public void setVideoId(Integer videoId) {
        this.videoId = videoId;
    }

    public String getHomeacitivity() {
        return homeacitivity;
    }

    public void setHomeacitivity(String homeacitivity) {
        this.homeacitivity = homeacitivity;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public Coscholaticmaster getCoScholasticid() {
        return coScholasticid;
    }

    public void setCoScholasticid(Coscholaticmaster coScholasticid) {
        this.coScholasticid = coScholasticid;
    }

    public Activity getActivityid() {
        return activityid;
    }

    public void setActivityid(Activity activityid) {
        this.activityid = activityid;
    }

    public Objective getObjectiveid() {
        return objectiveid;
    }

    public void setObjectiveid(Objective objectiveid) {
        this.objectiveid = objectiveid;
    }

    public RubricMaster getRubricid() {
        return rubricid;
    }

    public void setRubricid(RubricMaster rubricid) {
        this.rubricid = rubricid;
    }

    public User getUserid() {
        return userid;
    }

    public void setUserid(User userid) {
        this.userid = userid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LessonPlanMaster)) {
            return false;
        }
        LessonPlanMaster other = (LessonPlanMaster) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.tcs.modals.LessonPlanMaster[ id=" + id + " ]";
    }

    @XmlTransient
    public Collection<MappingMaster> getMappingMasterCollection() {
        return mappingMasterCollection;
    }

    public void setMappingMasterCollection(Collection<MappingMaster> mappingMasterCollection) {
        this.mappingMasterCollection = mappingMasterCollection;
    }
    
}
