/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tcs.modals;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Yash
 */
@Entity
@Table(name = "objective", catalog = "TSA_V4", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Objective.findAll", query = "SELECT o FROM Objective o"),
    @NamedQuery(name = "Objective.findById", query = "SELECT o FROM Objective o WHERE o.id = :id"),
    @NamedQuery(name = "Objective.findByObjectivename", query = "SELECT o FROM Objective o WHERE o.objectivename = :objectivename"),
    @NamedQuery(name = "Objective.findByCuricullumid", query = "SELECT o FROM Objective o WHERE o.curicullumid = :curicullumid"),
    @NamedQuery(name = "Objective.findByClassid", query = "SELECT o FROM Objective o WHERE o.classid = :classid"),
    @NamedQuery(name = "Objective.findBySubjectid", query = "SELECT o FROM Objective o WHERE o.subjectid = :subjectid"),
    @NamedQuery(name = "Objective.findByDescription", query = "SELECT o FROM Objective o WHERE o.description = :description")})
public class Objective implements Serializable {

    @OneToMany(mappedBy = "objectiveid")
    private Collection<LessonPlanMaster> lessonPlanMasterCollection;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Column(name = "objectivename", length = 45)
    private String objectivename;
    @Column(name = "curicullumid")
    private Integer curicullumid;
    @Column(name = "classid")
    private Integer classid;
    @Column(name = "subjectid")
    private Integer subjectid;
    @Column(name = "description", length = 300)
    private String description;
    @JoinColumn(name = "chapterid", referencedColumnName = "id")
    @ManyToOne
    private ChapterMaster chapterid;

    public Objective() {
    }

    public Objective(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getObjectivename() {
        return objectivename;
    }

    public void setObjectivename(String objectivename) {
        this.objectivename = objectivename;
    }

    public Integer getCuricullumid() {
        return curicullumid;
    }

    public void setCuricullumid(Integer curicullumid) {
        this.curicullumid = curicullumid;
    }

    public Integer getClassid() {
        return classid;
    }

    public void setClassid(Integer classid) {
        this.classid = classid;
    }

    public Integer getSubjectid() {
        return subjectid;
    }

    public void setSubjectid(Integer subjectid) {
        this.subjectid = subjectid;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ChapterMaster getChapterid() {
        return chapterid;
    }

    public void setChapterid(ChapterMaster chapterid) {
        this.chapterid = chapterid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Objective)) {
            return false;
        }
        Objective other = (Objective) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.tcs.modals.Objective[ id=" + id + " ]";
    }

    @XmlTransient
    public Collection<LessonPlanMaster> getLessonPlanMasterCollection() {
        return lessonPlanMasterCollection;
    }

    public void setLessonPlanMasterCollection(Collection<LessonPlanMaster> lessonPlanMasterCollection) {
        this.lessonPlanMasterCollection = lessonPlanMasterCollection;
    }
    
}
