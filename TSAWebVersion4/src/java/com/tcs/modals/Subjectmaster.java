/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tcs.modals;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Yash
 */
@Entity
@Table(name = "Subject_master", catalog = "TSA_V4", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Subjectmaster.findAll", query = "SELECT s FROM Subjectmaster s"),
    @NamedQuery(name = "Subjectmaster.findById", query = "SELECT s FROM Subjectmaster s WHERE s.id = :id"),
    @NamedQuery(name = "Subjectmaster.findBySubjectName", query = "SELECT s FROM Subjectmaster s WHERE s.subjectName = :subjectName")})
public class Subjectmaster implements Serializable {

    @OneToMany(mappedBy = "subject")
    private Collection<User> userCollection;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Column(name = "subject_name", length = 45)
    private String subjectName;
    @OneToMany(mappedBy = "subjectid")
    private Collection<ChapterMaster> chapterMasterCollection;

    public Subjectmaster() {
    }

    public Subjectmaster(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    @XmlTransient
    public Collection<ChapterMaster> getChapterMasterCollection() {
        return chapterMasterCollection;
    }

    public void setChapterMasterCollection(Collection<ChapterMaster> chapterMasterCollection) {
        this.chapterMasterCollection = chapterMasterCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Subjectmaster)) {
            return false;
        }
        Subjectmaster other = (Subjectmaster) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.tcs.modals.Subjectmaster[ id=" + id + " ]";
    }

    @XmlTransient
    public Collection<User> getUserCollection() {
        return userCollection;
    }

    public void setUserCollection(Collection<User> userCollection) {
        this.userCollection = userCollection;
    }
    
}
