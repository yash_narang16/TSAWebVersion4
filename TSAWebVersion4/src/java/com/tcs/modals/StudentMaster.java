/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tcs.modals;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Yash
 */
@Entity
@Table(name = "Student_Master", catalog = "TSA_V4", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "StudentMaster.findAll", query = "SELECT s FROM StudentMaster s"),
    @NamedQuery(name = "StudentMaster.findById", query = "SELECT s FROM StudentMaster s WHERE s.id = :id")})
public class StudentMaster implements Serializable {

    @OneToMany(mappedBy = "studentId")
    private Collection<AttendanceMaster> attendanceMasterCollection;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Lob
    @Column(name = "Student_name", length = 65535)
    private String studentname;
    @JoinColumn(name = "class_id", referencedColumnName = "id")
    @ManyToOne
    private Classmaster classId;

    public StudentMaster() {
    }

    public StudentMaster(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStudentname() {
        return studentname;
    }

    public void setStudentname(String studentname) {
        this.studentname = studentname;
    }

    public Classmaster getClassId() {
        return classId;
    }

    public void setClassId(Classmaster classId) {
        this.classId = classId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof StudentMaster)) {
            return false;
        }
        StudentMaster other = (StudentMaster) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.tcs.modals.StudentMaster[ id=" + id + " ]";
    }

    @XmlTransient
    public Collection<AttendanceMaster> getAttendanceMasterCollection() {
        return attendanceMasterCollection;
    }

    public void setAttendanceMasterCollection(Collection<AttendanceMaster> attendanceMasterCollection) {
        this.attendanceMasterCollection = attendanceMasterCollection;
    }
    
}
