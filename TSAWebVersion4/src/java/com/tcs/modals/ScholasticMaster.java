/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tcs.modals;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Yash
 */
@Entity
@Table(name = "ScholasticMaster", catalog = "TSA_V4", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ScholasticMaster.findAll", query = "SELECT s FROM ScholasticMaster s"),
    @NamedQuery(name = "ScholasticMaster.findById", query = "SELECT s FROM ScholasticMaster s WHERE s.id = :id"),
    @NamedQuery(name = "ScholasticMaster.findByRubricType", query = "SELECT s FROM ScholasticMaster s WHERE s.rubricType = :rubricType")})
public class ScholasticMaster implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Column(name = "rubric_type", length = 45)
    private String rubricType;
    @OneToMany(mappedBy = "scholasticid")
    private Collection<RubricMaster> rubricMasterCollection;

    public ScholasticMaster() {
    }

    public ScholasticMaster(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRubricType() {
        return rubricType;
    }

    public void setRubricType(String rubricType) {
        this.rubricType = rubricType;
    }

    @XmlTransient
    public Collection<RubricMaster> getRubricMasterCollection() {
        return rubricMasterCollection;
    }

    public void setRubricMasterCollection(Collection<RubricMaster> rubricMasterCollection) {
        this.rubricMasterCollection = rubricMasterCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ScholasticMaster)) {
            return false;
        }
        ScholasticMaster other = (ScholasticMaster) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.tcs.modals.ScholasticMaster[ id=" + id + " ]";
    }
    
}
