/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tcs.modals;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Yash
 */
@Entity
@Table(name = "Attendance_Master", catalog = "TSA_V4", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AttendanceMaster.findAll", query = "SELECT a FROM AttendanceMaster a"),
    @NamedQuery(name = "AttendanceMaster.findById", query = "SELECT a FROM AttendanceMaster a WHERE a.id = :id"),
    @NamedQuery(name = "AttendanceMaster.findByDateTime", query = "SELECT a FROM AttendanceMaster a WHERE a.dateTime = :dateTime"),
    @NamedQuery(name = "AttendanceMaster.findByStatus", query = "SELECT a FROM AttendanceMaster a WHERE a.status = :status")})
public class AttendanceMaster implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Lob
    @Column(name = "student_name", length = 65535)
    private String studentName;
    @Column(name = "date_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateTime;
    @Basic(optional = false)
    @Column(name = "status", nullable = false)
    private int status;
    @JoinColumn(name = "class_name", referencedColumnName = "id")
    @ManyToOne
    private Classmaster className;
    @JoinColumn(name = "student_id", referencedColumnName = "id")
    @ManyToOne
    private StudentMaster studentId;

    public AttendanceMaster() {
    }

    public AttendanceMaster(Integer id) {
        this.id = id;
    }

    public AttendanceMaster(Integer id, int status) {
        this.id = id;
        this.status = status;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Classmaster getClassName() {
        return className;
    }

    public void setClassName(Classmaster className) {
        this.className = className;
    }

    public StudentMaster getStudentId() {
        return studentId;
    }

    public void setStudentId(StudentMaster studentId) {
        this.studentId = studentId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AttendanceMaster)) {
            return false;
        }
        AttendanceMaster other = (AttendanceMaster) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.tcs.modals.AttendanceMaster[ id=" + id + " ]";
    }
    
}
