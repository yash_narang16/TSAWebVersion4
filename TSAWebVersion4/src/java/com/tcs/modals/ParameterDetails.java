/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tcs.modals;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Yash
 */
@Entity
@Table(name = "parameter_details", catalog = "TSA_V4", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ParameterDetails.findAll", query = "SELECT p FROM ParameterDetails p"),
    @NamedQuery(name = "ParameterDetails.findById", query = "SELECT p FROM ParameterDetails p WHERE p.id = :id"),
    @NamedQuery(name = "ParameterDetails.findByScore", query = "SELECT p FROM ParameterDetails p WHERE p.score = :score")})
public class ParameterDetails implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Lob
    @Column(name = "description", length = 65535)
    private String description;
    @Column(name = "score")
    private Integer score;
    @JoinColumn(name = "attribute_id", referencedColumnName = "id")
    @ManyToOne
    private AttributeMaster attributeId;
    @JoinColumn(name = "parameter_id", referencedColumnName = "id")
    @ManyToOne
    private ParameterMaster parameterId;
    @JoinColumn(name = "rubricid", referencedColumnName = "id")
    @ManyToOne
    private RubricMaster rubricid;

    public ParameterDetails() {
    }

    public ParameterDetails(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public AttributeMaster getAttributeId() {
        return attributeId;
    }

    public void setAttributeId(AttributeMaster attributeId) {
        this.attributeId = attributeId;
    }

    public ParameterMaster getParameterId() {
        return parameterId;
    }

    public void setParameterId(ParameterMaster parameterId) {
        this.parameterId = parameterId;
    }

    public RubricMaster getRubricid() {
        return rubricid;
    }

    public void setRubricid(RubricMaster rubricid) {
        this.rubricid = rubricid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ParameterDetails)) {
            return false;
        }
        ParameterDetails other = (ParameterDetails) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.tcs.modals.ParameterDetails[ id=" + id + " ]";
    }
    
}
