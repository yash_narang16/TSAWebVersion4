/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tcs.modals;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Yash
 */
@Entity
@Table(name = "rubric_master", catalog = "TSA_V4", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RubricMaster.findAll", query = "SELECT r FROM RubricMaster r"),
    @NamedQuery(name = "RubricMaster.findById", query = "SELECT r FROM RubricMaster r WHERE r.id = :id"),
    @NamedQuery(name = "RubricMaster.findByAttributecount", query = "SELECT r FROM RubricMaster r WHERE r.attributecount = :attributecount"),
    @NamedQuery(name = "RubricMaster.findByParametercount", query = "SELECT r FROM RubricMaster r WHERE r.parametercount = :parametercount")})
public class RubricMaster implements Serializable {

    @OneToMany(mappedBy = "rubricid")
    private Collection<LessonPlanMaster> lessonPlanMasterCollection;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Column(name = "attributecount")
    private Integer attributecount;
    @Column(name = "parametercount")
    private Integer parametercount;
    @OneToMany(mappedBy = "rubricId")
    private Collection<ParameterMaster> parameterMasterCollection;
    @JoinColumn(name = "scholasticid", referencedColumnName = "id")
    @ManyToOne
    private ScholasticMaster scholasticid;
    @OneToMany(mappedBy = "attribueid")
    private Collection<AttributeMaster> attributeMasterCollection;
    @OneToMany(mappedBy = "rubricid")
    private Collection<ParameterDetails> parameterDetailsCollection;

    public RubricMaster() {
    }

    public RubricMaster(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAttributecount() {
        return attributecount;
    }

    public void setAttributecount(Integer attributecount) {
        this.attributecount = attributecount;
    }

    public Integer getParametercount() {
        return parametercount;
    }

    public void setParametercount(Integer parametercount) {
        this.parametercount = parametercount;
    }

    @XmlTransient
    public Collection<ParameterMaster> getParameterMasterCollection() {
        return parameterMasterCollection;
    }

    public void setParameterMasterCollection(Collection<ParameterMaster> parameterMasterCollection) {
        this.parameterMasterCollection = parameterMasterCollection;
    }

    public ScholasticMaster getScholasticid() {
        return scholasticid;
    }

    public void setScholasticid(ScholasticMaster scholasticid) {
        this.scholasticid = scholasticid;
    }

    @XmlTransient
    public Collection<AttributeMaster> getAttributeMasterCollection() {
        return attributeMasterCollection;
    }

    public void setAttributeMasterCollection(Collection<AttributeMaster> attributeMasterCollection) {
        this.attributeMasterCollection = attributeMasterCollection;
    }

    @XmlTransient
    public Collection<ParameterDetails> getParameterDetailsCollection() {
        return parameterDetailsCollection;
    }

    public void setParameterDetailsCollection(Collection<ParameterDetails> parameterDetailsCollection) {
        this.parameterDetailsCollection = parameterDetailsCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RubricMaster)) {
            return false;
        }
        RubricMaster other = (RubricMaster) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.tcs.modals.RubricMaster[ id=" + id + " ]";
    }

    @XmlTransient
    public Collection<LessonPlanMaster> getLessonPlanMasterCollection() {
        return lessonPlanMasterCollection;
    }

    public void setLessonPlanMasterCollection(Collection<LessonPlanMaster> lessonPlanMasterCollection) {
        this.lessonPlanMasterCollection = lessonPlanMasterCollection;
    }
    
}
