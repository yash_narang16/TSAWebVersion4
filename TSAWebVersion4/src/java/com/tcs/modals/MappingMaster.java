/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tcs.modals;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Yash
 */
@Entity
@Table(name = "mapping_master", catalog = "TSA_V4", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MappingMaster.findAll", query = "SELECT m FROM MappingMaster m"),
    @NamedQuery(name = "MappingMaster.findById", query = "SELECT m FROM MappingMaster m WHERE m.id = :id"),
    @NamedQuery(name = "MappingMaster.findByDate", query = "SELECT m FROM MappingMaster m WHERE m.date = :date")})
public class MappingMaster implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Basic(optional = false)
    @Column(name = "date", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;
    @JoinColumn(name = "lessonplan_id", referencedColumnName = "id")
    @ManyToOne
    private LessonPlanMaster lessonplanId;

    public MappingMaster() {
    }

    public MappingMaster(Integer id) {
        this.id = id;
    }

    public MappingMaster(Integer id, Date date) {
        this.id = id;
        this.date = date;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public LessonPlanMaster getLessonplanId() {
        return lessonplanId;
    }

    public void setLessonplanId(LessonPlanMaster lessonplanId) {
        this.lessonplanId = lessonplanId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MappingMaster)) {
            return false;
        }
        MappingMaster other = (MappingMaster) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.tcs.modals.MappingMaster[ id=" + id + " ]";
    }
    
}
