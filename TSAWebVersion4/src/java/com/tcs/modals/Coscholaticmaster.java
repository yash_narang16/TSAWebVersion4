/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tcs.modals;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Yash
 */
@Entity
@Table(name = "Coscholatic_master", catalog = "TSA_V4", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Coscholaticmaster.findAll", query = "SELECT c FROM Coscholaticmaster c"),
    @NamedQuery(name = "Coscholaticmaster.findById", query = "SELECT c FROM Coscholaticmaster c WHERE c.id = :id"),
    @NamedQuery(name = "Coscholaticmaster.findByValueCount", query = "SELECT c FROM Coscholaticmaster c WHERE c.valueCount = :valueCount")})
public class Coscholaticmaster implements Serializable {

    @OneToMany(mappedBy = "coScholasticid")
    private Collection<LessonPlanMaster> lessonPlanMasterCollection;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Column(name = "value_count")
    private Integer valueCount;

    public Coscholaticmaster() {
    }

    public Coscholaticmaster(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getValueCount() {
        return valueCount;
    }

    public void setValueCount(Integer valueCount) {
        this.valueCount = valueCount;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Coscholaticmaster)) {
            return false;
        }
        Coscholaticmaster other = (Coscholaticmaster) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.tcs.modals.Coscholaticmaster[ id=" + id + " ]";
    }

    @XmlTransient
    public Collection<LessonPlanMaster> getLessonPlanMasterCollection() {
        return lessonPlanMasterCollection;
    }

    public void setLessonPlanMasterCollection(Collection<LessonPlanMaster> lessonPlanMasterCollection) {
        this.lessonPlanMasterCollection = lessonPlanMasterCollection;
    }
    
}
