/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tcs.modals;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Yash
 */
@Entity
@Table(name = "Parameter_Master", catalog = "TSA_V4", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ParameterMaster.findAll", query = "SELECT p FROM ParameterMaster p"),
    @NamedQuery(name = "ParameterMaster.findById", query = "SELECT p FROM ParameterMaster p WHERE p.id = :id"),
    @NamedQuery(name = "ParameterMaster.findByScore", query = "SELECT p FROM ParameterMaster p WHERE p.score = :score")})
public class ParameterMaster implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Lob
    @Column(name = "name", length = 65535)
    private String name;
    @Column(name = "Score")
    private Integer score;
    @JoinColumn(name = "rubric_id", referencedColumnName = "id")
    @ManyToOne
    private RubricMaster rubricId;
    @OneToMany(mappedBy = "parameterId")
    private Collection<ParameterDetails> parameterDetailsCollection;

    public ParameterMaster() {
    }

    public ParameterMaster(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public RubricMaster getRubricId() {
        return rubricId;
    }

    public void setRubricId(RubricMaster rubricId) {
        this.rubricId = rubricId;
    }

    @XmlTransient
    public Collection<ParameterDetails> getParameterDetailsCollection() {
        return parameterDetailsCollection;
    }

    public void setParameterDetailsCollection(Collection<ParameterDetails> parameterDetailsCollection) {
        this.parameterDetailsCollection = parameterDetailsCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ParameterMaster)) {
            return false;
        }
        ParameterMaster other = (ParameterMaster) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.tcs.modals.ParameterMaster[ id=" + id + " ]";
    }
    
}
