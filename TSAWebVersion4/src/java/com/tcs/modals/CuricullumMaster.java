/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tcs.modals;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Yash
 */
@Entity
@Table(name = "curicullum_master", catalog = "TSA_V4", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CuricullumMaster.findAll", query = "SELECT c FROM CuricullumMaster c"),
    @NamedQuery(name = "CuricullumMaster.findById", query = "SELECT c FROM CuricullumMaster c WHERE c.id = :id"),
    @NamedQuery(name = "CuricullumMaster.findByCuricullumName", query = "SELECT c FROM CuricullumMaster c WHERE c.curicullumName = :curicullumName")})
public class CuricullumMaster implements Serializable {

    @OneToMany(mappedBy = "curicullum")
    private Collection<User> userCollection;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Column(name = "curicullum_name", length = 45)
    private String curicullumName;
    @OneToMany(mappedBy = "curicullumid")
    private Collection<ChapterMaster> chapterMasterCollection;

    public CuricullumMaster() {
    }

    public CuricullumMaster(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCuricullumName() {
        return curicullumName;
    }

    public void setCuricullumName(String curicullumName) {
        this.curicullumName = curicullumName;
    }

    @XmlTransient
    public Collection<ChapterMaster> getChapterMasterCollection() {
        return chapterMasterCollection;
    }

    public void setChapterMasterCollection(Collection<ChapterMaster> chapterMasterCollection) {
        this.chapterMasterCollection = chapterMasterCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CuricullumMaster)) {
            return false;
        }
        CuricullumMaster other = (CuricullumMaster) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.tcs.modals.CuricullumMaster[ id=" + id + " ]";
    }

    @XmlTransient
    public Collection<User> getUserCollection() {
        return userCollection;
    }

    public void setUserCollection(Collection<User> userCollection) {
        this.userCollection = userCollection;
    }
    
}
