/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tcs.modals;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Yash
 */
@Entity
@Table(name = "user", catalog = "TSA_V4", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "User.findAll", query = "SELECT u FROM User u"),
    @NamedQuery(name = "User.findById", query = "SELECT u FROM User u WHERE u.id = :id"),
    @NamedQuery(name = "User.findByEmail", query = "SELECT u FROM User u WHERE u.email = :email"),
    @NamedQuery(name = "User.findByPasword", query = "SELECT u FROM User u WHERE u.pasword = :pasword"),
    @NamedQuery(name = "User.findBySchool", query = "SELECT u FROM User u WHERE u.school = :school")})
public class User implements Serializable {

    @OneToMany(mappedBy = "userid")
    private Collection<LessonPlanMaster> lessonPlanMasterCollection;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Column(name = "email", length = 45)
    private String email;
    @Column(name = "pasword", length = 45)
    private String pasword;
    @Column(name = "school", length = 45)
    private String school;
    @JoinColumn(name = "class", referencedColumnName = "id")
    @ManyToOne
    private Classmaster class1;
    @JoinColumn(name = "curicullum", referencedColumnName = "id")
    @ManyToOne
    private CuricullumMaster curicullum;
    @JoinColumn(name = "role", referencedColumnName = "role_id")
    @ManyToOne
    private RoleMaster role;
    @JoinColumn(name = "subject", referencedColumnName = "id")
    @ManyToOne
    private Subjectmaster subject;

    public User() {
    }

    public User(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPasword() {
        return pasword;
    }

    public void setPasword(String pasword) {
        this.pasword = pasword;
    }

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public Classmaster getClass1() {
        return class1;
    }

    public void setClass1(Classmaster class1) {
        this.class1 = class1;
    }

    public CuricullumMaster getCuricullum() {
        return curicullum;
    }

    public void setCuricullum(CuricullumMaster curicullum) {
        this.curicullum = curicullum;
    }

    public RoleMaster getRole() {
        return role;
    }

    public void setRole(RoleMaster role) {
        this.role = role;
    }

    public Subjectmaster getSubject() {
        return subject;
    }

    public void setSubject(Subjectmaster subject) {
        this.subject = subject;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof User)) {
            return false;
        }
        User other = (User) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.tcs.modals.User[ id=" + id + " ]";
    }

    @XmlTransient
    public Collection<LessonPlanMaster> getLessonPlanMasterCollection() {
        return lessonPlanMasterCollection;
    }

    public void setLessonPlanMasterCollection(Collection<LessonPlanMaster> lessonPlanMasterCollection) {
        this.lessonPlanMasterCollection = lessonPlanMasterCollection;
    }
    
}
