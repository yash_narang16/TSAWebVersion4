/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tcs.modals;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Yash
 */
@Entity
@Table(name = "ChapterMaster", catalog = "TSA_V4", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ChapterMaster.findAll", query = "SELECT c FROM ChapterMaster c"),
    @NamedQuery(name = "ChapterMaster.findById", query = "SELECT c FROM ChapterMaster c WHERE c.id = :id"),
    @NamedQuery(name = "ChapterMaster.findByChapterName", query = "SELECT c FROM ChapterMaster c WHERE c.chapterName = :chapterName")})
public class ChapterMaster implements Serializable {

    @OneToMany(mappedBy = "chapterid")
    private Collection<Objective> objectiveCollection;

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Column(name = "ChapterName", length = 45)
    private String chapterName;
    @OneToMany(mappedBy = "chapterid")
    private Collection<Activity> activityCollection;
    @JoinColumn(name = "classid", referencedColumnName = "id")
    @ManyToOne
    private Classmaster classid;
    @JoinColumn(name = "curicullumid", referencedColumnName = "id")
    @ManyToOne
    private CuricullumMaster curicullumid;
    @JoinColumn(name = "subjectid", referencedColumnName = "id")
    @ManyToOne
    private Subjectmaster subjectid;

    public ChapterMaster() {
    }

    public ChapterMaster(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getChapterName() {
        return chapterName;
    }

    public void setChapterName(String chapterName) {
        this.chapterName = chapterName;
    }

    @XmlTransient
    public Collection<Activity> getActivityCollection() {
        return activityCollection;
    }

    public void setActivityCollection(Collection<Activity> activityCollection) {
        this.activityCollection = activityCollection;
    }

    public Classmaster getClassid() {
        return classid;
    }

    public void setClassid(Classmaster classid) {
        this.classid = classid;
    }

    public CuricullumMaster getCuricullumid() {
        return curicullumid;
    }

    public void setCuricullumid(CuricullumMaster curicullumid) {
        this.curicullumid = curicullumid;
    }

    public Subjectmaster getSubjectid() {
        return subjectid;
    }

    public void setSubjectid(Subjectmaster subjectid) {
        this.subjectid = subjectid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ChapterMaster)) {
            return false;
        }
        ChapterMaster other = (ChapterMaster) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.tcs.modals.ChapterMaster[ id=" + id + " ]";
    }

    @XmlTransient
    public Collection<Objective> getObjectiveCollection() {
        return objectiveCollection;
    }

    public void setObjectiveCollection(Collection<Objective> objectiveCollection) {
        this.objectiveCollection = objectiveCollection;
    }
    
}
