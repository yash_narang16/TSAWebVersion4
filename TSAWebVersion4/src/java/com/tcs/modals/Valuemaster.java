/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tcs.modals;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Yash
 */
@Entity
@Table(name = "Value_master", catalog = "TSA_V4", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Valuemaster.findAll", query = "SELECT v FROM Valuemaster v"),
    @NamedQuery(name = "Valuemaster.findById", query = "SELECT v FROM Valuemaster v WHERE v.id = :id")})
public class Valuemaster implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Lob
    @Column(name = "value_name", length = 65535)
    private String valueName;
    @JoinColumn(name = "Coscholastic_id", referencedColumnName = "id")
    @ManyToOne
    private Coscholaticmaster coscholasticid;

    public Valuemaster() {
    }

    public Valuemaster(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getValueName() {
        return valueName;
    }

    public void setValueName(String valueName) {
        this.valueName = valueName;
    }

    public Coscholaticmaster getCoscholasticid() {
        return coscholasticid;
    }

    public void setCoscholasticid(Coscholaticmaster coscholasticid) {
        this.coscholasticid = coscholasticid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Valuemaster)) {
            return false;
        }
        Valuemaster other = (Valuemaster) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.tcs.modals.Valuemaster[ id=" + id + " ]";
    }
    
}
