/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tcs.modals;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Yash
 */
@Entity
@Table(name = "activity", catalog = "TSA_V4", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Activity.findAll", query = "SELECT a FROM Activity a"),
    @NamedQuery(name = "Activity.findById", query = "SELECT a FROM Activity a WHERE a.id = :id"),
    @NamedQuery(name = "Activity.findByActivityname", query = "SELECT a FROM Activity a WHERE a.activityname = :activityname"),
    @NamedQuery(name = "Activity.findByCuricullumid", query = "SELECT a FROM Activity a WHERE a.curicullumid = :curicullumid"),
    @NamedQuery(name = "Activity.findByClassid", query = "SELECT a FROM Activity a WHERE a.classid = :classid"),
    @NamedQuery(name = "Activity.findBySubjectid", query = "SELECT a FROM Activity a WHERE a.subjectid = :subjectid"),
    @NamedQuery(name = "Activity.findByMaterialRequired", query = "SELECT a FROM Activity a WHERE a.materialRequired = :materialRequired"),
    @NamedQuery(name = "Activity.findByDuration", query = "SELECT a FROM Activity a WHERE a.duration = :duration")})
public class Activity implements Serializable {

    @OneToMany(mappedBy = "activityid")
    private Collection<LessonPlanMaster> lessonPlanMasterCollection;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Column(name = "activityname", length = 45)
    private String activityname;
    @Column(name = "curicullumid")
    private Integer curicullumid;
    @Column(name = "classid")
    private Integer classid;
    @Column(name = "subjectid")
    private Integer subjectid;
    @Column(name = "material_required", length = 45)
    private String materialRequired;
    @Column(name = "duration", length = 15)
    private String duration;
    @Lob
    @Column(name = "description", length = 65535)
    private String description;
    @Lob
    @Column(name = "filepath", length = 65535)
    private String filepath;
    @JoinColumn(name = "chapterid", referencedColumnName = "id")
    @ManyToOne
    private ChapterMaster chapterid;

    public Activity() {
    }

    public Activity(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getActivityname() {
        return activityname;
    }

    public void setActivityname(String activityname) {
        this.activityname = activityname;
    }

    public Integer getCuricullumid() {
        return curicullumid;
    }

    public void setCuricullumid(Integer curicullumid) {
        this.curicullumid = curicullumid;
    }

    public Integer getClassid() {
        return classid;
    }

    public void setClassid(Integer classid) {
        this.classid = classid;
    }

    public Integer getSubjectid() {
        return subjectid;
    }

    public void setSubjectid(Integer subjectid) {
        this.subjectid = subjectid;
    }

    public String getMaterialRequired() {
        return materialRequired;
    }

    public void setMaterialRequired(String materialRequired) {
        this.materialRequired = materialRequired;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFilepath() {
        return filepath;
    }

    public void setFilepath(String filepath) {
        this.filepath = filepath;
    }

    public ChapterMaster getChapterid() {
        return chapterid;
    }

    public void setChapterid(ChapterMaster chapterid) {
        this.chapterid = chapterid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Activity)) {
            return false;
        }
        Activity other = (Activity) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.tcs.modals.Activity[ id=" + id + " ]";
    }

    @XmlTransient
    public Collection<LessonPlanMaster> getLessonPlanMasterCollection() {
        return lessonPlanMasterCollection;
    }

    public void setLessonPlanMasterCollection(Collection<LessonPlanMaster> lessonPlanMasterCollection) {
        this.lessonPlanMasterCollection = lessonPlanMasterCollection;
    }
    
}
