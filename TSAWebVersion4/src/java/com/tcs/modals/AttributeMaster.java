/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tcs.modals;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Yash
 */
@Entity
@Table(name = "attribute_Master", catalog = "TSA_V4", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AttributeMaster.findAll", query = "SELECT a FROM AttributeMaster a"),
    @NamedQuery(name = "AttributeMaster.findById", query = "SELECT a FROM AttributeMaster a WHERE a.id = :id"),
    @NamedQuery(name = "AttributeMaster.findByAttributename", query = "SELECT a FROM AttributeMaster a WHERE a.attributename = :attributename"),
    @NamedQuery(name = "AttributeMaster.findByScore", query = "SELECT a FROM AttributeMaster a WHERE a.score = :score")})
public class AttributeMaster implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Column(name = "attributename", length = 100)
    private String attributename;
    @Column(name = "score")
    private Integer score;
    @JoinColumn(name = "attribueid", referencedColumnName = "id")
    @ManyToOne
    private RubricMaster attribueid;
    @OneToMany(mappedBy = "attributeId")
    private Collection<ParameterDetails> parameterDetailsCollection;

    public AttributeMaster() {
    }

    public AttributeMaster(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAttributename() {
        return attributename;
    }

    public void setAttributename(String attributename) {
        this.attributename = attributename;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public RubricMaster getAttribueid() {
        return attribueid;
    }

    public void setAttribueid(RubricMaster attribueid) {
        this.attribueid = attribueid;
    }

    @XmlTransient
    public Collection<ParameterDetails> getParameterDetailsCollection() {
        return parameterDetailsCollection;
    }

    public void setParameterDetailsCollection(Collection<ParameterDetails> parameterDetailsCollection) {
        this.parameterDetailsCollection = parameterDetailsCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AttributeMaster)) {
            return false;
        }
        AttributeMaster other = (AttributeMaster) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.tcs.modals.AttributeMaster[ id=" + id + " ]";
    }
    
}
