/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tcs.services;

import com.tcs.modals.LessonPlanMaster;
import com.tcs.modals.MappingMaster;
//import static com.tcs.modals.MappingMaster_.lessonplanId;
import com.tcs.util.NewHibernateUtil;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author Yash
 */
public class LessonPlanMapping {

    Session session = null;
    Transaction tx = null;

    public Integer MapLessonPlan(String date, int lessonplanid) {
        Integer result = null;
        try {

            SessionFactory se = new NewHibernateUtil().getSessionFactory();
            session = se.getCurrentSession();
            session.beginTransaction();
            tx = session.getTransaction();
            MappingMaster map = new MappingMaster();
            LessonPlanMaster plan = new LessonPlanMaster();
            plan.setId(lessonplanid);
            map.setLessonplanId(plan);
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
            Date date1 = format.parse(date);
            map.setDate(date1);
            result = (Integer) session.save(map);
            tx.commit();

        } catch (ParseException ex) {
            Logger.getLogger(LessonPlanMapping.class.getName()).log(Level.SEVERE, null, ex);
        }

        return result;
    }

    public ArrayList<MappingMaster> fetchLessonplan() {
        ArrayList<MappingMaster> list = new ArrayList<MappingMaster>();
        SessionFactory se = new NewHibernateUtil().getSessionFactory();
        session = se.getCurrentSession();
        session.beginTransaction();
        tx = session.getTransaction();
        list = (ArrayList<MappingMaster>) session.createQuery("from MappingMaster").list();
        tx.commit();

        return list;
    }

}
