/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tcs.services;

import com.tcs.modals.Activity;
import com.tcs.modals.Coscholaticmaster;
import com.tcs.modals.LessonPlanMaster;
import com.tcs.modals.Objective;
import com.tcs.modals.RubricMaster;
import com.tcs.modals.User;
import com.tcs.util.NewHibernateUtil;
import java.util.ArrayList;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author Yash
 */
public class LessonPlanService {

    Session session = null;
    Transaction tx = null;

    public Integer SaveLessonPlan(int objectiveid, int activityid, int userid, String lessonplanName, String lessonplanduration, String homeactivity, String observation, String worksheetpath, String handoutpath, String videopath) {
        Integer result = null;
        SessionFactory se = new NewHibernateUtil().getSessionFactory();
        session = se.getCurrentSession();
        session.beginTransaction();
        tx = session.getTransaction();
        Query query = session.createQuery("From RubricMaster order by id DESC");
        query.setMaxResults(1);
        RubricMaster rubricId = new RubricMaster();
        rubricId = (RubricMaster) query.uniqueResult();
        query = session.createQuery("From Coscholaticmaster order by id DESC");
        query.setMaxResults(1);
        Coscholaticmaster ColsId = new Coscholaticmaster();
        ColsId = (Coscholaticmaster) query.uniqueResult();
        Activity activity = new Activity();
        activity.setId(activityid);
        Objective objective = new Objective();
        objective.setId(objectiveid);
        User user = new User();
        user.setId(userid);
        LessonPlanMaster lesson = new LessonPlanMaster();
        lesson.setActivityid(activity);
        lesson.setRubricid(rubricId);
        lesson.setCoScholasticid(ColsId);
        lesson.setUserid(user);
        lesson.setObservation(observation);
        lesson.setObjectiveid(objective);
        lesson.setHandouts(handoutpath);
        lesson.setWorksheets(worksheetpath);
        lesson.setDuration(Integer.parseInt(lessonplanduration));
        lesson.setHomeacitivity(homeactivity);
        lesson.setLessonPlanname(lessonplanName);

        result = (Integer) session.save(lesson);
        tx.commit();

        return result;
    }
    
    
    
    public ArrayList<LessonPlanMaster> fetchLessonPlan(){
    ArrayList<LessonPlanMaster> list = new ArrayList<LessonPlanMaster>();
    SessionFactory se = new NewHibernateUtil().getSessionFactory();
    session = se.getCurrentSession();
    session.beginTransaction();
    tx= session.getTransaction();
    list=(ArrayList<LessonPlanMaster>)session.createQuery("from LessonPlanMaster").list();
    tx.commit();
    return list;
    }
    

}
