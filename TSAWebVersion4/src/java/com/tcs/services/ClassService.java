/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tcs.services;

import com.tcs.modals.Classmaster;
import com.tcs.util.NewHibernateUtil;
import java.util.ArrayList;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author Yash
 */
public class ClassService {

    ArrayList<Classmaster> list = new ArrayList<Classmaster>();
    Session session = null;
    Transaction tx = null;

    public ArrayList<Classmaster> fetchClass() {
        try {
            SessionFactory se = new NewHibernateUtil().getSessionFactory();
            session = se.getCurrentSession();
            session.beginTransaction();
            tx = session.getTransaction();
            list = (ArrayList<Classmaster>) session.createQuery("from Classmaster").list();
            tx.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return list;
    }

}
