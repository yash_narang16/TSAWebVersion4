/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tcs.services;

import com.tcs.modals.AttendanceMaster;
import com.tcs.modals.Classmaster;
import com.tcs.modals.StudentMaster;
import com.tcs.util.NewHibernateUtil;
import gvjava.org.json.JSONArray;
import gvjava.org.json.JSONException;
import gvjava.org.json.JSONObject;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author Yash
 */
public class AttendanceService {

    Session session = null;
    Transaction tx = null;

    public ArrayList<StudentMaster> fetchstudentrecord(int classid) {
        ArrayList<StudentMaster> list = new ArrayList<StudentMaster>();
        SessionFactory se = new NewHibernateUtil().getSessionFactory();
        session = se.getCurrentSession();
        session.beginTransaction();
        tx = session.getTransaction();
        list = (ArrayList<StudentMaster>) session.createQuery("from StudentMaster where class_id='" + classid + "'").list();

        tx.commit();

        return list;
    }

    public Integer saveAttendance(JSONArray array) {
        Integer result = null;
        SessionFactory se = new NewHibernateUtil().getSessionFactory();
        session = se.getCurrentSession();
        session.beginTransaction();
        tx = session.getTransaction();
        for (int i = 0; i < array.length(); i++) {
            try {
                Classmaster master = new Classmaster();
                master.setId(Integer.parseInt(array.getJSONObject(i).getString("class_id")));
                StudentMaster stud = new StudentMaster();
                stud.setId(Integer.parseInt(array.getJSONObject(i).getString("student_id")));
                AttendanceMaster att = new AttendanceMaster();
                att.setClassName(master);
                att.setStudentId(stud);
                att.setStudentName(array.getJSONObject(i).getString("studentname"));
                if (array.getJSONObject(i).getString("check").equals("true")) {
                    att.setStatus(1);

                } else {
                    att.setStatus(0);

                }
                att.setDateTime(new Date());
                result = (Integer) session.save(att);
            } catch (JSONException ex) {
                Logger.getLogger(AttendanceService.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        tx.commit();

        return result;
    }

    public JSONArray viewAttendance() {
        ArrayList<StudentMaster> studlist = new ArrayList<StudentMaster>();
        ArrayList<AttendanceMaster> list = new ArrayList<AttendanceMaster>();
        SessionFactory se = new NewHibernateUtil().getSessionFactory();
        session = se.getCurrentSession();
        session.beginTransaction();
        tx = session.getTransaction();
        studlist = (ArrayList<StudentMaster>) session.createQuery("from StudentMaster").list();
        JSONArray arrayobj = new JSONArray();
        JSONObject object=null;
        for (int i = 0; i < studlist.size(); i++) {
            try {
                object = new JSONObject();
                object.put("student_name",studlist.get(i).getStudentname());
                list=(ArrayList<AttendanceMaster>) session.createQuery("from AttendanceMaster where student_id='" + studlist.get(i).getId() + "'").list();
                 JSONArray innerarray= new JSONArray();
                for(int j=0;j<list.size();j++){
                   JSONObject innerobj = new JSONObject();
                innerobj.put("student_id",list.get(j).getStudentId().getId());
                innerobj.put("attendance_date",list.get(j).getDateTime());
                innerobj.put("attendance_status",list.get(j).getStatus());
                 innerarray.put(innerobj);
                }
                object.put("arraydata",innerarray);
               
            } catch (JSONException ex) {
                Logger.getLogger(AttendanceService.class.getName()).log(Level.SEVERE, null, ex);
            }
            arrayobj.put(object);
        }
        tx.commit();

        return arrayobj;
    }

}
