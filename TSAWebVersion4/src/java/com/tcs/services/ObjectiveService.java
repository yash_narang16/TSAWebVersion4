/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tcs.services;

import com.tcs.modals.ChapterMaster;
import com.tcs.modals.Objective;

import com.tcs.util.NewHibernateUtil;
import java.util.ArrayList;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author Yash
 */
public class ObjectiveService {
    
    Session session=null;
    Transaction tx= null;
    ArrayList<Objective> list = new ArrayList<Objective>();
    public Integer SaveObjective(int curId,int classId, int subjectId,int chapterId,String name, String description){
        Integer result=null;
        try{
    SessionFactory se = new NewHibernateUtil().getSessionFactory();
    session=se.getCurrentSession();
    session.beginTransaction();
    tx=session.getTransaction();
    Objective object= new Objective();
    ChapterMaster chap=new ChapterMaster();
    chap.setId(chapterId);
    object.setObjectivename(name);
     object.setCuricullumid(curId);
      object.setClassid(classId);
       object.setSubjectid(subjectId);
        object.setChapterid(chap);
        object.setDescription(description);
        result=(Integer) session.save(object);
        tx.commit();
        }catch(Exception e){
        e.printStackTrace();
        }
        
    
    return result;
    }
    
    
    
    public ArrayList<Objective> FetchObjective(){
        try{
    SessionFactory se = new NewHibernateUtil().getSessionFactory();
    session =se.getCurrentSession();
    session.beginTransaction();
    tx= session.getTransaction();
    list=(ArrayList<Objective>) session.createQuery("from Objective").list();
    tx.commit();
        }catch(Exception e){
        e.printStackTrace();
        }
    
    
    return list;
    }
    
    
    
    
}
