/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tcs.services;

import com.tcs.modals.Coscholaticmaster;
import com.tcs.modals.Valuemaster;
import com.tcs.util.NewHibernateUtil;
import gvjava.org.json.JSONArray;
import gvjava.org.json.JSONException;
import gvjava.org.json.JSONObject;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author Yash
 */
public class Co_scholasticService {
    
    Session session = null;
    Transaction tx = null;
    
    public Integer SaveService(JSONArray value,int count){
    Integer result=null;
    SessionFactory  se = new NewHibernateUtil().getSessionFactory();
    session = se.getCurrentSession();
    session.beginTransaction();
    tx = session.getTransaction();
    Coscholaticmaster obj = new Coscholaticmaster();
     obj.setValueCount(count);
     session.save(obj);
     
     for(int i=0;i<count;i++){
     Valuemaster val = new Valuemaster();
     val.setCoscholasticid(obj);
        try {
            val.setValueName(value.getString(i).trim());
        } catch (JSONException ex) {
            Logger.getLogger(Co_scholasticService.class.getName()).log(Level.SEVERE, null, ex);
        }
        result=(Integer) session.save(val);
     }
    
    tx.commit();
    return  result;
    }
    
    
}
