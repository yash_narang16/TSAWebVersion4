/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tcs.services;

import com.tcs.modals.AttributeMaster;
import com.tcs.modals.ParameterDetails;
import com.tcs.modals.ParameterMaster;
import com.tcs.modals.RubricMaster;
import com.tcs.modals.ScholasticMaster;
import com.tcs.util.NewHibernateUtil;
import gvjava.org.json.JSONArray;
import gvjava.org.json.JSONException;
import gvjava.org.json.JSONObject;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author Yash
 */
public class RubricService {

    Session session = null;
    Transaction tx = null;

    public Integer SaveRubric(JSONArray name, JSONObject description, int type, int attribute_count, int parameter_count, JSONObject paramter_name) {
        Integer result = null;
        SessionFactory se = new NewHibernateUtil().getSessionFactory();
        session = se.getCurrentSession();
        session.beginTransaction();
        tx = session.getTransaction();
        ScholasticMaster scholastic = new ScholasticMaster();
        RubricMaster rubric = new RubricMaster();
        AttributeMaster attr = null;
        ParameterMaster para = null;
        scholastic.setId(type);
        rubric.setAttributecount(attribute_count);
        rubric.setParametercount(parameter_count);
        rubric.setScholasticid(scholastic);
         session.save(rubric);
       
          
            if(type==1){
            for (int i = 0; i < attribute_count; i++) {
                try {
                    attr = new AttributeMaster();
                    attr.setAttribueid(rubric);
                    attr.setAttributename(name.getString(i));
                  
                    session.save(attr);
                    if(type==1){
                    para = new ParameterMaster();
                    para.setRubricId(rubric);
                try {
                    para.setName(paramter_name.getJSONArray("parameter_name").getString(i));
                    if(type==2){
                       para.setScore(100);
                    }
                    else{
                    para.setScore(Integer.parseInt(paramter_name.getJSONObject("weightage").getString(""+i)));
                    }
                    
                } catch (JSONException ex) {
                    Logger.getLogger(RubricService.class.getName()).log(Level.SEVERE, null, ex);
                }
               session.save(para);
                    }
                    for(int j=0;j<parameter_count;j++){
                        
                    ParameterDetails details = new ParameterDetails();
                    details.setRubricid(rubric);
                    details.setParameterId(para);
                    details.setAttributeId(attr);
                    if(type==2)
                {
                    details.setDescription((description.getString("" + i)));
                }
                    else{
                    details.setDescription((description.getJSONObject(""+i).getString(""+j)));
                    }
                    session.save(details);
                }
                } catch (JSONException ex) {
                    Logger.getLogger(RubricService.class.getName()).log(Level.SEVERE, null, ex);
                }
            
                
        } 
            }
            else{
                
                for(int x=0;x<parameter_count;x++){
                
                    para = new ParameterMaster();
                    para.setRubricId(rubric);
                try {
                    para.setName(paramter_name.getJSONArray("parameter_name").getString(x));
                    if(type==2){
                       para.setScore(100);
                    }
                    else{
                    para.setScore(Integer.parseInt(paramter_name.getJSONObject("weightage").getString(""+x)));
                    }
                    
                } catch (JSONException ex) {
                    Logger.getLogger(RubricService.class.getName()).log(Level.SEVERE, null, ex);
                }
               session.save(para);
                    }
                
                
                
                
            
               for (int i = 0; i < attribute_count; i++) {
                try {
                    attr = new AttributeMaster();
                    attr.setAttribueid(rubric);
                    attr.setAttributename(name.getString(i));
                  
                    session.save(attr);
                    
                    for(int j=0;j<parameter_count;j++){
                        
                    ParameterDetails details = new ParameterDetails();
                    details.setRubricid(rubric);
                    details.setParameterId(para);
                    details.setAttributeId(attr);
                    if(type==2)
                {
                    details.setDescription((description.getString("" + i)));
                }
                    else{
                    details.setDescription((description.getJSONObject(""+i).getString(""+j)));
                    }
                   result= (Integer) session.save(details);
                }
                } catch (JSONException ex) {
                    Logger.getLogger(RubricService.class.getName()).log(Level.SEVERE, null, ex);
                }
            
                
        } 
            
            }
            
            
        
        tx.commit();
        
        return result;
}
    
}
