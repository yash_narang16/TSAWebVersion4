/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tcs.services;

import com.tcs.modals.Activity;
import com.tcs.modals.ChapterMaster;
import com.tcs.util.NewHibernateUtil;
import java.util.ArrayList;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author Yash
 */
public class ActivityService {
    Session session=null;
    Transaction tx= null;
    
    public Integer saveActivity(int curiId, String name, String description, String filepath, int classId, int subjectId,int chapterId,String material, String duration){
    Integer result=null;
    SessionFactory se = new NewHibernateUtil().getSessionFactory();
    session=se.getCurrentSession();
    session.beginTransaction();
    tx= session.getTransaction();
    Activity object = new Activity();
    ChapterMaster chap = new ChapterMaster();
    chap.setId(chapterId);
       object.setActivityname(name);
       object.setSubjectid(subjectId);
       object.setClassid(classId);
       object.setCuricullumid(curiId);
       object.setChapterid(chap);
       object.setDescription(description);
       object.setFilepath(filepath);
       object.setMaterialRequired(material);
       object.setDuration(duration);
       result=(Integer) session.save(object);
       tx.commit();
    return result;
    }
    
    
    public ArrayList<Activity> FetchActivty(){
       ArrayList<Activity> list = new ArrayList<Activity>();
        try{  
    SessionFactory se = new NewHibernateUtil().getSessionFactory();
    session=se.getCurrentSession();
    session.beginTransaction();
   tx= session.getTransaction();
   list= (ArrayList<Activity>) session.createQuery("from Activity").list();
  
    tx.commit();
        }catch(Exception e){
        e.printStackTrace();
        }
    return list;
    }
    
    
    
    
}
