/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tcs.services;

import com.tcs.modals.Subjectmaster;
import com.tcs.util.NewHibernateUtil;
import java.util.ArrayList;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author Yash
 */
public class SubjectService {
    
    Session session = null;
    Transaction tx= null;
    ArrayList<Subjectmaster> list = new ArrayList<Subjectmaster>();
    
    public ArrayList<Subjectmaster> fetchSubject(){
   try{
   SessionFactory se = new NewHibernateUtil().getSessionFactory();
   session = se.getCurrentSession();
   session.beginTransaction();
   tx= session.getTransaction();
   list=(ArrayList<Subjectmaster>) session.createQuery("from Subjectmaster").list();
   tx.commit();
   
   }catch(Exception e){
  e.printStackTrace();
   }
    
    return list;
    }
    
    
}
