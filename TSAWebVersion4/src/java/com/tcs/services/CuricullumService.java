/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tcs.services;

import com.tcs.modals.CuricullumMaster;
import com.tcs.util.NewHibernateUtil;
import java.util.ArrayList;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author Yash
 */
public class CuricullumService {
    
    ArrayList<CuricullumMaster> list = new ArrayList<CuricullumMaster>();
    Session session=null;
    Transaction tx=null;
    public ArrayList<CuricullumMaster> fetchcuricullum(){
        try{
    
        SessionFactory se= new NewHibernateUtil().getSessionFactory();
        session=se.getCurrentSession();
        session.beginTransaction();
        tx= session.getTransaction();
      list= (ArrayList<CuricullumMaster>) session.createQuery("from CuricullumMaster").list();
      tx.commit();
        }catch(Exception e){
        e.printStackTrace();
        }
        finally{
//        session.close();
        }
        
        
        
    return list;
    }
    
    
    public static void main(String[] args) {
        System.out.println(new CuricullumService().fetchcuricullum());
    }
    
}
