/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tcs.services;

import com.tcs.modals.ChapterMaster;
import com.tcs.modals.VideoMaster;
import com.tcs.util.NewHibernateUtil;
import java.util.ArrayList;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author Yash
 */
public class VideoService {
    
    
    Session session= null;
    Transaction tx = null;
    
    public Integer createVideo(String videoname, int chapterid, String filepath){
    Integer result=null;
    SessionFactory  se = new NewHibernateUtil().getSessionFactory();
    session=se.getCurrentSession();
    session.beginTransaction();
    tx=session.getTransaction();
    ChapterMaster chapter = new ChapterMaster();
    chapter.setId(chapterid);
     VideoMaster master  = new VideoMaster();
    master.setVideoname(videoname);
    master.setChapterId(chapter);
    master.setFilepath(filepath);
    result=(Integer) session.save(master);
    
    tx.commit();
    
    
    return result;
    }
    
    
    
    public ArrayList<VideoMaster> fetchVideos(){
    ArrayList<VideoMaster> list = new ArrayList<VideoMaster>();
    SessionFactory se = new NewHibernateUtil().getSessionFactory();
    session=se.getCurrentSession();
    session.beginTransaction();
    tx= session.getTransaction();
    list = (ArrayList<VideoMaster>) session.createQuery("From VideoMaster").list();
    
    tx.commit();
  
    return list;
    }
    
    
    
    
    
    
    
    
}
