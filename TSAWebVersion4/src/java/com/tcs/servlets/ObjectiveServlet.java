/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tcs.servlets;

import com.tcs.modals.Objective;
import com.tcs.services.ObjectiveService;
import gvjava.org.json.JSONArray;
import gvjava.org.json.JSONObject;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Yash
 */
public class ObjectiveServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Objective</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet Objective at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        String act = request.getParameter("act").trim();
        JSONObject jsonobject = new JSONObject();
        JSONArray jsonarray = new JSONArray();
        ArrayList<Objective> list = new ArrayList<Objective>();
        try {
            if (act.equals("save")) {
                int curId = Integer.parseInt(request.getParameter("curId"));
                int classId = Integer.parseInt(request.getParameter("classId"));
                int subjectId = Integer.parseInt(request.getParameter("subjectId"));
                int chapterId = Integer.parseInt(request.getParameter("chapterId"));
                String objective_name = request.getParameter("name");
                String objective_description = request.getParameter("description");

                ObjectiveService object = new ObjectiveService();
                Integer result;
                result = object.SaveObjective(curId, classId, subjectId, chapterId, objective_name, objective_description);

                if (result != null) {

                    jsonobject.put("status", "Yes");
                } else {
                    jsonobject.put("status", "No");
                }
                out.println(jsonobject); //sending back to angular js
                
            } else if (act.equals("fetch")) {
                ObjectiveService fetch_object = new ObjectiveService();
                list = fetch_object.FetchObjective();
                for (int i = 0; i < list.size(); i++) {
                    JSONObject fetchjson = new JSONObject();
                    fetchjson.put("objective_id", list.get(i).getId());
                    fetchjson.put("objective_name", list.get(i).getObjectivename());
                       fetchjson.put("objective_class", list.get(i).getChapterid().getClassid().getClassName());
                       fetchjson.put("objective_classid", list.get(i).getChapterid().getClassid().getId());
                    fetchjson.put("objective_chapter", list.get(i).getChapterid().getChapterName());
                        fetchjson.put("objective_chapterid", list.get(i).getChapterid().getId());
                    fetchjson.put("objective_curi", list.get(i).getChapterid().getCuricullumid().getCuricullumName());
                      fetchjson.put("objective_curiid", list.get(i).getChapterid().getCuricullumid().getId());
                    fetchjson.put("objective_subject", list.get(i).getChapterid().getSubjectid().getSubjectName());
                       fetchjson.put("objective_subjectid", list.get(i).getChapterid().getSubjectid().getId());
                    fetchjson.put("objective_description", list.get(i).getDescription());
                    jsonarray.put(fetchjson);
                }
                
                out.println(jsonarray);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
