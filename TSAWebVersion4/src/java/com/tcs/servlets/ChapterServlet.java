/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tcs.servlets;

import com.tcs.modals.ChapterMaster;
import com.tcs.services.ChapterService;
import gvjava.org.json.JSONArray;
import gvjava.org.json.JSONObject;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Yash
 */
public class ChapterServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ChapterServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ChapterServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        ArrayList<ChapterMaster> chap_array = new ArrayList<ChapterMaster>();
        JSONArray json_array = new JSONArray();
        try {
            String act = request.getParameter("act");
            int subjectId = Integer.parseInt(request.getParameter("subject"));
            int classId = Integer.parseInt(request.getParameter("class"));
             int curiId = Integer.parseInt(request.getParameter("curicullum"));
            if (act.equals("fetch")) {
                ChapterService serviceobj = new ChapterService();
                chap_array = serviceobj.fetchChapter(subjectId, classId,curiId);
                if(chap_array.size()!=0){
                for (int i = 0; i < chap_array.size(); i++) {
                    JSONObject json_chap = new JSONObject();
                    json_chap.put("chapter_id", chap_array.get(i).getId());
                    json_chap.put("chapter_name", chap_array.get(i).getChapterName());
                    json_array.put(json_chap);
                }
                }
                else
                {
             JSONObject error= new JSONObject();
             error.put("status", "no");
             error.put("no", "No Data found");
             json_array.put(error);
                }

            }
        } catch (Exception e) {
            e.printStackTrace();;
        }

        out.println(json_array);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
