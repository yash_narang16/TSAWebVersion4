/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tcs.servlets;

import com.tcs.modals.StudentMaster;
import com.tcs.services.AttendanceService;
import gvjava.org.json.JSONArray;
import gvjava.org.json.JSONException;
import gvjava.org.json.JSONObject;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Yash
 */
public class AttendanceServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet AttendanceServlet</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet AttendanceServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
          response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        String act = request.getParameter("act");
        if(act.equals("fetch")){
            ArrayList<StudentMaster> list = new ArrayList<StudentMaster>();
            JSONArray arrayobj = new JSONArray();
        int classid = Integer.parseInt(request.getParameter("classid"));
        AttendanceService serviceobj = new AttendanceService();
       list= serviceobj.fetchstudentrecord(classid);
        for(int i=0;i<list.size();i++){
                try {
                    JSONObject jsonobj = new JSONObject();
                    jsonobj.put("studentname",list.get(i).getStudentname());
                    jsonobj.put("classname",list.get(i).getClassId().getClassName());
                     jsonobj.put("class_id",list.get(i).getClassId().getId());
                     jsonobj.put("student_id",list.get(i).getId());
                    jsonobj.put("check",true);
                    arrayobj.put(jsonobj);
                } catch (JSONException ex) {
                    Logger.getLogger(AttendanceServlet.class.getName()).log(Level.SEVERE, null, ex);
                }
        
        }
        
        
        out.println(arrayobj);
        
        
        
        }
        else if(act.equals("save")){
              JSONObject resultobj = new JSONObject();
              try {
                  String data=request.getParameter("studentdata");
                  JSONArray arrayobj = new JSONArray(data);
                  System.out.println(arrayobj);
                 AttendanceService serviceobj = new AttendanceService();
                 Integer result= serviceobj.saveAttendance(arrayobj);
               
                 if(result!=null)
                 {
                     resultobj.put("status","Yes");
                    
                 }
                 else{
                 resultobj.put("status","No");
                 
                 }
              } catch (JSONException ex) {
                  Logger.getLogger(AttendanceServlet.class.getName()).log(Level.SEVERE, null, ex);
              }
        out.println(resultobj);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
