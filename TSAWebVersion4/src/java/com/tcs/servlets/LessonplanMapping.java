/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tcs.servlets;

import com.tcs.modals.MappingMaster;
import com.tcs.services.LessonPlanMapping;
import gvjava.org.json.JSONArray;
import gvjava.org.json.JSONException;
import gvjava.org.json.JSONObject;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Yash
 */
public class LessonplanMapping extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet LessonplanMapping</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet LessonplanMapping at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        String act = request.getParameter("act");

        if (act.equals("map")) {
            String date = request.getParameter("date");
            
            int lessonplanid = Integer.parseInt(request.getParameter("id"));

            LessonPlanMapping plan = new LessonPlanMapping();
            Integer result = plan.MapLessonPlan(date, lessonplanid);

            JSONObject obj = new JSONObject();
            if (result != null) {
                try {
                    obj.put("status", "Yes");
                } catch (JSONException ex) {
                    Logger.getLogger(LessonplanMapping.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                try {
                    obj.put("status", "No");
                } catch (JSONException ex) {
                    Logger.getLogger(LessonplanMapping.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            out.println(obj);
        } else if (act.equals("fetch")) {
            JSONArray objarray = new JSONArray();
            ArrayList<MappingMaster> list = new ArrayList<MappingMaster>();
            LessonPlanMapping mapping = new LessonPlanMapping();
            list = mapping.fetchLessonplan();
            for (int i = 0; i < list.size(); i++) {
                try {
                    JSONObject obj = new JSONObject();
                    obj.put("lessonplan_date", list.get(i).getDate().toString().replace(" 00:00:00.0", ""));
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(list.get(i).getDate());
                    cal.add(Calendar.DATE, 1); //minus number would decrement the days
                 SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
                    obj.put("lessonplan_enddate",   format1.format(cal.getTime()));
                    obj.put("lessonplan_name", list.get(i).getLessonplanId().getLessonPlanname());
                    objarray.put(obj);
                } catch (JSONException ex) {
                    Logger.getLogger(LessonplanMapping.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            out.println(objarray);

        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
