/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tcs.servlets;

import com.tcs.services.RubricService;
import gvjava.org.json.JSONArray;
import gvjava.org.json.JSONException;
import gvjava.org.json.JSONObject;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Yash
 */
public class RubricServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet RubricServlet</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet RubricServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
         PrintWriter out = response.getWriter();
         String act =request.getParameter("act");
         int type=Integer.parseInt(request.getParameter("Type"));
         int attribute_count=Integer.parseInt(request.getParameter("count"));
         int parameter_count=Integer.parseInt(request.getParameter("parameter_count"));
         String listOfdata=request.getParameter("List");
         String paramter_name=request.getParameter("parameter_name");
         System.out.println(paramter_name);
          JSONObject printobj = new JSONObject();
        try {
            JSONObject object= new JSONObject(listOfdata);
            JSONArray name=object.getJSONArray("name");
             JSONObject pname= new JSONObject(paramter_name);
          
//            System.out.println(object.getJSONArray("name").getString(0));
           JSONObject description=object.getJSONObject("description");
           
//           System.out.println(object.getJSONObject("score").getString("0"));
            
           RubricService rubric = new RubricService();
           Integer result=null;
                   result=rubric.SaveRubric(name, description,type, attribute_count,parameter_count,pname);
           
           if(result!=null){
         
          printobj.put("status", "Yes");
           }
           else{
           printobj.put("status", "No");
           }
        } catch (JSONException ex) {
            Logger.getLogger(RubricServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
         
         
         
         
        out.println(printobj);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
