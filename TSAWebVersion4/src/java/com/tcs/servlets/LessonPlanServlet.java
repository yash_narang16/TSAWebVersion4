/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tcs.servlets;

import com.tcs.modals.LessonPlanMaster;
import com.tcs.services.LessonPlanService;
import gvjava.org.json.JSONArray;
import gvjava.org.json.JSONObject;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Yash
 */
public class LessonPlanServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet LessonPlanServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet LessonPlanServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        try {
            String act = request.getParameter("act");
            JSONObject jsonobj = new JSONObject();
            if (act.equals("save")) {
                int objectiveid = Integer.parseInt(request.getParameter("objectiveid"));
                int activityid = Integer.parseInt(request.getParameter("activityid"));
                int userid = Integer.parseInt(request.getParameter("userid"));
                String lessonplanName = request.getParameter("lessonplanName");
                String lessonplanduration = request.getParameter("lessonplanduration");
                String homeactivity = request.getParameter("homeactivity");
                String observation = request.getParameter("observation");
                String worksheetpath = request.getParameter("worksheetpath");
                String handoutpath = request.getParameter("handoutpath");
                String videopath = request.getParameter("videopath");
                LessonPlanService serviceobj = new LessonPlanService();
                Integer result= serviceobj.SaveLessonPlan(objectiveid, activityid, userid, lessonplanName, lessonplanduration, homeactivity, observation, worksheetpath, handoutpath, videopath);
                if(result!=null){
                jsonobj.put("status","yes");
                }
                else{
                 jsonobj.put("status","no");
                }
                out.println(jsonobj);
            }
            else if(act.equals("fetch")){
                ArrayList<LessonPlanMaster> list = new ArrayList<LessonPlanMaster>();
                JSONArray dataarray = new JSONArray();
            LessonPlanService fetchobj = new LessonPlanService();
            list=fetchobj.fetchLessonPlan();
            for(int i=0;i<list.size();i++){
            JSONObject data= new JSONObject();
            data.put("lessonplan_id",list.get(i).getId());
            data.put("lessonplan_name",list.get(i).getLessonPlanname());
            data.put("lessonplan_activity_name",list.get(i).getActivityid().getActivityname());
            data.put("lessonplan_curicullum",list.get(i).getActivityid().getChapterid().getCuricullumid().getCuricullumName());
            data.put("lessonplan_class",list.get(i).getActivityid().getChapterid().getClassid().getClassName());
            data.put("lessonplan_subject",list.get(i).getActivityid().getChapterid().getSubjectid().getSubjectName());
            data.put("lessonplan_chapter",list.get(i).getActivityid().getChapterid().getChapterName());
            data.put("lessonplan_worksheet",list.get(i).getWorksheets());
            data.put("lessonplan_hadouts",list.get(i).getHandouts());
            data.put("lessonplan_activity_descp",list.get(i).getActivityid().getDescription());
            data.put("lessonplan_objective_name",list.get(i).getObjectiveid().getObjectivename());
            data.put("lessonplan_objective_descp",list.get(i).getObjectiveid().getDescription());
            data.put("lessonplan_homeactivity",list.get(i).getHomeacitivity());
            data.put("lessonplan_duration",list.get(i).getDuration());
            data.put("lessonplan_user",list.get(i).getUserid().getEmail());
            dataarray.put(data);
            }
            
            
            out.println(dataarray);
            }
            

        } catch (Exception e) {
            e.printStackTrace();

        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
