/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tcs.servlets;

import com.tcs.modals.VideoMaster;
import com.tcs.services.VideoService;
import gvjava.org.json.JSONArray;
import gvjava.org.json.JSONException;
import gvjava.org.json.JSONObject;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Yash
 */
public class VideoServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet VideoServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet VideoServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        String act = request.getParameter("act");
        if (act.equals("save")) {
            int chapterid = Integer.parseInt(request.getParameter("chapterId"));

            String name = request.getParameter("name");
            String filepath = request.getParameter("filepath");
            VideoService serviceobj = new VideoService();
            Integer result = serviceobj.createVideo(name, chapterid, filepath);
            JSONObject obj = new JSONObject();
            if (result != null) {
                try {

                    obj.put("status", "Yes");
                } catch (JSONException ex) {
                    Logger.getLogger(VideoServlet.class.getName()).log(Level.SEVERE, null, ex);
                }

            } else {
                try {
                    obj.put("status", "No");
                } catch (JSONException ex) {
                    Logger.getLogger(VideoServlet.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            out.println(obj);
        } else if (act.equals("fetch")) {
            ArrayList<VideoMaster> list = new ArrayList<VideoMaster>();
            VideoService serviceobj = new VideoService();
            list = serviceobj.fetchVideos();
            JSONArray arrayobj = new JSONArray();
            for (int i = 0; i < list.size(); i++) {
                try {
                    JSONObject object = new JSONObject();
                    object.put("chapterid", list.get(i).getChapterId().getId());
                    object.put("chaptername", list.get(i).getChapterId().getChapterName());
                    int index = list.get(i).getFilepath().lastIndexOf("/");
                        System.out.println(index);
                        String filePath ="upload"+list.get(i).getFilepath().substring(index, list.get(i).getFilepath().length());
                    object.put("videoname",list.get(i).getVideoname());
                    object.put("videopath",filePath);
                    object.put("videoid", list.get(i).getId());
                    arrayobj.put(object);
                } catch (JSONException ex) {
                    Logger.getLogger(VideoServlet.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            out.println(arrayobj);

        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
