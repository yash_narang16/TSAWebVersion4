/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tcs.servlets;

import com.tcs.modals.Activity;
import com.tcs.services.ActivityService;
import gvjava.org.json.JSONArray;
import gvjava.org.json.JSONObject;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Yash
 */
public class ActivityServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ActivityServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ActivityServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        JSONObject jsonobj = new JSONObject();
        try {

            String act = request.getParameter("act");

            ActivityService serviceobj = new ActivityService();
            if (act.equals("save")) {
                int curiId = Integer.parseInt(request.getParameter("curicullum"));
                int classId = Integer.parseInt(request.getParameter("classobj"));
                int subjectId = Integer.parseInt(request.getParameter("subject"));
                int chapterId = Integer.parseInt(request.getParameter("chapter"));
                String name = request.getParameter("name");
                String filepath = request.getParameter("filepath");
                String material = request.getParameter("material");
                String duration = request.getParameter("duration");
                String description = request.getParameter("description");
                System.out.print(act + ":" + curiId + " " + classId + " " + subjectId + " " + chapterId + " " + name + " " + filepath + " " + material + " " + duration + " " + description);
                Integer result = serviceobj.saveActivity(curiId, name, description, filepath, classId, subjectId, chapterId, material, duration);
                if (result != null) {
                    jsonobj.put("status", "Yes");
                } else {
                    jsonobj.put("status", "No");
                }
                out.println(jsonobj);
            } else if (act.equals("fetch")) {

                ActivityService fetchall = new ActivityService();
                JSONArray fetcharray = new JSONArray();
                ArrayList<Activity> list = new ArrayList<Activity>();
                list = fetchall.FetchActivty();
                for (int i = 0; i < list.size(); i++) {
                    JSONObject activity = new JSONObject();
                    activity.put("activity_id", list.get(i).getId());
                    activity.put("activity_name", list.get(i).getActivityname());
                    activity.put("activity_curi", list.get(i).getChapterid().getCuricullumid().getCuricullumName());
                    activity.put("activity_curId", list.get(i).getChapterid().getCuricullumid().getId());
                    activity.put("activity_class", list.get(i).getChapterid().getClassid().getClassName());
                    activity.put("activity_classId", list.get(i).getChapterid().getClassid().getId());
                    activity.put("activity_subject", list.get(i).getChapterid().getSubjectid().getSubjectName());
                    activity.put("activity_subjectId", list.get(i).getChapterid().getSubjectid().getId());
                    activity.put("activity_chapter", list.get(i).getChapterid().getChapterName());
                    activity.put("activity_chapterId", list.get(i).getChapterid().getId());
                    if (!list.get(i).getFilepath().equals("undefined")) {
                        int index = list.get(i).getFilepath().lastIndexOf("/");
                        System.out.println(index);
                        String filePath = list.get(i).getFilepath().substring(index, list.get(i).getFilepath().length());
                        activity.put("activity_filepath", filePath);
                    } else {
                        activity.put("activity_filepath", "nofile");
                    }
                    activity.put("activty_description", list.get(i).getDescription());
                    activity.put("activity_duration", list.get(i).getDuration());
                    fetcharray.put(activity);
                }

                out.println(fetcharray);
            }

        } catch (Exception e) {

        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
